((docker nil)
 (magit-branch nil)
 (magit-branch-configure nil)
 (magit-commit nil)
 (magit-dispatch nil)
 (magit-push nil)
 (magit-remote
  ("-f"))
 (magit-stash nil))
