(require 'spacemacs-common.el)

(deftheme spacemacs-dark "Spacemacs theme, the dark version")

(create-spacemacs-theme 'dark 'spacemacs-dark)

(provide-theme 'spacemacs-dark)
