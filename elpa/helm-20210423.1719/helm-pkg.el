(define-package "helm" "20210423.1719" "Helm is an Emacs incremental and narrowing framework"
  '((emacs "25.1")
    (async "1.9.4")
    (popup "0.5.3")
    (helm-core "3.7.1"))
  :commit "3e3c81b0ad9b8ce0607d6f44225a5f2598fe7602" :authors
  '(("Thierry Volpiatto" . "thierry.volpiatto@gmail.com"))
  :maintainer
  '("Thierry Volpiatto" . "thierry.volpiatto@gmail.com")
  :url "https://github.com/emacs-helm/helm")
;; Local Variables:
;; no-byte-compile: t
;; End:
