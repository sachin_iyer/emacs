;;; -*- no-byte-compile: t -*-
(define-package "springboard" "20170106.755" "Temporarily change default-directory for one command" '((helm "1.6.9")) :commit "687d1e5898a880878995dc9bffe93b4598366203" :keywords '("helm") :authors '(("John Wiegley" . "jwiegley@gmail.com")) :maintainer '("John Wiegley" . "jwiegley@gmail.com") :url "https://github.com/jwiegley/springboard")
