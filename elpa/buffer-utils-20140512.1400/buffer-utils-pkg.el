;;; -*- no-byte-compile: t -*-
(define-package "buffer-utils" "20140512.1400" "Buffer-manipulation utility functions" 'nil :commit "685b13457e3a2085b7584e41365d2aa0779a1b6f" :keywords '("extensions") :authors '(("Roland Walker" . "walker@pobox.com")) :maintainer '("Roland Walker" . "walker@pobox.com") :url "http://github.com/rolandwalker/buffer-utils")
