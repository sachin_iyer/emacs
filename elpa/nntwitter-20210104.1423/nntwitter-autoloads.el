;;; nntwitter-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "nntwitter" "nntwitter.el" (0 0 0 0))
;;; Generated autoloads from nntwitter.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "nntwitter" '("nntwitter-")))

;;;***

;;;### (autoloads nil "nntwitter-api" "nntwitter-api.el" (0 0 0 0))
;;; Generated autoloads from nntwitter-api.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "nntwitter-api" '("nntwitter-api-")))

;;;***

;;;### (autoloads nil nil ("nntwitter-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; nntwitter-autoloads.el ends here
