(define-package "nntwitter" "20210104.1423" "Gnus Backend For Twitter"
  '((emacs "25.1")
    (dash "20190401")
    (anaphora "20180618")
    (request "20190819"))
  :commit "174eb3bdb1339872b62fe2bf0c27d9a3eb142d27" :keywords
  '("news")
  :url "https://github.com/dickmao/nntwitter")
;; Local Variables:
;; no-byte-compile: t
;; End:
