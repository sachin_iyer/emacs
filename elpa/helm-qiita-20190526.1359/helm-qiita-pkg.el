;;; -*- no-byte-compile: t -*-
(define-package "helm-qiita" "20190526.1359" "Qiita with helm interface" '((emacs "24") (helm "2.8.2")) :commit "5f82010c595f8e122aa3f68148ba8d8ccb1333d8" :authors '(("Takashi Masuda" . "masutaka.net@gmail.com")) :maintainer '("Takashi Masuda" . "masutaka.net@gmail.com") :url "https://github.com/masutaka/emacs-helm-qiita")
