;;; helm-qiita-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-qiita" "helm-qiita.el" (0 0 0 0))
;;; Generated autoloads from helm-qiita.el

(autoload 'helm-qiita "helm-qiita" "\
Search Qiita Stocks using `helm'.

\(fn)" t nil)

(autoload 'helm-qiita-initialize "helm-qiita" "\
Initialize `helm-qiita'.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-qiita" '("helm-qiita-")))

;;;***

;;;### (autoloads nil nil ("helm-qiita-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-qiita-autoloads.el ends here
