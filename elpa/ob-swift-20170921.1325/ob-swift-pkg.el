;;; -*- no-byte-compile: t -*-
(define-package "ob-swift" "20170921.1325" "org-babel functions for swift evaluation" '((org "8")) :commit "ed478ddbbe41ce5373efde06b4dd0c3663c9055f" :keywords '("org" "babel" "swift") :authors '(("Feng Zhou" . "zf.pascal@gmail.com")) :maintainer '("Feng Zhou" . "zf.pascal@gmail.com") :url "http://github.com/zweifisch/ob-swift")
