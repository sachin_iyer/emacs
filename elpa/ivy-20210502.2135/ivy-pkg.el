(define-package "ivy" "20210502.2135" "Incremental Vertical completYon"
  '((emacs "24.5"))
  :commit "fc89be74f2ff1ae1145436a2ebc3b9e1e4a8d521" :authors
  '(("Oleh Krehel" . "ohwoeowho@gmail.com"))
  :maintainer
  '("Oleh Krehel" . "ohwoeowho@gmail.com")
  :keywords
  '("matching")
  :url "https://github.com/abo-abo/swiper")
;; Local Variables:
;; no-byte-compile: t
;; End:
