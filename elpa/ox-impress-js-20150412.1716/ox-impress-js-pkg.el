(define-package "ox-impress-js" "20150412.1716" "impress.js Back-End for Org Export Engine"
  '((org "8"))
  :keywords
  '("outlines" "hypermedia" "calendar" "wp")
  :authors
  '(("Takumi Kinjo <takumi dot kinjo at gmail dot org>"))
  :maintainer
  '("Takumi Kinjo <takumi dot kinjo at gmail dot org>")
  :url "https://github.com/kinjo/org-impress-js.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
