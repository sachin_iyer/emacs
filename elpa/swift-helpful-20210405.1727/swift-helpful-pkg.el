(define-package "swift-helpful" "20210405.1727" "Show documentation for Swift programs."
  '((emacs "25.1")
    (dash "2.12.0")
    (lsp-mode "6.0")
    (swift-mode "8.0.0"))
  :commit "ed36ea3d8cd80159f7f90b144c4503411b74ae3e" :authors
  '(("Daniel Martín" . "mardani29@yahoo.es"))
  :maintainer
  '("Daniel Martín" . "mardani29@yahoo.es")
  :keywords
  '("help" "swift")
  :url "https://github.com/danielmartin/swift-helpful")
;; Local Variables:
;; no-byte-compile: t
;; End:
