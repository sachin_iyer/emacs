;;; -*- no-byte-compile: t -*-
(define-package "ido-migemo" "20191017.1919" "Migemo plug-in for Ido" '((migemo "1.9.1")) :commit "09a2cc175b500cab7655a25ffc982e78d46ca669" :keywords '("files") :authors '(("myuhe <yuhei.maeda_at_gmail.com>")) :maintainer '("myuhe") :url "https://github.com/myuhe/ido-migemo.el")
