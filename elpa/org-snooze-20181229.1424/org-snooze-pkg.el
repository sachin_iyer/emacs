;;; -*- no-byte-compile: t -*-
(define-package "org-snooze" "20181229.1424" "Snooze your code, doc and feed" '((emacs "24.4")) :commit "8799adc14a20f3489063d279ff69312de3180bf9" :keywords '("extensions") :authors '(("Bill Xue")) :maintainer '("Bill Xue") :url "https://github.com/xueeinstein/org-snooze.el")
