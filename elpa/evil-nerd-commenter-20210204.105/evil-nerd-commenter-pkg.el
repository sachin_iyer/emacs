(define-package "evil-nerd-commenter" "20210204.105" "Comment/uncomment lines efficiently. Like Nerd Commenter in Vim"
  '((emacs "24.4"))
  :commit "8809dccfdbd7eb4cb34fa42794a80de5469e11d6" :authors
  '(("Chen Bin <chenbin DOT sh AT gmail.com>"))
  :maintainer
  '("Chen Bin <chenbin DOT sh AT gmail.com>")
  :keywords
  '("convenience" "evil")
  :url "http://github.com/redguardtoo/evil-nerd-commenter")
;; Local Variables:
;; no-byte-compile: t
;; End:
