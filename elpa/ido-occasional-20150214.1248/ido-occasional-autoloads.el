;;; ido-occasional-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ido-occasional" "ido-occasional.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from ido-occasional.el

(autoload 'with-ido-completion "ido-occasional" "\
Wrap FUN in another interactive function with ido completion.

\(fn FUN)" nil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ido-occasional" '("ido-occasional-completing-read")))

;;;***

;;;### (autoloads nil nil ("ido-occasional-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ido-occasional-autoloads.el ends here
