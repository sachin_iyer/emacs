;;; helm-taskswitch-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-taskswitch" "helm-taskswitch.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from helm-taskswitch.el

(autoload 'helm-taskswitch "helm-taskswitch" "\
Use helm to switch between tasks (X11 windows, buffers or recentf).

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-taskswitch" '("helm-taskswitch-")))

;;;***

;;;### (autoloads nil nil ("helm-taskswitch-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-taskswitch-autoloads.el ends here
