;;; -*- no-byte-compile: t -*-
(define-package "org-table-comment" "20120209.1851" "Org table comment modes." 'nil :commit "33b9966c33ecbc3e27cca67c2f2cdea04364d74e" :keywords '("org-mode" "orgtbl") :authors '(("Matthew L. Fidler <matthew dot fidler at gmail . com>")) :maintainer '("Matthew L. Fidler") :url "http://github.com/mlf176f2/org-table-comment.el")
