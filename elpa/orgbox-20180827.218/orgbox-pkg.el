;;; -*- no-byte-compile: t -*-
(define-package "orgbox" "20180827.218" "Mailbox-like task scheduling Org." '((org "8.0") (cl-lib "0.5")) :commit "3982f56efd67ec016389cad82ce5a44f619b36a9" :keywords '("org") :authors '(("Yasuhito Takamiya" . "yasuhito@gmail.com")) :maintainer '("Yasuhito Takamiya" . "yasuhito@gmail.com") :url "https://github.com/yasuhito/orgbox")
