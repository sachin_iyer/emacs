(define-package "php-mode" "20210430.1507" "Major mode for editing PHP code"
  '((emacs "25.2"))
  :commit "209913f8ce0f18898625b41e0094a99ce8accd0d" :authors
  '(("Eric James Michael Ritz"))
  :maintainer
  '("USAMI Kenta" . "tadsan@zonu.me")
  :keywords
  '("languages" "php")
  :url "https://github.com/emacs-php/php-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
