(define-package "solidity-mode" "20210331.1709" "Major mode for ethereum's solidity language" 'nil :commit "b83354943626ea7c50011d5806b17be17077d1c4" :authors
  '(("Lefteris Karapetsas " . "lefteris@refu.co"))
  :maintainer
  '("Lefteris Karapetsas " . "lefteris@refu.co")
  :keywords
  '("languages" "solidity"))
;; Local Variables:
;; no-byte-compile: t
;; End:
