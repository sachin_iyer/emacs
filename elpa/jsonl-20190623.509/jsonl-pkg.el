;;; -*- no-byte-compile: t -*-
(define-package "jsonl" "20190623.509" "Utility functions for working with line-delimited JSON" '((emacs "25")) :commit "3dd0b7bb2b4bce9f9de7367941f0cc78f82049c9" :keywords '("tools") :authors '(("Erik Anderson" . "erik@ebpa.link")) :maintainer '("Erik Anderson" . "erik@ebpa.link") :url "https://github.com/ebpa/jsonl.el")
