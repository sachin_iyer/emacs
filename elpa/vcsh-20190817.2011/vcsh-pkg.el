;;; -*- no-byte-compile: t -*-
(define-package "vcsh" "20190817.2011" "vcsh integration" '((emacs "25")) :commit "2051e4ee20709f82ab2396ab2ccfbe887a3c6a67" :keywords '("vc" "files") :authors '(("Štěpán Němec" . "stepnem@gmail.com")) :maintainer '("Štěpán Němec" . "stepnem@gmail.com") :url "https://gitlab.com/stepnem/vcsh-el")
