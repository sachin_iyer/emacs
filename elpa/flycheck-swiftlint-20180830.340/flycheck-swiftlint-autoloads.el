;;; flycheck-swiftlint-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "flycheck-swiftlint" "flycheck-swiftlint.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from flycheck-swiftlint.el

(autoload 'flycheck-swiftlint-setup "flycheck-swiftlint" "\
Setup Flycheck for Swiftlint.

\(fn)" t nil)

(autoload 'flycheck-swiftlint-autocorrect "flycheck-swiftlint" "\
Automatically fix Swiftlint errors.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "flycheck-swiftlint" '("flycheck-swiftlint-")))

;;;***

;;;### (autoloads nil nil ("flycheck-swiftlint-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; flycheck-swiftlint-autoloads.el ends here
