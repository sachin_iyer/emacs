;;; -*- no-byte-compile: t -*-
(define-package "request-deferred" "20181129.317" "Wrap request.el by deferred" '((deferred "0.3.1") (request "0.2.0")) :commit "f0aeeb5fc17ae270d9a109299edc48e8cf2bf2b6" :authors '(("Takafumi Arakaki <aka.tkf at gmail.com>")) :maintainer '("Takafumi Arakaki <aka.tkf at gmail.com>") :url "https://github.com/tkf/emacs-request")
