;;; -*- no-byte-compile: t -*-
(define-package "org-randomnote" "20190403.1633" "Find a random note in your Org-Mode files" '((f "0.19.0") (dash "2.12.0") (org "0")) :commit "f35a9d948751ad409aa057bfb68f1d008fdf9442" :authors '(("Michael Fogleman" . "michaelwfogleman@gmail.com")) :maintainer '("Michael Fogleman" . "michaelwfogleman@gmail.com") :url "http://github.com/mwfogleman/org-randomnote")
