;;; -*- no-byte-compile: t -*-
(define-package "ox-slack" "20181119.1131" "Slack Exporter for org-mode" '((org "9.1.4") (ox-gfm "1.0")) :commit "96d90914e6df1a0141657fc51f1dc5bb8f1da6bd" :keywords '("org" "slack" "outlines") :authors '(("Matt Price")) :maintainer '("Matt Price") :url "https://github.com/titaniumbones/ox-slack")
