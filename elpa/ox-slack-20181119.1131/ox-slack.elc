;ELC   
;;; Compiled
;;; in Emacs version 26.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\302\303\304\305\306$\207" [require ox-gfm org-export-define-derived-backend slack gfm :translate-alist ((bold . org-slack-bold) (code . org-slack-code) (headline . org-slack-headline) (inner-template . org-slack-inner-template) (italic . org-slack-italic) (link . org-slack-link) (plain-text . org-slack-plain-text) (src-block . org-slack-src-block) (strike-through . org-slack-strike-through) (timestamp . org-slack-timestamp))] 5)
#@164 Transcode TIMESTAMP element into Slack format.
CONTENTS is the timestamp contents. INFO is a plist used as a
ocmmunications channel.

(fn TIMESTAMP CONTENTS INFO)
(defalias 'org-slack-timestamp #[771 "\300\301!\"\207" [org-html-plain-text org-timestamp-translate] 6 (#$ . 868)])
#@164 Transcode HEADLINE element into Markdown format.
CONTENTS is the headline contents.  INFO is a plist used as
a communication channel.

(fn HEADLINE CONTENTS INFO)
(defalias 'org-slack-headline #[771 "\300\211;\203 \301\302#\266\202\202 \303A@\"\266\202?\205\270 \304\"\305\306\211;\2033 \301\302#\266\202\202; \303A@\"\266\202\"\303\307\"\205i \310\211;\203U \301\302#\266\202\202] \303A@\"\266\202\211\205g \305\"\311P\262\303\312\"\205\200 \313\"\211\205~ \314\315!P\262\303\316\"\205\253 \317\211;\203\231 \301\302#\266\202\202\241 \303A@\"\266\202\211\205\251 \320\321\"\262Q\320\322\n#\266\206\207" [:footnote-section-p get-text-property 0 plist-get org-export-get-relative-level org-export-data :title :with-todo-keywords :todo-keyword " " :with-tags org-export-get-tags "     " org-make-tag-string :with-priority :priority format "[#%c] " "*%s*\n\n%s"] 13 (#$ . 1156)])
#@160 Transcode LINK object into Markdown format.
  CONTENTS is the link's description.  INFO is a plist used as
  a communication channel.

(fn LINK CONTENTS INFO)
(defalias 'org-slack-link #[771 "\301\302\211;\203 \303\304#\266\202\202 \305A@\"\266\202\306\307#\206\310\211\310\235\203\352 \211\311\230\2035 \312\"\2029 \313\"\211\211:\204J \211;\205R \314\262\202T \211@9\205R \211@\262\211\315\267\202\263 !\204i \316\317\"\202o \316\320#\262\202\343 \316\321\322!\206\257 \323\"\203\222 \324\325\326\n\"\327#\202\257 \330\331\211;\203\244 \303\304#\266\202\202\254 \305A@\"\266\202\"\"\202\343 \322!\206\331 \332\"\211\204\307 \333\202\327 \211:\204\322 \325!\202\327 \324\325\327#\262\211\205\341 \316\321\"\262\262\262\202\310\334\"\203F\335\211;\203\303\304#\266\202\202\n\305A@\"\266\202\336\232\204\337Q\202$\340!\204!\211\202$\341!\262\330\342\343!!\"\316\344\322!\204;\202@\316\345#\"\266\202\202\310\211\346\230\203v\335\211;\203]\303\304#\266\202\202e\305A@\"\266\202\316\347\"\350\"\"\262\202\310\211\351\232\203\200\202\310\335\211;\203\221\303\304#\266\202\202\231\305A@\"\266\202\352\235\203\246\337Q\202\265\336\230\203\264\353!!\202\265\211\204\300\316\354\"\202\306\316\355#\266\202\207" [org-html-inline-image-rules #[257 "\300\301\"\227\302\230\203 \303!\304P\207\207" [file-name-extension "." ".org" file-name-sans-extension ".md"] 4 "\n\n(fn RAW-PATH)"] :type get-text-property 0 plist-get org-export-custom-protocol-maybe md ("custom-id" "id" "fuzzy") "fuzzy" org-export-resolve-fuzzy-link org-export-resolve-id-link plain-text #s(hash-table size 2 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (plain-text 90 headline 116)) format "%s>" "[%s](%s)" "[%s]" org-string-nw-p org-export-numbered-headline-p mapconcat number-to-string org-export-get-headline-number "." org-export-data :title org-export-get-ordinal nil org-export-inline-image-p :path "file" ":" file-name-absolute-p expand-file-name org-export-get-caption org-export-get-parent-element "![img](%s)" "%s \"%s\"" "coderef" org-export-get-coderef-format org-export-resolve-coderef "radio" ("http" "https" "ftp" "mailto") org-export-file-uri "%s" "*%s* (%s)"] 16 (#$ . 2085)])
#@159 Transcode VERBATIM from Org to Slack.
  CONTENTS is the text with bold markup. INFO is a plist holding
  contextual information.

(fn VERBATIM CONTENTS INFO)
(defalias 'org-slack-verbatim #[771 "\300\301\"\207" [format "`%s`"] 6 (#$ . 4397)])
#@136 Return a CODE object from Org to SLACK.
  CONTENTS is nil.  INFO is a plist holding contextual
  information.

(fn CODE CONTENTS INFO)
(defalias 'org-slack-code #[771 "\300\301\302\211;\203 \303\304#\266\202\202 \305A@\"\266\202\"\207" [format "`%s`" :value get-text-property 0 plist-get] 11 (#$ . 4648)])
#@158 Transcode italic from Org to SLACK.
  CONTENTS is the text with italic markup.  INFO is a plist holding
  contextual information.

(fn ITALIC CONTENTS INFO)
(defalias 'org-slack-italic #[771 "\300\301\"\207" [format "_%s_"] 6 (#$ . 4968)])
#@152 Transcode bold from Org to SLACK.
  CONTENTS is the text with bold markup.  INFO is a plist holding
  contextual information.

(fn BOLD CONTENTS INFO)
(defalias 'org-slack-bold #[771 "\300\301\"\207" [format "*%s*"] 6 (#$ . 5216)])
#@178 Transcode STRIKE-THROUGH from Org to SLACK.
  CONTENTS is text with strike-through markup.  INFO is a plist
  holding contextual information.

(fn STRIKE-THROUGH CONTENTS INFO)
(defalias 'org-slack-strike-through #[771 "\300\301\"\207" [format "~%s~"] 6 (#$ . 5456)])
#@189 Transcode an INLINE-SRC-BLOCK element from Org to SLACK.
  CONTENTS holds the contents of the item.  INFO is a plist holding
  contextual information.

(fn INLINE-SRC-BLOCK CONTENTS INFO)
(defalias 'org-slack-inline-src-block #[771 "\300\301\302\211;\203 \303\304#\266\202\202 \305A@\"\266\202\"\207" [format "`%s`" :value get-text-property 0 plist-get] 11 (#$ . 5732)])
#@167 Transcode SRC-BLOCK element into Github Flavored Markdown format.
  CONTENTS is nil. INFO is a plist used as a communication
  channel.

(fn SRC-BLOCK CONTENTS INFO)
(defalias 'org-slack-src-block #[771 "\300\211;\203 \301\302#\266\202\202 \303A@\"\266\202\304\"\305\306Q\207" [:language get-text-property 0 plist-get org-export-format-code-default "```\n" "```"] 10 (#$ . 6117)])
#@179 Transcode a QUOTE-BLOCK element from Org to SLACK.
  CONTENTS holds the contents of the block.  INFO is a plist
  holding contextual information.

(fn QUOTE-BLOCK CONTENTS INFO)
(defalias 'org-slack-quote-block #[771 "\300\301\302\"\"\207" [org-slack--indent-string plist-get :slack-quote-margin] 8 (#$ . 6517)])
#@174 Return body of document after converting it to Markdown syntax.
  CONTENTS is the transcoded contents string.  INFO is a plist
  holding export options.

(fn CONTENTS INFO)
(defalias 'org-slack-inner-template #[514 "\300\301!Q\207" ["\n" org-md--footnote-section] 6 (#$ . 6839)])
#@149 Transcode a TEXT string into Markdown format.
  TEXT is the string to transcode.  INFO is a plist holding
  contextual information.

(fn TEXT INFO)
(defalias 'org-slack-plain-text #[514 "\300\301\302#\262\300\303\304\305\211\306&\262\307\310\"\203  \300\311\312#\262\207" [replace-regexp-in-string "\n#" "\n\\\\#" "\\(!\\)\\[" "\\\\!" nil 1 plist-get :preserve-breaks "[ 	]*\n" "  \n"] 9 (#$ . 7128)])
#@1037 Export current buffer to a text buffer.

  If narrowing is active in the current buffer, only export its
  narrowed part.

  If a region is active, export that region.

  A non-nil optional argument ASYNC means the process should happen
  asynchronously.  The resulting buffer should be accessible
  through the `org-export-stack' interface.

  When optional argument SUBTREEP is non-nil, export the sub-tree
  at point, extracting information from the headline properties
  first.

  When optional argument VISIBLE-ONLY is non-nil, don't export
  contents of hidden elements.

  When optional argument BODY-ONLY is non-nil, strip title and
  table of contents from output.

  EXT-PLIST, when provided, is a property list with external
  parameters overriding Org default settings, but still inferior to
  file-local settings.

  Export is done in a buffer named "*Org SLACK Export*", which
  will be displayed when `org-export-show-temporary-export-buffer'
  is non-nil.

(fn &optional ASYNC SUBTREEP VISIBLE-ONLY BODY-ONLY EXT-PLIST)
(defalias 'org-slack-export-as-slack #[1280 "\300\301\302\303&\207" [org-export-to-buffer slack "*Org SLACK Export*" #[0 "\300 \207" [text-mode] 1]] 14 (#$ . 7547) nil])
#@918 Export current buffer to a text file.

  If narrowing is active in the current buffer, only export its
  narrowed part.

  If a region is active, export that region.

  A non-nil optional argument ASYNC means the process should happen
  asynchronously.  The resulting file should be accessible through
  the `org-export-stack' interface.

  When optional argument SUBTREEP is non-nil, export the sub-tree
  at point, extracting information from the headline properties
  first.

  When optional argument VISIBLE-ONLY is non-nil, don't export
  contents of hidden elements.

  When optional argument BODY-ONLY is non-nil, strip title and
  table of contents from output.

  EXT-PLIST, when provided, is a property list with external
  parameters overriding Org default settings, but still inferior to
  file-local settings.

  Return output file's name.

(fn &optional ASYNC SUBTREEP VISIBLE-ONLY BODY-ONLY EXT-PLIST)
(defalias 'org-slack-export-to-slack #[1280 "\300\301\"\302\303&\207" [org-export-output-file-name ".txt" org-export-to-file slack] 14 (#$ . 8771) nil])
#@84 Export region to slack, and copy to the kill ring for pasting into other programs.
(defalias 'org-slack-export-to-clipboard-as-slack #[0 "\302\211\303\304\305!!*\207" [org-export-with-toc org-export-with-smart-quotes nil kill-new org-export-as slack] 4 (#$ . 9860) nil])
(provide 'ox-slack)
