;;; Generated package description from org-tag-beautify.el  -*- no-byte-compile: t -*-
(define-package "org-tag-beautify" "20210304.1124" "Beautify Org Mode tags" '((emacs "26.1") (org-pretty-tags "0.2.2") (all-the-icons "4.0.0")) :commit "e655ced70140cbec8fe12f9207614ca2b3a6c37c" :keywords '("hypermedia") :url "https://github.com/stardiviner/org-tag-beautify")
