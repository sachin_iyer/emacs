;;; -*- no-byte-compile: t -*-
(define-package "company-edbi" "20160221.1923" "Edbi backend for company-mode" '((company "0.8.5") (edbi "0.1.3") (cl-lib "0.5.0") (s "1.9.0")) :commit "ffaeff75d0457285d16d11db772881542a6026ad" :authors '(("Artem Malyshev" . "proofit404@gmail.com")) :maintainer '("Artem Malyshev" . "proofit404@gmail.com") :url "https://github.com/proofit404/company-edbi")
