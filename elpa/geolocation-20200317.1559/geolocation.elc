;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\306\307\310\311\312\313\314\315\316\317&	\210\320\321\322\323\324DD\325\326\327\314\307&\210\320\330\322\323\331DD\332\326\333\314\307&\210\320\334\322\323\335DD\336\326\337\314\307&\207" [require auth-source json deferred request-deferred subr-x custom-declare-group geolocation nil "Get your location on Earth" :prefix "geolocation-" :group hardware :link (url-link :tag "Github" "https://github.com/gonewest818/geolocation.el") custom-declare-variable geolocation-api-vendor funcall function #[0 "\300\207" [:google] 1] "Select which third party geolocation API will be called." :type (radio (const :tag "Google Maps Geolocation API" :google) (const :tag "HERE Technologies Positioning API" :here) (const :tag "Unwired Labs Location API" :unwiredlabs)) geolocation-update-interval #[0 "\300\207" [300] 1] "Set the interval (in seconds) how often location is updated.\nThis setting has effect only when `geolocation-update-position'\nhas been called." (integer) geolocation-update-hook #[0 "\300\207" [nil] 1] "Hook functions to run when the location is updated.\n\nEach hook is a function taking no arguments.  When the hook is\ncalled, the value in `geolocation-location' will have been\nupdated recently.  Note, we make no attempt to track movement or\ndistance from the last position.  To get movement, the hook\nitself will need to have saved the previous coordinates and\ncompute the distance between the previous and new." (repeat symbol)] 10)
#@337 The most recently scanned location.

Value is nil if the position is unknown.  Otherwise the value is
an alist with the following keys:
  latitude  : latitude of the current position
  longitude : longitude of the current position
  accuracy  : accuracy of the estimate in meters
  timestamp : timestamp when this position was obtained
(defvar geolocation-location nil (#$ . 1943))
#@133 Control debug output to *Messages* buffer.
Set 0 to disable all output, 1 for basic output, or a larger
integer for more verbosity.
(defvar geolocation-debug-messages 0 (#$ . 2332))
#@97 Print debug message at verbosity V, filling format string FMT with ARGS.

(fn V FMT &rest ARGS)
(defalias 'geolocation--dbg #[642 "Y\205 \301\302#\207" [geolocation-debug-messages apply message] 7 (#$ . 2520)])
#@684 Invoke COMMAND in a shell, run PARSER, and optionally pass to CALLBACK.
The command and parser will run in a deferred chain that does not
block the calling thread.  This function returns a deferred
object suitable for further chaining.

Within the deferred chain the PARSER will be invoked with a
single argument which is the buffer containing the output of
COMMAND.  It is the responsibility of PARSER to delete the buffer
when done.

If a CALLBACK is provided, it will be invoked with the output of
the PARSER.  It is the responsibility of the CALLBACK to return
the value it was passed, so that further callbacks can be added
to the chain.

(fn COMMAND PARSER &optional CALLBACK)
(defalias 'geolocation--shell-command-async #[770 "\300\301\302!\262\303!\262\304\"\262\304\305\"\262\306\307\"\262\304\310\311\312\313\314!\315\"\316\317%\"\262\304\320\"\262\211\207" [nil deferred:next #[0 "\300\301\302\"\207" [geolocation--dbg 1 "geolocation--shell-command-async:start"] 3] deferred:process-shell-buffer deferred:nextc #[257 "\300\301\"\207" [sort #[514 "\300\301\"\300\301\"V\207" [alist-get signal] 6 "\n\n(fn I J)"]] 4 "\n\n(fn X)"] deferred:error #[257 "\300\301\302#\210\303\207" [geolocation--dbg 0 "geolocation--shell-command-async:error: %s" nil] 5 "\n\n(fn ERR)"] make-byte-code 257 "\211\203\f \300\203\f \300!\207\207" vconcat vector [] 3 "\n\n(fn X)" #[257 "\300\301\302\"\210\207" [geolocation--dbg 1 "geolocation--shell-command-async:end"] 4 "\n\n(fn X)"]] 12 (#$ . 2743)])
#@68 Append a timestamp to the alist stored in LOCATION.

(fn LOCATION)
(defalias 'geolocation--timestamp #[257 "\300\301 BB\207" [timestamp float-time] 3 (#$ . 4261)])
(byte-code "\300\301\302\303\304\305\306\307&\210\310\311\312\313\314DD\315\316\317\306\301&\210\310\320\312\313\321DD\322\316\323\306\301&\207" [custom-declare-group geolocation-system-osx nil "Mac OSX specific settings for the geolocation library" :prefix "geolocation-system-osx-" :group geolocation custom-declare-variable geolocation-system-osx-airport-path funcall function #[0 "\300\207" ["/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/"] 1] "Path to the Apple 'airport' binary on Mac OSX.\n\nWe need to specify this because the utility is not in a standard\nlocation.  It's unlikely users need to customize this, unless\nApple changes the location of the utility." :type (string) geolocation-system-osx-airport-command #[0 "\300\207" [#1="airport --scan | cut -c 34-63 | tail -n +2"] 1 #1#] "Command line with arguments needed to invoke airport." (string)] 8)
#@63 Parse output from the airport command in BUFFER.

(fn BUFFER)
(defalias 'geolocation--osx-parse-wifi #[257 "\300rq\210eb\210m\204\" \301\302\303\304\305`\306 {!$B\262\307\310!\210\202 )\311!\210\211\207" [nil cl-mapcar #[771 "\300=\203 \301!\202 B\207" [int string-to-number] 6 "\n\n(fn C K V)"] (str int str) (bssid signal channel) split-string point-at-eol beginning-of-line 2 kill-buffer] 9 (#$ . 5339)])
#@152 Scan wifi asynchronously, and optionally call CALLBACK with result.
Return a deferred object for chaining further operations.

(fn &optional CALLBACK)
(defalias 'geolocation--osx-scan-wifi #[256 "\302	P\303#\207" [geolocation-system-osx-airport-path geolocation-system-osx-airport-command geolocation--shell-command-async geolocation--osx-parse-wifi] 5 (#$ . 5765)])
(byte-code "\300\301\302\303\304\305\306\307&\210\310\311\312\313\314DD\315\316\317\306\301&\207" [custom-declare-group geolocation-system-windows nil "Windows-specific settings for the geolocation library" :prefix "geolocation-system-windows-" :group geolocation custom-declare-variable geolocation-system-windows-netsh-command funcall function #[0 "\300\207" [#1="netsh wlan show networks mode=bssid"] 1 #1#] "Command line with arguments needed to invoke netsh." :type (string)] 8)
#@39 Search via regexp for the next BSSID.
(defalias 'geolocation--windows-bssid #[0 "\300\301\302\303#\205 \304\305!\207" [re-search-forward "\\s-*BSSID\\s-[0-9]+\\s-*:\\s-\\([a-f0-9:]+\\)" nil t match-string-no-properties 1] 4 (#$ . 6626)])
#@46 Search via regexp for the next Signal value.
(defalias 'geolocation--windows-signal #[0 "\300\301\302\303#\205 \304\305!\306!\307\245\310Z\262\207" [re-search-forward "\\s-*Signal\\s-*:\\s-\\([0-9:]+\\)" nil t match-string-no-properties 1 string-to-number 2.0 100] 4 (#$ . 6871)])
#@48 Search via regexp for the next Channel number.
(defalias 'geolocation--windows-channel #[0 "\300\301\302\303#\205 \304\305!\207" [re-search-forward "\\s-*Channel\\s-*:\\s-\\([0-9:]+\\)" nil t match-string-no-properties 1] 4 (#$ . 7161)])
#@61 Parse output from the netsh command in BUFFER.

(fn BUFFER)
(defalias 'geolocation--windows-parse-wifi #[257 "\300rq\210eb\210m\2047 \301 \302 \303 \203/ \203/ \211\203/ \304B\305B\306BEB\262\2022 db\210\266\202 )\307!\210\211\207" [nil geolocation--windows-bssid geolocation--windows-signal geolocation--windows-channel bssid signal channel kill-buffer] 9 (#$ . 7406)])
#@152 Scan wifi asynchronously, and optionally call CALLBACK with result.
Return a deferred object for chaining further operations.

(fn &optional CALLBACK)
(defalias 'geolocation--windows-scan-wifi #[256 "\301\302#\207" [geolocation-system-windows-netsh-command geolocation--shell-command-async geolocation--windows-parse-wifi] 5 (#$ . 7795)])
(byte-code "\300\301\302\303\304\305\306\307&\210\310\311\312\313\314DD\315\316\317\306\301&\207" [custom-declare-group geolocation-system-linux nil "Linux-specific settings for the geolocation library" :prefix "geolocation-system-linux-" :group geolocation custom-declare-variable geolocation-system-linux-dbus-command funcall function #[0 "\300\207" [#1="echo not implemented"] 1 #1#] "Command line with arguments needed to query dbus to scan wifi." :type (string)] 8)
#@70 Wifi scanning on Linux not yet implemented.

(fn &optional CALLBACK)
(defalias 'geolocation--linux-scan-wifi #[256 "\300\301!\207" [error "Wifi scanning on Linux not yet implemented"] 3 (#$ . 8615)])
(byte-code "\300\301\302\303\304\305\306\307&\210\310\311\312\313\314DD\315\316\317\306\301&\210\310\320\312\313\321DD\322\316\323\306\301&\210\310\324\312\313\325DD\326\316\327\306\301&\210\310\330\312\313\331DD\332\316\333\306\301&\207" [custom-declare-group geolocation-api-google nil "Configuration needed to call the Google Maps Geolocation API" :prefix "geolocation-api-google-" :group geolocation custom-declare-variable geolocation-api-google-url funcall function #[0 "\300\207" [#1="https://www.googleapis.com/geolocation/v1/geolocate"] 1 #1#] "URL for the Google Maps Geolocation API." :type (string) geolocation-api-google-token #[0 "\300\207" [nil] 1] "Authorization token for the Google Geolocation API.\n\nIMPORTANT NOTE: This customization is offered for the convenience\nof people who want to quickly set up and test this package.\nHowever you are strongly discouraged from leaving authorization\ntokens (which are like passwords) in your Emacs configurations as\nplainly readable text.  Setting this variable to nil will cause\nthe library to retrieve your token via `auth-source' instead." (choice (const :tag "Retrieve token from `auth-source'" nil) (string :tag "Google API token")) geolocation-api-google-auth-source-host #[0 "\300\207" [#2="googleapis.com"] 1 #2#] "The host name used for lookups in `auth-source'." (string) geolocation-api-google-auth-source-user #[0 "\300\207" [#3="geolocation.el"] 1 #3#] "The user name used for lookups in `auth-source'." (string)] 8)
#@271 Transform WIFI list into the format needed for Google's API.

In particular Google wants the json payload to contain keys
"macAddress", "signalStrength" and "channel" whereas the
wifi scanning functions in the library produce different keys for
those fields.

(fn WIFI)
(defalias 'geolocation--google-xform-wifi #[257 "\300\301\"\207" [mapcar #[257 "\300\301\302\"B\303\301\304\"B\305\301\305\"BE\207" [macAddress alist-get bssid signalStrength signal channel] 7 "\n\n(fn X)"]] 4 (#$ . 10322)])
#@83 Transform the Google API response RESPONSE into the format needed.

(fn RESPONSE)
(defalias 'geolocation--google-xform-location #[257 "\301!>\204 \302\303\304D\"\210\211\305H\306\307\"\306\310\"\306\311\"\306\312\"\313B\314B\312BE\207" [cl-struct-request-response-tags type-of signal wrong-type-argument request-response 3 alist-get location lat lng accuracy latitude longitude] 10 (#$ . 10827)])
#@261 Resolve the Google API token.

If `geolocation-google-token' is non-nil, then use that.
Otherwise, retrieve the token via `auth-source-search' under the
hostname `geolocation-api-google-auth-source-host' and username
`geolocation-api-google-auth-source-user'.
(defalias 'geolocation--google-get-token #[0 "\206\n \303\304	\305\n$\207" [geolocation-api-google-token geolocation-api-google-auth-source-host geolocation-api-google-auth-source-user auth-source-pick-first-password :host :user] 5 (#$ . 11242)])
#@530 Request location from Google API using wifi data in the deferred WD.
Send results to a CALLBACK which is expected to store the
resulting location data.  Return a deferred object.

The implementation is a chain of deferreds with the following
steps executing on a separate thread:
  - transform wifi into the format Google wants
  - call the Google API with that wifi data
  - transform the Google response into the alist object we need
  - attach a timestamp to the alist
  - invoke the callback with the alist

(fn WD CALLBACK)
(defalias 'geolocation--call-google-api #[514 "\300 \301\302\303\"\262\302\304\305\306\307\310!\311\"\312\313%\"\262\302\314\"\262\302\315\"\262\302\304\305\316\307\310	!\317\"\320\321%\"\262\211\262\207" [geolocation--google-get-token nil deferred:nextc #[257 "\300!\207" [geolocation--google-xform-wifi] 3 "\n\n(fn WIFI)"] make-byte-code 257 "\302\303\304\"\210\305	\306\307\310\311\300BC\312\313\314	BC!\315\316\317\320&\207" vconcat vector [geolocation-api-google-url geolocation--dbg 1 "geolocation--call-google-api:request" request-deferred :type "POST" :params "key" :data json-encode "wifiAccessPoints" :parser json-read :timeout 15] 13 "\n\n(fn WIFI)" #[257 "\301!>\204 \302\303\304D\"\210\211\305H\306U\203 \307!\207\310\311\312\301!>\204. \302\303\304D\"\210\313H#\210\310\311\314\301!>\204F \302\303\304D\"\210\305H#\210\315\207" [cl-struct-request-response-tags type-of signal wrong-type-argument request-response 1 200 geolocation--google-xform-location geolocation--dbg 0 "geolocation--call-google-api:error-thrown: %s" 4 "geolocation--call-google-api:status-code: %s" nil] 8 "\n\n(fn RESPONSE)"] #[257 "\211\205 \300!\207" [geolocation--timestamp] 3 "\n\n(fn LOCATION)"] "\211\205 \300!\207" [] 3 "\n\n(fn LOCATION)"] 12 (#$ . 11757)])
(byte-code "\300\301\302\303\304\305\306\307&\210\310\311\312\313\314DD\315\316\317\306\301&\210\310\320\312\313\321DD\322\316\323\306\301&\210\310\324\312\313\325DD\326\316\327\306\301&\210\310\330\312\313\331DD\332\316\333\306\301&\207" [custom-declare-group geolocation-api-here nil "Configuration needed to call the HERE Technologies Positioning API" :prefix "geolocation-api-here-" :group geolocation custom-declare-variable geolocation-api-here-url funcall function #[0 "\300\207" [#1="https://pos.ls.hereapi.com/positioning/v1/locate"] 1 #1#] "URL for the HERE Technologies Positioning API." :type (string) geolocation-api-here-token #[0 "\300\207" [nil] 1] "Authorization token for the HERE Technologies Positioning API.\n\nIMPORTANT NOTE: This customization is offered for the convenience\nof people who want to quickly set up and test this package.\nHowever you are strongly discouraged from leaving authorization\ntokens (which are like passwords) in your Emacs configurations as\nplainly readable text.  Setting this variable to nil will cause\nthe library to retrieve your token via `auth-source' instead." (choice (const :tag "Retrieve token from `auth-source'" nil) (string :tag "HERE Technologies API token")) geolocation-api-here-auth-source-host #[0 "\300\207" [#2="pos.ls.hereapi.com"] 1 #2#] "The host name used for lookups in `auth-source'." (string) geolocation-api-here-auth-source-user #[0 "\300\207" [#3="geolocation.el"] 1 #3#] "The user name used for lookups in `auth-source'." (string)] 8)
#@181 Transform WIFI list into the format needed for HERE's API.

In particular HERE wants the json payload to contain just the
"mac" address, and "powrx" or signal strength.

(fn WIFI)
(defalias 'geolocation--here-xform-wifi #[257 "\300\301\"\207" [mapcar #[257 "\300\301\302\"B\303\301\304\"BD\207" [mac alist-get bssid powrx signal] 6 "\n\n(fn X)"]] 4 (#$ . 15105)])
#@79 Transform HERE's API response RESPONSE into the format needed.

(fn RESPONSE)
(defalias 'geolocation--here-xform-location #[257 "\301!>\204 \302\303\304D\"\210\211\305H\306\307\"\306\310\"\306\311\"\306\312\"\313B\314B\312BE\207" [cl-struct-request-response-tags type-of signal wrong-type-argument request-response 3 alist-get location lat lng accuracy latitude longitude] 10 (#$ . 15478)])
#@266 Resolve the HERE Technologies API token.

If `geolocation-here-token' is non-nil, then use that.
Otherwise, retrieve the token via `auth-source-search' under the
hostname `geolocation-api-here-auth-source-host' and username
`geolocation-api-here-auth-source-user'.
(defalias 'geolocation--here-get-token #[0 "\206\n \303\304	\305\n$\207" [geolocation-api-here-token geolocation-api-here-auth-source-host geolocation-api-here-auth-source-user auth-source-pick-first-password :host :user] 5 (#$ . 15887)])
#@522 Request location from HERE API using wifi data in the deferred WD.
Send results to a CALLBACK which is expected to store the
resulting location data.  Return a deferred object.

The implementation is a chain of deferreds with the following
steps executing on a separate thread:
  - transform wifi into the format HERE wants
  - call the HERE API with that wifi data
  - transform the HERE response into the alist object we need
  - attach a timestamp to the alist
  - invoke the callback with the alist

(fn WD CALLBACK)
(defalias 'geolocation--call-here-api #[514 "\300 \301\302\303\"\262\302\304\305\306\307\310!\311\"\312\313%\"\262\302\314\"\262\302\315\"\262\302\304\305\316\307\310	!\317\"\320\321%\"\262\211\262\207" [geolocation--here-get-token nil deferred:nextc #[257 "\300!\207" [geolocation--here-xform-wifi] 3 "\n\n(fn WIFI)"] make-byte-code 257 "\302\303\304\"\210\305	\306\307\310\311\300BC\312\313\314\315\316BC!\317\320\321\322&\207" vconcat vector [geolocation-api-here-url geolocation--dbg 1 "geolocation--call-here-api:request" request-deferred :type "POST" :params "apiKey" :headers (("Content-Type" . "application/json")) :data json-encode "wlan" :parser json-read :timeout 15] 15 "\n\n(fn WIFI)" #[257 "\301!>\204 \302\303\304D\"\210\211\305H\306U\203 \307!\207\310\311\312\301!>\204. \302\303\304D\"\210\313H#\210\310\311\314\301!>\204F \302\303\304D\"\210\305H#\210\315\207" [cl-struct-request-response-tags type-of signal wrong-type-argument request-response 1 200 geolocation--here-xform-location geolocation--dbg 0 "geolocation--call-here-api:error-thrown: %s" 4 "geolocation--call-here-api:status-code: %s" nil] 8 "\n\n(fn RESPONSE)"] #[257 "\211\205 \300!\207" [geolocation--timestamp] 3 "\n\n(fn LOCATION)"] "\211\205 \300!\207" [] 3 "\n\n(fn LOCATION)"] 12 (#$ . 16399)])
(byte-code "\300\301\302\303\304\305\306\307&\210\310\311\312\313\314DD\315\316\317\306\301&\210\310\320\312\313\321DD\322\316\323\306\301&\210\310\324\312\313\325DD\326\316\327\306\301&\210\310\330\312\313\331DD\332\316\333\306\301&\207" [custom-declare-group geolocation-api-unwiredlabs nil "Configuration needed to call the Unwired Labs Geolocation API." :prefix "geolocation-api-unwiredlabs-" :group geolocation custom-declare-variable geolocation-api-unwiredlabs-url funcall function #[0 "\300\207" [#1="https://us2.unwiredlabs.com/v2/process.php"] 1 #1#] "URL for the Unwired Labs Geolocation API." :type (choice (const :tag "US Virginia" "https://us1.unwiredlabs.com/v2/process.php") (const :tag "US San Francisco" "https://us2.unwiredlabs.com/v2/process.php") (const :tag "EU France" "https://eu1.unwiredlabs.com/v2/process.php") (const :tag "AP Singapore" "https://ap1.unwiredlabs.com/v2/process.php") (string :tag "Other")) geolocation-api-unwiredlabs-token #[0 "\300\207" [nil] 1] "Authorization token for the Unwired Labs Geolocation API.\n\nIMPORTANT NOTE: This customization is offered for the convenience\nof people who want to quickly set up and test this package.\nHowever you are strongly discouraged from leaving authorization\ntokens (which are like passwords) in your Emacs configurations as\nplainly readable text.  Setting this variable to nil will cause\nthe library to retrieve your token via `auth-source' instead." (choice (const :tag "Retrieve token from `auth-source'" nil) (string :tag "Unwired Labs API token")) geolocation-api-unwiredlabs-auth-source-host #[0 "\300\207" [#2="unwiredlabs.com"] 1 #2#] "The host name used for lookups in `auth-source'." (string) geolocation-api-unwiredlabs-auth-source-user #[0 "\300\207" [#3="geolocation.el"] 1 #3#] "The user name used for lookups in `auth-source'." (string)] 8)
#@87 Transform Unwired Labs's API response RESPONSE into the format needed.

(fn RESPONSE)
(defalias 'geolocation--unwiredlabs-xform-location #[257 "\301!>\204 \302\303\304D\"\210\211\305H\306\307\"\306\310\"\306\311\"\312B\313B\311BE\207" [cl-struct-request-response-tags type-of signal wrong-type-argument request-response 3 alist-get lat lon accuracy latitude longitude] 9 (#$ . 20098)])
#@280 Resolve the Unwired Labs API token.

If `geolocation-api-unwiredlabs-token' is non-nil, then use that.
Otherwise, retrieve the token via `auth-source-search' under the
hostname `geolocation--unwiredlabs-auth-source-host' and username
`geolocation--unwiredlabs-auth-source-user'.
(defalias 'geolocation--unwiredlabs-get-token #[0 "\206\n \303\304	\305\n$\207" [geolocation-api-unwiredlabs-token geolocation-api-unwiredlabs-auth-source-host geolocation-api-unwiredlabs-auth-source-user auth-source-pick-first-password :host :user] 5 (#$ . 20501)])
#@558 Request location from Unwired Labs API using wifi data in the deferred WD.
Send results to a CALLBACK which is expected to store the
resulting location data.  Return a deferred object.

The implementation is a chain of deferreds with the following steps
executing on a separate thread:
  - (unlike other APIs, no need to transform the wifi data)
  - call the Unwired API with the supplied wifi data
  - transform the Unwired response into the alist object we need
  - attach a timestamp to the alist
  - invoke the callback with the alist

(fn WD CALLBACK)
(defalias 'geolocation--call-unwiredlabs-api #[514 "\300 \301\302\303\304\305\306\307!\310\"\311\312%\"\262\302\313\"\262\302\314\"\262\302\303\304\315\306\307	!\316\"\317\320%\"\262\211\262\207" [geolocation--unwiredlabs-get-token nil deferred:nextc make-byte-code 257 "\302\303\304\"\210\305	\306\307\310\311\312\300B\313BD!\314\315\316\317&	\207" vconcat vector [geolocation-api-unwiredlabs-url geolocation--dbg 1 "geolocation--call-unwiredlabs-api:request" request-deferred :type "POST" :data json-encode "token" "wifi" :parser json-read :timeout 15] 11 "\n\n(fn WIFI)" #[257 "\301!>\204 \302\303\304D\"\210\211\305H\306U\203 \307!\207\310\311\312\301!>\204. \302\303\304D\"\210\313H#\210\310\311\314\301!>\204F \302\303\304D\"\210\305H#\210\315\207" [cl-struct-request-response-tags type-of signal wrong-type-argument request-response 1 200 geolocation--unwiredlabs-xform-location geolocation--dbg 0 "geolocation--call-unwiredlabs-api:error-thrown: %s" 4 "geolocation--call-unwiredlabs-api:status-code: %s" nil] 8 "\n\n(fn RESPONSE)"] #[257 "\211\205 \300!\207" [geolocation--timestamp] 3 "\n\n(fn LOCATION)"] "\211\205 \300!\207" [] 3 "\n\n(fn LOCATION)"] 12 (#$ . 21055)])
#@151 Scan wifi asynchronously and call (optional) CALLBACK with result.
Return a deferred object for chaining further operations.

(fn &optional CALLBACK)
(defalias 'geolocation-scan-wifi #[256 "\301\230\203\n \302!\207\303\230\203 \304!\207\305\230\205 \306!\207" [system-type "darwin" geolocation--osx-scan-wifi "windows-nt" geolocation--windows-scan-wifi "gnu/linux" geolocation--linux-scan-wifi] 3 (#$ . 22833)])
#@432 Get a position in terms of latitude and longitude.
Return a deferred object for chaining further operations.  The
position is sent to CALLBACK as an alist with a structure
identical to `geolocation-location':
  latitude  : latitude of the current position
  longitude : longitude of the current position
  accuracy  : accuracy of the estimate in meters
  timestamp : timestamp when this position was found

(fn &optional CALLBACK)
(defalias 'geolocation-get-position #[256 "\301 \302\267\202 \303\"\202 \304\"\202 \305\"\202 \306\207" [geolocation-api-vendor geolocation-scan-wifi #s(hash-table size 3 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (:google 8 :here 15 :unwiredlabs 22)) geolocation--call-google-api geolocation--call-here-api geolocation--call-unwiredlabs-api nil] 5 (#$ . 23260)])
#@115 Update `calendar-latitude' and `calendar-longitude'.
This function is intended to be a `geolocation-update-hook'.
(defalias 'geolocation-update-calendar #[0 "\303\304\"\303\305\"\211\207" [geolocation-location calendar-latitude calendar-longitude alist-get latitude longitude] 3 (#$ . 24094)])
#@105 Update `geolocation-location' to position P.
Then call the `geolocation-update-hook' functions.

(fn P)
(defalias 'geolocation--update-position-callback #[257 "\211\302\303\304#\210	\211\205 \211@\211 \210A\266\202\202	 \207" [geolocation-location geolocation-update-hook geolocation--dbg 1 "geolocation-location: %s"] 5 (#$ . 24399)])
#@144 Controls the `geolocation--update-position-loop' iteration.
This var is managed by `geolocation-update-position' and need not
be set directly.
(defvar geolocation--update-active nil (#$ . 24746))
#@296 Call `geolocation-get-position' in a loop.
After each update store the result in `geolocation-location' and
run the `geolocation-update-hook' functions.  The loop interval
is controlled by `geolocation-update-interval', which can be
updated on the fly without stopping and restarting this loop.
(defalias 'geolocation--update-position-loop #[0 "\301\302\303\304\"\262\305\306_!\262\307\310\"\262\211\207" [geolocation-update-interval nil deferred:call geolocation-get-position geolocation--update-position-callback deferred:wait 1000 deferred:nextc #[0 "\205 \301 \207" [geolocation--update-active geolocation--update-position-loop] 1]] 4 (#$ . 24949)])
#@126 Start polling and updating the current position.
With a prefix argument ARG, stop after the next update.

(fn &optional ARG)
(defalias 'geolocation-update-position #[256 "\211?\211\203 \203 \301\202 \211\203 \301\302 \210\301\202 \303\211\207" [geolocation--update-active t geolocation--update-position-loop nil] 4 (#$ . 25617) "P"])
(provide 'geolocation)
