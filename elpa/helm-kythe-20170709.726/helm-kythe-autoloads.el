;;; helm-kythe-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-kythe" "helm-kythe.el" (0 0 0 0))
;;; Generated autoloads from helm-kythe.el

(autoload 'helm-kythe-mode "helm-kythe" "\
Toggle Helm-Kythe mode on or off.
With a prefix argument ARG, enable Helm-Kythe mode if ARG is
positive, and disable it otherwise.  If called from Lisp, enable
the mode if ARG is omitted or nil, and toggle it if ARG is `toggle'.
\\{helm-kythe-mode-map}

\(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-kythe" '("helm-kythe-")))

;;;***

;;;### (autoloads nil nil ("helm-kythe-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-kythe-autoloads.el ends here
