;;; helm-js-codemod-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-js-codemod" "helm-js-codemod.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from helm-js-codemod.el

(autoload 'helm-js-codemod "helm-js-codemod" "\
`helm-js-codemod' entry point to run codemod on current line or seleted region.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-js-codemod" '("helm-js-codemod-")))

;;;***

;;;### (autoloads nil nil ("helm-js-codemod-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-js-codemod-autoloads.el ends here
