;ELC   
;;; Compiled
;;; in Emacs version 27.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\302\303\304\305\306\307%\210\310\311\312\313\314DD\315\316\317\306\303&\210\310\320\312\313\321DD\322\316\323\306\303&\210\310\324\312\313\325DD\326\316\323\306\303&\207" [require org-element custom-declare-group org-elp nil "org-elp customizable variables." :group org custom-declare-variable org-elp-buffer-name funcall function #[0 "\300\207" [#1="*Equation Live Preview*"] 1 #1#] "Buffer name used to show the previewed equation." :type string org-elp-split-fraction #[0 "\300\207" [0.2] 1] "Fraction of the window taken up by the previewing buffer." float org-elp-idle-time #[0 "\300\207" [0.5] 1] "Idle time after editing to wait before creating the fragment."] 8)
#@48 A variable that keeps track of the idle timer.
(defvar org-elp--timer nil (#$ . 1108))
#@39 Buffer used for previewing equations.
(defvar org-elp--preview-buffer nil (#$ . 1201))
#@75 Preview the equation at point in buffer defined in `org-elp-buffer-name'.
(defalias 'org-elp--preview #[0 "\302 \211\211:\204 \211;\205 \303\262\202 \211@9\205 \211@\262\304>\205m \305\211;\2033 \306\307#\266\202\202; \310A@\"\266\202\311\211;\203L \306\307#\266\202\202T \310A@\"\266\202\312\"rq\210\313\314 \210\315\316\317#c\210\320 *\266\203\207" [org-elp--preview-buffer inhibit-read-only org-element-context plain-text (latex-environment latex-fragment) :begin get-text-property 0 plist-get :end buffer-substring-no-properties t erase-buffer replace-regexp-in-string "\n$" "" org-latex-preview] 8 (#$ . 1294)])
#@26 Open the preview buffer.
(defalias 'org-elp--open-buffer #[0 "\302\303\304Z\305 _!!\210\306\304!\210\307	!\210\310 \210\306\311!\207" [org-elp-split-fraction org-elp-buffer-name split-window-vertically floor 1 window-height other-window switch-to-buffer special-mode -1] 4 (#$ . 1938)])
#@44 Activate previewing buffer and idle timer.
(defalias 'org-elp-activate #[0 "\304\305!\210\306!\307 \210\310\n\311\312#\211\207" [org-elp-buffer-name org-elp--preview-buffer org-elp-idle-time org-elp--timer message "Activating org-elp" get-buffer-create org-elp--open-buffer run-with-idle-timer t org-elp--preview] 4 (#$ . 2232) nil])
#@50 Deactivate previewing and remove the idle timer.
(defalias 'org-elp-deactivate #[0 "rq\210\301 \210)\302\303!\210\304\305!\207" [org-elp--preview-buffer kill-buffer-and-window message "Deactivating org-elp" cancel-function-timers org-elp--preview] 2 (#$ . 2575) nil])
#@93 Non-nil if Org-Elp mode is enabled.
Use the command `org-elp-mode' to change this variable.
(defvar org-elp-mode nil (#$ . 2850))
(make-variable-buffer-local 'org-elp-mode)
#@312 org-elp mode: display latex fragment while typing.

If called interactively, enable Org-Elp mode if ARG is positive,
and disable it if ARG is zero or negative.  If called from Lisp,
also enable the mode if ARG is omitted or nil, and toggle it if
ARG is `toggle'; disable the mode otherwise.

(fn &optional ARG)
(defalias 'org-elp-mode #[256 "\301 \302=\203 ?\202 \303!\304V\211\203 \305 \210\202  \306 \210\307\310\203* \311\202+ \312\"\210\313\314!\203O \301 \203? \211\301 \232\203O \315\316\317\203J \320\202K \321#\266\210\322 \210\207" [org-elp-mode current-message toggle prefix-numeric-value 0 org-elp-activate org-elp-deactivate run-hooks org-elp-mode-hook org-elp-mode-on-hook org-elp-mode-off-hook called-interactively-p any " in current buffer" message "Org-Elp mode %sabled%s" "en" "dis" force-mode-line-update] 8 (#$ . 3030) (byte-code "\206 \301C\207" [current-prefix-arg toggle] 1)])
(defvar org-elp-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\300!\205 \311\211%\210\312\313!\207" [org-elp-mode-map org-elp-mode-hook variable-documentation put "Hook run after entering or leaving `org-elp-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode org-elp-mode " org-elp" boundp nil provide org-elp] 6)
