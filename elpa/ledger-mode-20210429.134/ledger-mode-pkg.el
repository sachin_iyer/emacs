(define-package "ledger-mode" "20210429.134" "Helper code for use with the \"ledger\" command-line tool"
  '((emacs "25.1"))
  :commit "58a2bf57af9289daeaac6892fa4008ea8255b205")
;; Local Variables:
;; no-byte-compile: t
;; End:
