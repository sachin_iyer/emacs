;;; -*- no-byte-compile: t -*-
(define-package "org-kindle" "20190315.439" "Send org link file to ebook reader." '((emacs "25") (cl-lib "0.5") (seq "2.20")) :commit "612a2894bbbff8a6cf54709d591fee86005755de" :keywords '("org" "link" "ebook" "kindle" "epub" "mobi") :url "https://github.com/stardiviner/org-kindle")
