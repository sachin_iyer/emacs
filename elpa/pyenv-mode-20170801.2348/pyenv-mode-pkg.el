;;; -*- no-byte-compile: t -*-
(define-package "pyenv-mode" "20170801.2348" "Integrate pyenv with python-mode" '((pythonic "0.1.0")) :commit "123facbaca9aa53dbf990348ea2780cbbdd0f96a" :authors '(("Artem Malyshev" . "proofit404@gmail.com")) :maintainer '("Artem Malyshev" . "proofit404@gmail.com") :url "https://github.com/proofit404/pyenv-mode")
