;;; helm-rhythmbox-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-rhythmbox" "helm-rhythmbox.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from helm-rhythmbox.el

(autoload 'helm-rhythmbox "helm-rhythmbox" "\
Choose a song from the Rhythmbox library to play or enqueue.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-rhythmbox" '("helm-")))

;;;***

;;;### (autoloads nil nil ("helm-rhythmbox-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-rhythmbox-autoloads.el ends here
