;;; Generated package description from emoji-display.el  -*- no-byte-compile: t -*-
(define-package "emoji-display" "20140117.1013" "emoji displaying module" 'nil :commit "bb4217f6400151a9cfa6d4524b8427f01feb5193" :authors '(("Kazuhiro Ito" . "kzhr@d1.dion.ne.jp")) :maintainer '("Kazuhiro Ito" . "kzhr@d1.dion.ne.jp") :keywords '("emoji") :url "https://github.com/ikazuhiro/emoji-display")
