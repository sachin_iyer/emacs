(define-package "emms" "20210429.2204" "The Emacs Multimedia System"
  '((cl-lib "0.5")
    (nadvice "0.3")
    (seq "0"))
  :commit "6958871ce8748cb7d39812f4de53116c1c32220c" :authors
  '(("Jorgen Schäfer" . "forcer@forcix.cx"))
  :maintainer
  '("Yoni Rabkin" . "yrk@gnu.org")
  :keywords
  '("emms" "mp3" "ogg" "flac" "music" "mpeg" "video" "multimedia")
  :url "https://www.gnu.org/software/emms/")
;; Local Variables:
;; no-byte-compile: t
;; End:
