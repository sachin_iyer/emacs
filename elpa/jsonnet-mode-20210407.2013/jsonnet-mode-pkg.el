;;; Generated package description from jsonnet-mode.el  -*- no-byte-compile: t -*-
(define-package "jsonnet-mode" "20210407.2013" "Major mode for editing jsonnet files" '((emacs "24") (dash "2.17.0")) :commit "9bb6f86dfe6418ccccb929e8a03fb4bb24a9ac0e" :authors '(("Nick Lanham")) :maintainer '("Nick Lanham") :keywords '("languages") :url "https://github.com/mgyucht/jsonnet-mode")
