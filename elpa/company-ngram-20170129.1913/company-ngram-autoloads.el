;;; company-ngram-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "company-ngram" "company-ngram.el" (0 0 0 0))
;;; Generated autoloads from company-ngram.el

(autoload 'turn-on-company-ngram "company-ngram" "\


\(fn)" t nil)

(autoload 'turn-off-company-ngram "company-ngram" "\


\(fn)" t nil)

(autoload 'company-ngram-backend "company-ngram" "\


\(fn COMMAND &optional ARG &rest IGNORED)" t nil)

(autoload 'company-ngram-init "company-ngram" "\


\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "company-ngram" '("company-ngram-")))

;;;***

;;;### (autoloads nil nil ("company-ngram-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; company-ngram-autoloads.el ends here
