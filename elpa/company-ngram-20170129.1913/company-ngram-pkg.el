(define-package "company-ngram" "20170129.1913" "N-gram based completion"
  '((cl-lib "0.5")
    (company "0.8.0"))
  :authors
  '(("kshramt"))
  :maintainer
  '("kshramt")
  :url "https://github.com/kshramt/company-ngram")
;; Local Variables:
;; no-byte-compile: t
;; End:
