(define-package "mustache" "20201119.529" "a mustache templating library in emacs lisp"
  '((ht "0.9")
    (s "1.3.0")
    (dash "1.2.0"))
  :commit "7f4b1cf0483366a77539081fde94fa6079b514a0" :authors
  '(("Wilfred Hughes" . "me@wilfred.me.uk"))
  :maintainer
  '("Wilfred Hughes" . "me@wilfred.me.uk")
  :keywords
  '("convenience" "mustache" "template")
  :url "https://github.com/Wilfred/mustache.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
