(define-package "phpactor" "20210311.1615" "Interface to Phpactor"
  '((emacs "24.4")
    (f "0.17")
    (php-runtime "0.2")
    (composer "0.2.0")
    (async "1.9.3"))
  :commit "80788a817b0257363c1eee11a57cc0f873f0eef1" :authors
  '(("USAMI Kenta" . "tadsan@zonu.me")
    ("Mikael Kermorgant" . "mikael@kgtech.fi"))
  :maintainer
  '("USAMI Kenta" . "tadsan@zonu.me")
  :keywords
  '("tools" "php")
  :url "https://github.com/emacs-php/phpactor.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
