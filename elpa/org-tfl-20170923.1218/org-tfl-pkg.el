(define-package "org-tfl" "20170923.1218" "Transport for London meets Orgmode"
  '((org "0.16.2")
    (cl-lib "0.5")
    (emacs "24.1"))
  :keywords
  '("org" "tfl")
  :authors
  '(("storax (David Zuber), <zuber [dot] david [at] gmx [dot] de>"))
  :maintainer
  '("storax (David Zuber), <zuber [dot] david [at] gmx [dot] de>")
  :url "https://github.com/storax/org-tfl")
;; Local Variables:
;; no-byte-compile: t
;; End:
