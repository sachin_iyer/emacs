;;; -*- no-byte-compile: t -*-
(define-package "js2-closure" "20170816.1918" "Google Closure dependency manager" '((js2-mode "20150909")) :commit "f59db386d7d0693935d0bf52babcd2c203c06d04" :keywords '("javascript" "closure") :authors '(("Justine Tunney" . "jart@google.com")) :maintainer '("Justine Tunney" . "jart@google.com") :url "http://github.com/jart/js2-closure")
