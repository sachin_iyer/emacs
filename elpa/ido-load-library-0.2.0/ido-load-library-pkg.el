;;; -*- no-byte-compile: t -*-
(define-package "ido-load-library" "0.2.0" "Load-library alternative using ido-completing-read" '((persistent-soft "0.8.8") (pcache "0.2.3")))
