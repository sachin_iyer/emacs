;;; -*- no-byte-compile: t -*-
(define-package "vcsh" "20191007.1102" "vcsh integration" '((emacs "25")) :commit "cbb2b387ea035ee4f95455964144d699f573491d" :keywords '("vc" "files") :authors '(("Štěpán Němec" . "stepnem@gmail.com")) :maintainer '("Štěpán Němec" . "stepnem@gmail.com") :url "https://gitlab.com/stepnem/vcsh-el")
