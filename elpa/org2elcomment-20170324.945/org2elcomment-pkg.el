;;; -*- no-byte-compile: t -*-
(define-package "org2elcomment" "20170324.945" "Convert Org file to Elisp comments" '((org "8.3.4")) :commit "c88a75d9587c484ead18f7adf08592b09c1cceb0" :keywords '("extensions") :authors '(("Junpeng Qiu" . "qjpchmail@gmail.com")) :maintainer '("Junpeng Qiu" . "qjpchmail@gmail.com"))
