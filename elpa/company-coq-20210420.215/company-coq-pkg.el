(define-package "company-coq" "20210420.215" "A collection of extensions for Proof General's Coq mode"
  '((cl-lib "0.5")
    (dash "2.12.1")
    (yasnippet "0.11.0")
    (company "0.8.12")
    (company-math "1.1"))
  :commit "6a23da61e4008f54cf1b713f8b8bffd37887e172" :authors
  '(("Clément Pit-Claudel" . "clement.pitclaudel@live.com"))
  :maintainer
  '("Clément Pit-Claudel" . "clement.pitclaudel@live.com")
  :keywords
  '("convenience" "languages")
  :url "https://github.com/cpitclaudel/company-coq")
;; Local Variables:
;; no-byte-compile: t
;; End:
