;;; helm-rdefs-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-rdefs" "helm-rdefs.el" (0 0 0 0))
;;; Generated autoloads from helm-rdefs.el

(autoload 'helm-rdefs "helm-rdefs" "\
Open a helm buffer with rdefs output.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-rdefs" '("helm-rdefs-")))

;;;***

;;;### (autoloads nil nil ("helm-rdefs-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-rdefs-autoloads.el ends here
