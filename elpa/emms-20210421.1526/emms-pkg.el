(define-package "emms" "20210421.1526" "The Emacs Multimedia System"
  '((cl-lib "0.5")
    (seq "0"))
  :commit "75563821de35dacceb7bb118ee031790d0225583" :authors
  '(("Jorgen Schäfer" . "forcer@forcix.cx"))
  :maintainer
  '("Yoni Rabkin" . "yrk@gnu.org")
  :keywords
  '("emms" "mp3" "ogg" "flac" "music" "mpeg" "video" "multimedia")
  :url "https://www.gnu.org/software/emms/")
;; Local Variables:
;; no-byte-compile: t
;; End:
