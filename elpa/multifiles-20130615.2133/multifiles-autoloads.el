;;; multifiles-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "multifiles" "multifiles.el" (0 0 0 0))
;;; Generated autoloads from multifiles.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "multifiles" '("multifiles-minor-mode" "create-")))

;;;***

;;;### (autoloads nil nil ("multifiles-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; multifiles-autoloads.el ends here
