;;; helm-growthforecast-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-growthforecast" "helm-growthforecast.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-growthforecast.el

(autoload 'helm-growthforecast "helm-growthforecast" "\


\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-growthforecast" '("helm-growthforecast-")))

;;;***

;;;### (autoloads nil nil ("helm-growthforecast-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-growthforecast-autoloads.el ends here
