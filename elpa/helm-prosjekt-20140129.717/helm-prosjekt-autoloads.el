;;; helm-prosjekt-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-prosjekt" "helm-prosjekt.el" (0 0 0 0))
;;; Generated autoloads from helm-prosjekt.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-prosjekt" '("helm-" "prosjekt-temp-file-name")))

;;;***

;;;### (autoloads nil nil ("helm-prosjekt-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-prosjekt-autoloads.el ends here
