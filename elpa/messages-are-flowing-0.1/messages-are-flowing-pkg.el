;;; Generated package description from /home/siyer/.emacs.d/elpa/messages-are-flowing-0.1/messages-are-flowing.el  -*- no-byte-compile: t -*-
(define-package "messages-are-flowing" "0.1" "visible indication when composing \"flowed\" emails" 'nil :authors '(("Magnus Henoch" . "magnus.henoch@gmail.com")) :maintainer '("Magnus Henoch" . "magnus.henoch@gmail.com") :keywords '("mail"))
