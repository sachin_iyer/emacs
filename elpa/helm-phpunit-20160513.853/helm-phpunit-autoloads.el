;;; helm-phpunit-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-phpunit" "helm-phpunit.el" (0 0 0 0))
;;; Generated autoloads from helm-phpunit.el

(autoload 'helm-phpunit-selected-test "helm-phpunit" "\
Launch PHPUnit on the selected test by Helm.

\(fn TEST)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-phpunit" '("helm-phpunit-")))

;;;***

;;;### (autoloads nil nil ("helm-phpunit-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-phpunit-autoloads.el ends here
