(define-package "circe" "20210423.746" "Client for IRC in Emacs"
  '((cl-lib "0.5"))
  :commit "2f70aa236878d9c3726c31d6ba922e2d7076951d" :authors
  '(("Jorgen Schaefer" . "forcer@forcix.cx"))
  :maintainer
  '("Jorgen Schaefer" . "forcer@forcix.cx")
  :keywords
  '("irc" "chat")
  :url "https://github.com/jorgenschaefer/circe")
;; Local Variables:
;; no-byte-compile: t
;; End:
