;;; -*- no-byte-compile: t -*-
(define-package "markless" "20190306.1002" "Major mode for Markless documents" '((emacs "24.4")) :commit "75fdef45df96978e9326ea4d9bf4e534a250c4c0" :keywords '("languages" "wp") :authors '(("Nicolas Hafner" . "shinmera@tymoon.eu")) :maintainer '("Nicolas Hafner" . "shinmera@tymoon.eu") :url "http://github.com/shirakumo/markless.el/")
