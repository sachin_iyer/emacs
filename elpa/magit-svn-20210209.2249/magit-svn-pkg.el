;;; Generated package description from magit-svn.el  -*- no-byte-compile: t -*-
(define-package "magit-svn" "20210209.2249" "Git-Svn extension for Magit" '((emacs "24.4") (magit "2.1.0")) :commit "99601f47f47a421576809595ca7463fd010760b1" :authors '(("Phil Jackson" . "phil@shellarchive.co.uk")) :maintainer '("Phil Jackson" . "phil@shellarchive.co.uk") :keywords '("vc" "tools"))
