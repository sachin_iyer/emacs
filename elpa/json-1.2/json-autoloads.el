;;; json-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "json" "json.el" (0 0 0 0))
;;; Generated autoloads from json.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "json" '("json-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; json-autoloads.el ends here
