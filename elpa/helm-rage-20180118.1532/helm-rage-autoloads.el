;;; helm-rage-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-rage" "helm-rage.el" (0 0 0 0))
;;; Generated autoloads from helm-rage.el

(autoload 'helm-rage "helm-rage" "\
Precofigured `helm' for looking up memes by name.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-rage" '("helm-rage-")))

;;;***

;;;### (autoloads nil nil ("helm-rage-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-rage-autoloads.el ends here
