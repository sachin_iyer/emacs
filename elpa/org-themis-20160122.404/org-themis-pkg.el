;;; -*- no-byte-compile: t -*-
(define-package "org-themis" "20160122.404" "Experimental project management mode for org-mode" '((cl-lib "0.4")) :commit "78aadbbe22b1993be5c4accd0d3f91a4e85c9a3c" :keywords '("org-mode" "elisp" "project") :maintainer '("Zachary Elliott" . "contact@zell.io") :url "http://github.com/zellio/org-themis")
