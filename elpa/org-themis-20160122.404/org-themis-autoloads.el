;;; org-themis-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "org-themis" "org-themis.el" (0 0 0 0))
;;; Generated autoloads from org-themis.el

(autoload 'org-themis-mode "org-themis" "\
Experimental project management mode for `org-mode'

\(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-themis" '("org-themis-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-themis-autoloads.el ends here
