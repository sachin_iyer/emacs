;;; soundklaus-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "soundklaus" "soundklaus.el" (0 0 0 0))
;;; Generated autoloads from soundklaus.el

(autoload 'soundklaus-activities "soundklaus" "\
List activities on SoundCloud.

\(fn)" t nil)

(autoload 'soundklaus-connect "soundklaus" "\
Connect with SoundCloud.

\(fn)" t nil)

(autoload 'soundklaus-tracks "soundklaus" "\
List all tracks on SoundCloud matching QUERY.

\(fn QUERY)" t nil)

(autoload 'soundklaus-playlists "soundklaus" "\
List all playlists on SoundCloud matching QUERY.

\(fn QUERY)" t nil)

(autoload 'soundklaus-my-playlists "soundklaus" "\
List your playlists on SoundCloud.

\(fn)" t nil)

(autoload 'soundklaus-my-favorites "soundklaus" "\
List your favorite tracks on SoundCloud.

\(fn)" t nil)

(autoload 'soundklaus-my-tracks "soundklaus" "\
List your tracks on SoundCloud.

\(fn)" t nil)

(autoload 'soundklaus-desktop-entry-save "soundklaus" "\
Install the SoundKlaus desktop entry for the X Window System.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus" '("soundklaus-")))

;;;***

;;;### (autoloads nil "soundklaus-collection" "soundklaus-collection.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from soundklaus-collection.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-collection" '("soundklaus-")))

;;;***

;;;### (autoloads nil "soundklaus-custom" "soundklaus-custom.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from soundklaus-custom.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-custom" '("soundklaus-")))

;;;***

;;;### (autoloads nil "soundklaus-generic" "soundklaus-generic.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from soundklaus-generic.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-generic" '("soundklaus-")))

;;;***

;;;### (autoloads nil "soundklaus-helm" "soundklaus-helm.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from soundklaus-helm.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-helm" '("helm-soundklaus" "soundklaus-helm-")))

;;;***

;;;### (autoloads nil "soundklaus-playlist" "soundklaus-playlist.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from soundklaus-playlist.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-playlist" '("soundklaus-" "playlist")))

;;;***

;;;### (autoloads nil "soundklaus-request" "soundklaus-request.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from soundklaus-request.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-request" '("soundklaus-")))

;;;***

;;;### (autoloads nil "soundklaus-resource" "soundklaus-resource.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from soundklaus-resource.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-resource" '("define-soundklaus-resource" "soundklaus-")))

;;;***

;;;### (autoloads nil "soundklaus-track" "soundklaus-track.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from soundklaus-track.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-track" '("soundklaus-" "track")))

;;;***

;;;### (autoloads nil "soundklaus-user" "soundklaus-user.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from soundklaus-user.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-user" '("soundklaus-make-user" "user")))

;;;***

;;;### (autoloads nil "soundklaus-utils" "soundklaus-utils.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from soundklaus-utils.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundklaus-utils" '("soundklaus-")))

;;;***

;;;### (autoloads nil nil ("soundklaus-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; soundklaus-autoloads.el ends here
