(define-package "soundklaus" "20160314.1231" "Play music on SoundCloud with Emacs via EMMS"
  '((dash "2.12.1")
    (emacs "24")
    (emms "4.0")
    (s "1.11.0")
    (pkg-info "0.4")
    (cl-lib "0.5")
    (request "0.2.0"))
  :keywords
  '("soundcloud" "music" "emms")
  :authors
  '(("r0man" . "roman@burningswell.com"))
  :maintainer
  '("r0man" . "roman@burningswell.com")
  :url "https://github.com/r0man/soundklaus.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
