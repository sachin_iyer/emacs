;;; xcode-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "xcode-mode" "xcode-mode.el" (0 0 0 0))
;;; Generated autoloads from xcode-mode.el

(autoload 'xcode-mode "xcode-mode" "\
Minor mode to perform xcode like actions.

\(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "xcode-mode" '("xcode-")))

;;;***

;;;### (autoloads nil nil ("xcode-mode-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; xcode-mode-autoloads.el ends here
