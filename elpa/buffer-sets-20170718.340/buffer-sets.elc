;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(require 'cl-lib)
#@45 compiler-macro for inlining `buffer-set-p'.
(defalias 'buffer-set-p--cmacro #[(_cl-whole-arg cl-x) "\301\302\303\304\211\211&\207" [cl-x cl--defsubst-expand (cl-x) (cl-block buffer-set-p (and (memq (type-of cl-x) cl-struct-buffer-set-tags) t)) nil] 7 (#$ . 426)])
(put 'buffer-set-p 'compiler-macro 'buffer-set-p--cmacro)
(defalias 'buffer-set-p #[(cl-x) "\302!	>\205	 \303\207" [cl-x cl-struct-buffer-set-tags type-of t] 2])
(byte-code "\300\301\302\303#\304\305\306\301#\207" [function-put buffer-set-p side-effect-free error-free put buffer-set cl-deftype-satisfies] 5)
#@48 compiler-macro for inlining `buffer-set-name'.
(defalias 'buffer-set-name--cmacro #[(_cl-whole-arg cl-x) "\301\302\303\304\211\211&\207" [cl-x cl--defsubst-expand (cl-x) (cl-block buffer-set-name (progn (or (buffer-set-p cl-x) (signal 'wrong-type-argument (list 'buffer-set cl-x))) (aref cl-x 1))) nil] 7 (#$ . 1008)])
(put 'buffer-set-name 'compiler-macro 'buffer-set-name--cmacro)
#@49 Access slot "name" of `buffer-set' struct CL-X.
(defalias 'buffer-set-name #[(cl-x) "\302!	>\204 \303\304\305D\"\210\306H\207" [cl-x cl-struct-buffer-set-tags type-of signal wrong-type-argument buffer-set 1] 4 (#$ . 1399)])
(byte-code "\300\301\302\303#\300\207" [function-put buffer-set-name side-effect-free t] 4)
#@49 compiler-macro for inlining `buffer-set-files'.
(defalias 'buffer-set-files--cmacro #[(_cl-whole-arg cl-x) "\301\302\303\304\211\211&\207" [cl-x cl--defsubst-expand (cl-x) (cl-block buffer-set-files (progn (or (buffer-set-p cl-x) (signal 'wrong-type-argument (list 'buffer-set cl-x))) (aref cl-x 2))) nil] 7 (#$ . 1725)])
(put 'buffer-set-files 'compiler-macro 'buffer-set-files--cmacro)
#@50 Access slot "files" of `buffer-set' struct CL-X.
(defalias 'buffer-set-files #[(cl-x) "\302!	>\204 \303\304\305D\"\210\306H\207" [cl-x cl-struct-buffer-set-tags type-of signal wrong-type-argument buffer-set 2] 4 (#$ . 2121)])
(byte-code "\300\301\302\303#\300\207" [function-put buffer-set-files side-effect-free t] 4)
#@50 compiler-macro for inlining `buffer-set-select'.
(defalias 'buffer-set-select--cmacro #[(_cl-whole-arg cl-x) "\301\302\303\304\211\211&\207" [cl-x cl--defsubst-expand (cl-x) (cl-block buffer-set-select (progn (or (buffer-set-p cl-x) (signal 'wrong-type-argument (list 'buffer-set cl-x))) (aref cl-x 3))) nil] 7 (#$ . 2450)])
(put 'buffer-set-select 'compiler-macro 'buffer-set-select--cmacro)
#@51 Access slot "select" of `buffer-set' struct CL-X.
(defalias 'buffer-set-select #[(cl-x) "\302!	>\204 \303\304\305D\"\210\306H\207" [cl-x cl-struct-buffer-set-tags type-of signal wrong-type-argument buffer-set 3] 4 (#$ . 2851)])
(byte-code "\300\301\302\303#\300\207" [function-put buffer-set-select side-effect-free t] 4)
#@52 compiler-macro for inlining `buffer-set-on-apply'.
(defalias 'buffer-set-on-apply--cmacro #[(_cl-whole-arg cl-x) "\301\302\303\304\211\211&\207" [cl-x cl--defsubst-expand (cl-x) (cl-block buffer-set-on-apply (progn (or (buffer-set-p cl-x) (signal 'wrong-type-argument (list 'buffer-set cl-x))) (aref cl-x 4))) nil] 7 (#$ . 3183)])
(put 'buffer-set-on-apply 'compiler-macro 'buffer-set-on-apply--cmacro)
#@53 Access slot "on-apply" of `buffer-set' struct CL-X.
(defalias 'buffer-set-on-apply #[(cl-x) "\302!	>\204 \303\304\305D\"\210\306H\207" [cl-x cl-struct-buffer-set-tags type-of signal wrong-type-argument buffer-set 4] 4 (#$ . 3594)])
(byte-code "\300\301\302\303#\300\207" [function-put buffer-set-on-apply side-effect-free t] 4)
#@59 compiler-macro for inlining `buffer-set-on-apply-source'.
(defalias 'buffer-set-on-apply-source--cmacro #[(_cl-whole-arg cl-x) "\301\302\303\304\211\211&\207" [cl-x cl--defsubst-expand (cl-x) (cl-block buffer-set-on-apply-source (progn (or (buffer-set-p cl-x) (signal 'wrong-type-argument (list 'buffer-set cl-x))) (aref cl-x 5))) nil] 7 (#$ . 3932)])
(put 'buffer-set-on-apply-source 'compiler-macro 'buffer-set-on-apply-source--cmacro)
#@60 Access slot "on-apply-source" of `buffer-set' struct CL-X.
(defalias 'buffer-set-on-apply-source #[(cl-x) "\302!	>\204 \303\304\305D\"\210\306H\207" [cl-x cl-struct-buffer-set-tags type-of signal wrong-type-argument buffer-set 5] 4 (#$ . 4378)])
(byte-code "\300\301\302\303#\300\207" [function-put buffer-set-on-apply-source side-effect-free t] 4)
#@53 compiler-macro for inlining `buffer-set-on-remove'.
(defalias 'buffer-set-on-remove--cmacro #[(_cl-whole-arg cl-x) "\301\302\303\304\211\211&\207" [cl-x cl--defsubst-expand (cl-x) (cl-block buffer-set-on-remove (progn (or (buffer-set-p cl-x) (signal 'wrong-type-argument (list 'buffer-set cl-x))) (aref cl-x 6))) nil] 7 (#$ . 4737)])
(put 'buffer-set-on-remove 'compiler-macro 'buffer-set-on-remove--cmacro)
#@54 Access slot "on-remove" of `buffer-set' struct CL-X.
(defalias 'buffer-set-on-remove #[(cl-x) "\302!	>\204 \303\304\305D\"\210\306H\207" [cl-x cl-struct-buffer-set-tags type-of signal wrong-type-argument buffer-set 6] 4 (#$ . 5153)])
(byte-code "\300\301\302\303#\300\207" [function-put buffer-set-on-remove side-effect-free t] 4)
#@60 compiler-macro for inlining `buffer-set-on-remove-source'.
(defalias 'buffer-set-on-remove-source--cmacro #[(_cl-whole-arg cl-x) "\301\302\303\304\211\211&\207" [cl-x cl--defsubst-expand (cl-x) (cl-block buffer-set-on-remove-source (progn (or (buffer-set-p cl-x) (signal 'wrong-type-argument (list 'buffer-set cl-x))) (aref cl-x 7))) nil] 7 (#$ . 5494)])
(put 'buffer-set-on-remove-source 'compiler-macro 'buffer-set-on-remove-source--cmacro)
#@61 Access slot "on-remove-source" of `buffer-set' struct CL-X.
(defalias 'buffer-set-on-remove-source #[(cl-x) "\302!	>\204 \303\304\305D\"\210\306H\207" [cl-x cl-struct-buffer-set-tags type-of signal wrong-type-argument buffer-set 7] 4 (#$ . 5945)])
(byte-code "\300\301\302\303#\304\305\306\"\207" [function-put buffer-set-on-remove-source side-effect-free t defalias copy-buffer-set copy-sequence] 4)
#@148 compiler-macro for inlining `make-buffer-set'.

(fn CL-WHOLE &cl-quote &key NAME FILES SELECT ON-APPLY ON-APPLY-SOURCE ON-REMOVE ON-REMOVE-SOURCE)
(defalias 'make-buffer-set--cmacro #[(cl-whole &rest #1=#:--cl-rest--) "\306\307\"A@\306\310\"A@\306\311\"A@\306\312\"A@\306\313\"A@\306\314\"A@\306\315\"A@\203f @\316>\203M AA\211\2028 \317>A@\203\\ \320\211\2028 \321\322@\"\210\2026 )\323\324\325\320\320	\n\f&\f.\207" [#1# name files select on-apply on-apply-source plist-member :name :files :select :on-apply :on-apply-source :on-remove :on-remove-source (:name :files :select :on-apply :on-apply-source :on-remove :on-remove-source :allow-other-keys) :allow-other-keys nil error "Keyword argument %s not one of (:name :files :select :on-apply :on-apply-source :on-remove :on-remove-source)" cl--defsubst-expand (name files select on-apply on-apply-source on-remove on-remove-source) (cl-block make-buffer-set (record 'buffer-set name files select on-apply on-apply-source on-remove on-remove-source)) on-remove on-remove-source #2=#:--cl-keys-- cl-whole] 14 (#$ . 6357)])
(put 'make-buffer-set 'compiler-macro 'make-buffer-set--cmacro)
#@128 Constructor for objects of type `buffer-set'.

(fn &key NAME FILES SELECT ON-APPLY ON-APPLY-SOURCE ON-REMOVE ON-REMOVE-SOURCE)
(defalias 'make-buffer-set #[(&rest #1=#:--cl-rest--) "\306\307\"A@\306\310\"A@\306\311\"A@\306\312\"A@\306\313\"A@\306\314\"A@\306\315\"A@\203f @\316>\203M AA\211\2028 \317>A@\203\\ \320\211\2028 \321\322@\"\210\2026 )\323\324	\n\f&.\207" [#1# name files select on-apply on-apply-source plist-member :name :files :select :on-apply :on-apply-source :on-remove :on-remove-source (:name :files :select :on-apply :on-apply-source :on-remove :on-remove-source :allow-other-keys) :allow-other-keys nil error "Keyword argument %s not one of (:name :files :select :on-apply :on-apply-source :on-remove :on-remove-source)" record buffer-set on-remove on-remove-source #2=#:--cl-keys--] 10 (#$ . 7543)])
(byte-code "\300\301\302\303#\304\305\306\307\310\306\311\312\305\303&	\207" [function-put make-buffer-set side-effect-free t cl-struct-define buffer-set nil cl-structure-object record ((cl-tag-slot) (name) (files) (select) (on-apply) (on-apply-source) (on-remove) (on-remove-source)) cl-struct-buffer-set-tags] 11)
#@34 List of all defined buffer sets.
(defvar *buffer-sets* nil (#$ . 8727))
#@30 List of applied buffer-sets.
(defvar *buffer-sets-applied* nil (#$ . 8805))
#@37 List of all buffer set definitions.
(defvar *buffer-set-definitions* nil (#$ . 8887))
#@40 List of buffers in loaded buffer sets.
(defvar *buffer-set-buffers* nil (#$ . 8979))
(defvar buffer-sets-mode-p nil)
#@23 Hook run on set load.
(defvar buffer-sets-load-set-hook nil (#$ . 9102))
#@25 Hook run on set unload.
(defvar buffer-sets-unload-hook nil (#$ . 9181))
(byte-code "\300\301\302\303\304\305\306\307&\210\300\310\311\312\304\313\306\307&\210\300\314\315\316\304\317\306\307&\207" [custom-declare-variable buffer-set-file "~/.emacs.d/buffer-set-definitions.el" "The file to store buffer set definitions in." :type file :group editing buffer-sets-load-on-start (list) "A list of buffer-sets to load on Emacs start." (repeat symbol) buffer-sets-ignore-save (list) "A list of buffer-sets to ignore on saving." (repeat symbol)] 8)
#@33 Returns true if SET is applied.
(defalias 'buffer-sets-applied-p #[(set) "	\235\207" [set *buffer-sets-applied*] 2 (#$ . 9734)])
(defalias 'buffer-set--get-buffer-set-definition #[(set-name) "\301\302\"@\207" [*buffer-set-definitions* cl-remove-if-not #[(set) "\303	!\n>\204 \304\305\306	D\"\210	\307H=\207" [set-name set cl-struct-buffer-set-tags type-of signal wrong-type-argument buffer-set 1] 5]] 3])
(defalias 'buffer-set--generate-buffers-list #[(set-name) "\301\302\303\"!\207" [set-name intern format "*buffer-set-%s--buffers*"] 4])
#@166 Define a buffer set named NAME, taking FILES, RUN-ON-APPLY, RUN-ON-REMOVE and BUFFER-TO-SELECT as keyword arguments.

(fn NAME &key FILES SELECT ON-APPLY ON-REMOVE)
(defalias 'define-buffer-set '(macro . #[(name &rest #1=#:--cl-rest--) "\306\307\"A@\306\310\"A@\306\311\"A@\306\312\"A@\203H @\313>\2031 AA\211\202 \314>A@\203? \315\211\202 \316\317@\"\210\202 )\320\321\322$D\323BB\324\325\326\327\330\322$D\307\322	D\310\n\331\322D\332\322\fD\311\333\315BB\312\333\315\fBB\257\334\333\335\336\322$D\337BBE\340BBEE\341\342$!\343BB\322$D\257,\207" [#1# files select on-apply on-remove #2=#:--cl-keys-- plist-member :files :select :on-apply :on-remove (:files :select :on-apply :on-remove :allow-other-keys) :allow-other-keys nil error "Keyword argument %s not one of (:files :select :on-apply :on-remove)" progn cl-pushnew quote (*buffer-sets*) setq *buffer-set-definitions* cons make-buffer-set :name :on-apply-source :on-remove-source lambda cl-remove-if (structure) eq ((buffer-set-name structure)) (*buffer-set-definitions*) defvar buffer-set--generate-buffers-list (nil) name] 23 (#$ . 10287)]))
(defalias 'buffer-sets-load-set #[(name) "\306!\307	!\n>\204 \310\311\"\202\205 \307	!\n>\204\" \312\313\314	D\"\210	\315H\307	!\n>\2044 \312\313\314	D\"\210	\316H\307	!\n>\204F \312\313\314	D\"\210	\317H\320!\211\321\322\"L\210)\f \210;\203i \323!\210\235\203v \210\202| B\324\325!\210\326\327\",)\207" [name set-definition cl-struct-buffer-set-tags buffers-list on-apply select buffer-set--get-buffer-set-definition type-of error "Set Undefined: %s" signal wrong-type-argument buffer-set 2 3 4 buffer-set--generate-buffers-list mapcar find-file switch-to-buffer run-hooks buffer-sets-load-set-hook message "Applied buffer set %s." files #1=#:v *buffer-sets-applied*] 7 nil (list (intern (completing-read "Set Name: " (cl-remove-if #'(lambda (set) (member set *buffer-sets-applied*)) *buffer-sets*) nil t)))])
(defalias 'load-buffer-set 'buffer-sets-load-set)
(defalias 'buffer-sets-in-buffers-list #[(set buffer) "\305\n!\211\211J\306	\f\"\203 \f\202 	\fB)L*\207" [buffer #1=#:a1 set #2=#:v #3=#:vlist buffer-set--generate-buffers-list memql] 5])
#@31 Unload Buffer Set named NAME.
(defalias 'buffer-sets-unload-buffer-set #[(name) "\306!\307	!\n>\204 \310\311\"\202J \312!\307	!\n>\204% \313\314\315	D\"\210	\316H\317\320\fJ\"\210 \210\f\211\321L\210)\322\"\323\324!\210\325\326\"*)\207" [name set-definition cl-struct-buffer-set-tags on-remove buffers-list #1=#:v buffer-set--get-buffer-set-definition type-of error "Set Undefined: %s" buffer-set--generate-buffers-list signal wrong-type-argument buffer-set 6 mapc #[(buffer) "\301!\205 rq\210\302 \210\303!)\207" [buffer buffer-live-p save-buffer kill-buffer] 2] nil delq run-hooks buffer-sets-unload-hook message "Removed Buffer Set: %s" *buffer-sets-applied*] 6 (#$ . 12505) (list (intern (completing-read "Set Name: " *buffer-sets-applied*)))])
(defalias 'buffer-sets-unload-last-loaded-set #[nil "@\302	!)\207" [*buffer-sets-applied* set buffer-sets-unload-buffer-set] 2 nil nil])
#@40 Produce a list of defined buffer sets.
(defalias 'buffer-sets-list #[nil "\306\307!\203\n \310\307!\210\311\211\223\210\312	B\313\nB\314 \315\307!\211\311\211$%r\307q\210\316c\210&\311'\211(\205\275 (@'\317'!\204N \320\321'\"c\210\202U \320\322'\"c\210\323'!J\311)\211(\203\262 (@)\306)!\203\251 \324)\311\325#\204\222 \326c\210\327\330)!\331\332\333\334\335)DE!#\210\336c\210\202\251 \326c\210\327\330)!\331\332\333\337\335)DE!#\210\340c\210(A\211(\204c *(A\211(\2048 \311+%r\fq\210\341\f\311\"$)\342\343!\203\332 \343$%\"\202\334 %.\207" [help-window-point-marker temp-buffer-window-setup-hook temp-buffer-window-show-hook help-window-old-frame #1=#:buffer standard-output buffer-live-p "*Buffer Sets*" kill-buffer nil help-mode-setup help-mode-finish selected-frame temp-buffer-window-setup "Defined Buffer Sets:\n\n" buffer-sets-applied-p format " - %s\n" " - %s (Applied)\n" buffer-set--generate-buffers-list get-buffer-window-list t "    - " insert-text-button buffer-name action eval lambda (but) switch-to-buffer "\n" (but) "    - %s (visible)\n" temp-buffer-window-show functionp help-window-setup #2=#:window #3=#:value *buffer-sets* set --dolist-tail-- buffer] 9 (#$ . 13418) nil])
#@32 Unload all loaded buffer sets.
(defalias 'buffer-sets-unload-all-buffer-sets #[nil "\303\211\205 \n@\304	!\210\nA\211\204 \303*\207" [*buffer-sets-applied* buffer-set --dolist-tail-- nil buffer-sets-unload-buffer-set] 3 (#$ . 14654) nil])
#@19 Create a new set.
(defalias 'buffer-sets-create-set #[(name) "	\235?\205@ \306\n	\"\203 	\210\202 \n	B)\307!\211\310L\210)\311\312\310\211\313\310\314\310&\306\f\"\203: \202? \fB\211)\207" [name *buffer-sets* #1=#:var #2=#:v #3=#:var *buffer-set-definitions* memql buffer-set--generate-buffers-list nil record buffer-set #[nil "\300\207" [nil] 1] #[nil "\300\207" [nil] 1]] 10 (#$ . 14906) "SNew Set Name: "])
#@24 Add a file to the set.
(defalias 'buffer-sets-add-file-to-set #[(name file) "\305!\306	!\n>\204 \307\310\311	D\"\210	\211\312\313\306	!\n>\204' \307\310\311	D\"\210	\312H\fC\"I*\207" [name set cl-struct-buffer-set-tags #1=#:v file buffer-set--get-buffer-set-definition type-of signal wrong-type-argument buffer-set 2 append] 8 (#$ . 15337) (list (intern (completing-read "Set Name: " *buffer-sets* nil t)) (read-file-name "File Name: "))])
(defalias 'buffer-sets-add-directory-to-set #[(name directory) "\305!\306	!\n>\204 \307\310\311	D\"\210	\211\312\313\306	!\n>\204' \307\310\311	D\"\210	\312H\fC\"I*\207" [name set cl-struct-buffer-set-tags #1=#:v directory buffer-set--get-buffer-set-definition type-of signal wrong-type-argument buffer-set 2 append] 8 nil (list (intern (completing-read "Set Name: " *buffer-sets* nil t)) (read-directory-name "Directory: "))])
#@32 Add a buffer to the given set.
(defalias 'buffer-sets-add-buffer-to-set #[(name buffer) "\306!\307	!\310!\f>\204 \311\312\313D\"\210\211\314\315\310!\f>\204+ \311\312\313D\"\210\314H\nC\"I+\207" [name buffer file set cl-struct-buffer-set-tags #1=#:v buffer-set--get-buffer-set-definition buffer-file-name type-of signal wrong-type-argument buffer-set 2 append] 8 (#$ . 16219) (list (intern (completing-read "Set Name: " *buffer-sets* nil t)) (get-buffer (read-buffer "Buffer: " (current-buffer))))])
#@41 Set the buffer to automatically select.
(defalias 'buffer-sets-set-buffer-to-select #[(name) "\305!\306	!\n>\204 \307\310\311	D\"\210	\312H\306	!\n>\204& \307\310\311	D\"\210	\211\313\314\315\316\317\320!J\"\321\322$I+\207" [name set cl-struct-buffer-set-tags files #1=#:v buffer-set--get-buffer-set-definition type-of signal wrong-type-argument buffer-set 2 3 completing-read "Buffer: " mapcar buffer-name buffer-set--generate-buffers-list nil t] 9 (#$ . 16736) (list (intern (completing-read "Set Name: " *buffer-sets* nil t)))])
(defalias 'buffer-sets-remove-file #[(set) "\303!\304!	>\204 \305\306\307D\"\210\211\310\311\312\313\304!	>\204) \305\306\307D\"\210\310H\314\315$\304!	>\204> \305\306\307D\"\210\310H\"I*\207" [set cl-struct-buffer-set-tags #1=#:v buffer-set--get-buffer-set-definition type-of signal wrong-type-argument buffer-set 2 delq completing-read "File: " nil t] 10 nil (list (intern (completing-read "Set Name: " *buffer-sets* nil t)))])
#@27 Save defined buffer sets.
(defalias 'buffer-sets-save #[(the-set) "	\235?\205{ \306\307\310!\n>\204 \311\312\313D\"\210\314H\310!\n>\204* \311\312\313D\"\210\315H\310!\n>\204< \311\312\313D\"\210\316H\310!\n>\204N \311\312\313D\"\210\317H\310!\n>\204` \311\312\313D\"\210\320H\321\322\323\324\f\325\257\n-\"c\207" [the-set buffer-sets-ignore-save cl-struct-buffer-set-tags on-remove on-apply select format "%S\n\n" type-of signal wrong-type-argument buffer-set 1 2 3 5 7 define-buffer-set :files :select :on-apply :on-remove files name] 12 (#$ . 17723)])
#@35 Load buffer set definitions file.
(defalias 'buffer-sets-load-definitions-file #[nil "\301\302\211#\210\303\304!\207" [buffer-set-file load t message "Loaded Buffer Set Definitions."] 4 (#$ . 18314) nil])
(defalias 'buffer-sets-save-definitions #[nil "r\302!q\210\303\304\305!\304\306!\"\210\307\310\311	!\"\210\312 \210\313 \210)\314\315!\207" [buffer-set-file *buffer-set-definitions* find-file kill-region buffer-end -1 1 mapc buffer-sets-save reverse save-buffer kill-buffer message "Saved Buffer Set Definitions."] 4 nil nil])
#@33 Keymap for buffer-set commands.
(defvar buffer-sets-mode-map (byte-code "\301 \302\303\304#\210\302\305\306#\210\302\307\310#\210\302\311\312#\210\302\313\314#\210\302\315\316#\210\302\317\320#\210\302\321\322#\210\302\323\324#\210\302\325\326#\210\302\327\330#\210\302\331\332#\210\302\333\334#\210)\207" [keymap make-keymap define-key "Ll" buffer-sets-load-set "LL" buffer-sets-list "Lu" buffer-sets-unload-buffer-set "LU" buffer-sets-unload-all-buffer-sets "Lc" buffer-sets-create-set "Lf" buffer-sets-add-file-to-set "Lb" buffer-sets-add-buffer-to-set "Ld" buffer-sets-add-directory-to-set "LR" buffer-sets-remove-file "Ls" buffer-sets-set-buffer-to-select "Lp" buffer-sets-unload-last-loaded-set "L" buffer-sets-load-definitions-file "L" buffer-sets-save-definitions] 4) (#$ . 18854))
#@282 A mode for managing sets of buffers.

If called interactively, enable Buffer-Sets mode if ARG is
positive, and disable it if ARG is zero or negative.  If called
from Lisp, also enable the mode if ARG is omitted or nil, and
toggle it if ARG is `toggle'; disable the mode otherwise.
(defalias 'buffer-sets-mode #[(&optional arg) "\304 	\305=\203 \n?\202 \306	!\307V\211\203( \310 \210\311\312\313\"\210\311\312\314\"\210\2025 \314 \210\315\312\313\"\210\315\312\314\"\210\316\317\n\203? \320\202@ \321\"\210\322\323!\203e \304 \203T \304 \232\203e \324\325\326\n\203` \327\202a \330#\210))\331 \210\n\207" [#1=#:last-message arg buffer-sets-mode-p local current-message toggle prefix-numeric-value 0 buffer-sets-load-definitions-file add-hook kill-emacs-hook buffer-sets-unload-all-buffer-sets buffer-sets-save-definitions remove-hook run-hooks buffer-sets-mode-hook buffer-sets-mode-on-hook buffer-sets-mode-off-hook called-interactively-p any "" message "Buffer-Sets mode %sabled%s" "en" "dis" force-mode-line-update] 5 (#$ . 19681) (list (or current-prefix-arg 'toggle))])
(defvar buffer-sets-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\211%\207" [buffer-sets-mode-map buffer-sets-mode-hook variable-documentation put "Hook run after entering or leaving `buffer-sets-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode buffer-sets-mode-p " BSM" nil] 6)
#@52 Check to see if a buffer is in a given buffer-set.
(defalias 'ibuffer-filter-by-in-buffer-set #[(qualifier) "\303B\304\n!\204 \305\306	\"\202 \305\307	\"\210\310\311\312\"*\207" [qualifier #1=#:ibuffer-qualifier-str #2=#:ibuffer-filter in-buffer-set ibuffer-push-filter message "Filter by nil already applied:  %s" "Filter by nil added:  %s" ibuffer-update nil t] 3 (#$ . 21184) (list (intern (completing-read "Set Name: " *buffer-sets-applied*)))])
(byte-code "\301\302\303EB\302\207" [ibuffer-filtering-alist in-buffer-set nil #[(buf qualifier) "\3031 \304!J\n	\235)0\207\210\305 \210\306\207" [qualifier buffers-list buf (error) buffer-set--generate-buffers-list ibuffer-pop-filter nil] 2]] 3)
#@54 Install the hook to load buffer-sets on Emacs start.
(defalias 'buffer-sets-install-emacs-start-hook #[nil "\300\301\302\"\207" [add-hook after-init-hook buffer-sets-after-init] 3 (#$ . 21898)])
#@34 Load buffer-sets on Emacs start.
(defalias 'buffer-sets-after-init #[nil "\301\302\"\207" [buffer-sets-load-on-start mapcar load-buffer-set] 3 (#$ . 22099)])
(provide 'buffer-sets)
