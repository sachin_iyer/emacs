;;; -*- no-byte-compile: t -*-
(define-package "buffer-sets" "20170718.340" "Sets of Buffers for Buffer Management" '((cl-lib "0.5")) :commit "4a4ccb0d6916c3e9fba737bb7b48e8aac921954e" :keywords '("buffer-management") :authors '(("Samuel W. Flint" . "swflint@flintfam.org")) :maintainer '("Samuel W. Flint" . "swflint@flintfam.org") :url "http://github.com/swflint/buffer-sets")
