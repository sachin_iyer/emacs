;;; helm-ghs-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-ghs" "helm-ghs.el" (0 0 0 0))
;;; Generated autoloads from helm-ghs.el

(autoload 'helm-ghs "helm-ghs" "\
Interactively call ghs and show repos matching QUERY using helm.

\(fn QUERY)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-ghs" '("helm-ghs-")))

;;;***

;;;### (autoloads nil nil ("helm-ghs-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-ghs-autoloads.el ends here
