(define-package "geiser" "20210421.1938" "GNU Emacs and Scheme talk to each other"
  '((emacs "24.4"))
  :commit "2b45bd368b4acbcef53c3c761725241fb6846102" :authors
  '(("Jose Antonio Ortega Ruiz" . "jao@gnu.org"))
  :maintainer
  '("Jose Antonio Ortega Ruiz" . "jao@gnu.org")
  :keywords
  '("languages" "scheme" "geiser")
  :url "https://gitlab.com/emacs-geiser/")
;; Local Variables:
;; no-byte-compile: t
;; End:
