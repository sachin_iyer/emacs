;;; helm-system-packages-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-system-packages" "helm-system-packages.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-system-packages.el

(autoload 'helm-system-packages "helm-system-packages" "\
Helm user interface for system packages." t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-system-packages" '("helm-system-packages-")))

;;;***

;;;### (autoloads nil "helm-system-packages-brew" "helm-system-packages-brew.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-system-packages-brew.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-system-packages-brew" '("helm-system-packages-brew")))

;;;***

;;;### (autoloads nil "helm-system-packages-dnf" "helm-system-packages-dnf.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-system-packages-dnf.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-system-packages-dnf" '("helm-system-packages-dnf")))

;;;***

;;;### (autoloads nil "helm-system-packages-dpkg" "helm-system-packages-dpkg.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-system-packages-dpkg.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-system-packages-dpkg" '("helm-system-packages-")))

;;;***

;;;### (autoloads nil "helm-system-packages-guix" "helm-system-packages-guix.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-system-packages-guix.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-system-packages-guix" '("helm-system-packages-g")))

;;;***

;;;### (autoloads nil "helm-system-packages-pacman" "helm-system-packages-pacman.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-system-packages-pacman.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-system-packages-pacman" '("helm-system-packages-pacman")))

;;;***

;;;### (autoloads nil "helm-system-packages-portage" "helm-system-packages-portage.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-system-packages-portage.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-system-packages-portage" '("helm-system-packages-portage")))

;;;***

;;;### (autoloads nil "helm-system-packages-xbps" "helm-system-packages-xbps.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-system-packages-xbps.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-system-packages-xbps" '("helm-system-packages-xbps")))

;;;***

;;;### (autoloads nil nil ("helm-system-packages-pkg.el") (0 0 0
;;;;;;  0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-system-packages-autoloads.el ends here
