(define-package "mew" "20210416.33" "Messaging in the Emacs World" 'nil :commit "380d6059fa9f102e736969d086749980820a9e0e" :authors
  '(("Kazu Yamamoto" . "Kazu@Mew.org"))
  :maintainer
  '("Kazu Yamamoto" . "Kazu@Mew.org"))
;; Local Variables:
;; no-byte-compile: t
;; End:
