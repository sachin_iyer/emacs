(define-package "org-octopress" "20170821.415" "Compose octopress articles using org-mode."
  '((org "9.0")
    (orglue "0.1")
    (ctable "0.1.1"))
  :keywords
  '("org" "jekyll" "octopress" "blog")
  :authors
  '(("Yoshinari Nomura" . "nom@quickhack.net"))
  :maintainer
  '("Yoshinari Nomura" . "nom@quickhack.net"))
;; Local Variables:
;; no-byte-compile: t
;; End:
