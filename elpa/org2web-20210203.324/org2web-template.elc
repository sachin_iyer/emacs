;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\302\303\304\"\210\300\305!\210\300\306!\210\300\307!\210\300\310!\207" [require ox autoload mustache-render "mustache" org2web-util org2web-vars org2web-config md5] 3)
#@63 Get path of template file which name is `template-file-name'.
(defalias 'org2web-get-template-file #[(template-file-name) "\300\301\302\303\304\301\211\305#\"\"@\207" [remove nil mapcar #[(dir) "\303!	P\304\n!\205 \n)\207" [dir template-file-name file file-name-as-directory file-exists-p] 2] org2web-get-theme-dirs templates] 8 (#$ . 602)])
#@71 Get path of uploader template which name is `uploader-template-name'.
(defalias 'org2web-get-uploader-template #[(uploader-template-name) "\302\303\304\305\306 \307P\310!\307\311\312\313\314	!#\312R\310!\315P\310!\307PF\"\"@\207" [org2web-load-directory system-type remove nil mapcar #[(dir) "\303!	P\304\n!\205 \n)\207" [dir uploader-template-name file file-name-as-directory file-exists-p] 2] org2web-get-repository-directory "uploaders/" file-name-as-directory replace-regexp-in-string "/" "-" symbol-name "uploaders/common/"] 12 (#$ . 953)])
#@28 Get the title of org file.
(defalias 'org2web-get-title #[(org-file) "\302\303!\211\203 G\304V\203 \202 \305	!)\207" [title org-file org2web-read-org-option "TITLE" 0 file-name-base] 3 (#$ . 1511)])
#@89 Get org file category presented by ORG-FILE, return all categories if
ORG-FILE is nil. 
(defalias 'org2web-get-category #[(org-file) "\302\303!\304!\203 	!\202 \305	!)\207" [func org-file org2web-get-config-option :retrieve-category-function functionp org2web-get-file-category] 2 (#$ . 1722)])
#@149 Get the item associated with KEY in `org2web-item-cache', if `org2web-item-cache' is
nil or there is no item associated with KEY in it, return nil.
(defalias 'org2web-get-cache-item #[(key) "\205 \302	\"\207" [org2web-item-cache key plist-get] 3 (#$ . 2029)])
#@109 Update the item associated with KEY in `org2web-item-cache', if `org2web-item-cache' is
nil, initialize it.
(defalias 'org2web-update-cache-item #[(key value) "\203 \303	\n#\210\202 	\nD\n\207" [org2web-item-cache key value plist-put] 4 (#$ . 2299)])
#@133 Firstly get item from `org2web-item-cache' with KEY, if item not found, evaluate
BODY and push the result into cache and return it.
(defalias 'org2web-get-cache-create '(macro . #[(key &rest body) "\302\303D\304\305\306\307	BBDEE\207" [key body or org2web-get-cache-item org2web-update-cache-item funcall lambda nil] 8 (#$ . 2562)]))
#@200 Render the header on each page. PARAM-TABLE is the hash table from mustache
to render the template. If it is not set or nil, this function will try to build
a hash table accordint to current buffer.
(defalias 'org2web-render-header #[(&optional param-table org-file) "\304\305\306!\206 \307\306\310\311!\210\312\313\314!!\"\206M \315\316\317\"\320\321\322\323!\n!\324\322\325!Q	#\210\320\326\327\330!\2068 \2068 \331	#\210\320\332\327\333!	#\210\320\334\327\335!	#\210	)\"\207" [param-table #1=#:ht-temp org-file user-full-name mustache-render org2web-get-cache-item :header-template org2web-update-cache-item message "Read header.mustache from file" org2web-file-to-string org2web-get-template-file "header.mustache" make-hash-table :test equal puthash "page-title" org2web-get-config-option :get-title-function " - " :site-main-title "author" org2web-read-org-option "AUTHOR" "Unknown Author" "description" "DESCRIPTION" "keywords" "KEYWORDS"] 8 (#$ . 2905)])
#@332 Render the navigation bar on each page. it will be read firstly from
`org2web-item-cache', if there is no cached content, it will be rendered
and pushed into cache from template. PARAM-TABLE is the hash table for mustache
to render the template. If it is not set or nil, this function will try to
render from a default hash table.
(defalias 'org2web-render-navigation-bar #[(&optional param-table org-file) "\306 \307\310!\311\312!\206\362 \313\312\314\315!\210\316\311\317!\206& \313\317\314\320!\210\321\322\323!!\"\n\206\360 \324\325\326\"\327\330\307\331!#\210\327\332\307\333!#\210\327\334\335\336\337\340\341\342\343!\"\344\"\"#\210\327\345\335\346\335\347\307\350!\"\"#\210\327\351\307\352!\211\205~ \324\325\326\"\327\353\f@#\210\327\354\fA@#\210))#\210\327\355\307\356!\211\205\246 \324\325\326\"<\327\357\f@<#\210\327\360\fA@<#\210<))#\210\327\361\307\362!\211\205\316 \324\325\326\"=\327\363\f@=#\210\327\364\fA@=#\210=))#\210\327\365\307\366!#\210\327\367\370\371	\"\203\352 \372\373	\"\202\353 	#\210)\"\"*\207" [category-ignore-list site-domain param-table #1=#:ht-temp list #2=#:ht-temp org2web-get-site-domain org2web-get-config-option :category-ignore-list org2web-get-cache-item :nav-bar-html org2web-update-cache-item message "Render navigation bar from template" mustache-render :nav-bar-template "Read nav.mustache from file" org2web-file-to-string org2web-get-template-file "nav.mustache" make-hash-table :test equal puthash "site-main-title" :site-main-title "site-sub-title" :site-sub-title "nav-categories" mapcar #[(cat) "\302\303\304\"\305\306\307\310	!\307Q#\210\305\311\312	!#\210)\207" [#3=#:ht-temp cat make-hash-table :test equal puthash "category-uri" "/" org2web-encode-string-to-url "category-name" capitalize] 5] sort cl-remove-if #[(cat) "\302\230\206 \303\230\206 	\235\207" [cat category-ignore-list "index" "about"] 2] org2web-get-category nil string-lessp "nav-summary" #[(cat) "\302\303\304\"\305\306\307\310	!\307Q#\210\305\311\312	!#\210)\207" [#4=#:ht-temp cat make-hash-table :test equal puthash "summary-item-uri" "/" org2web-encode-string-to-url "summary-item-name" capitalize] 5] car :summary "nav-source-browse" :source-browse-url "source-browse-name" "source-browse-uri" "nav-about" :about "about-name" "about-uri" "nav-rss" :rss "rss-name" "rss-uri" "avatar" :personal-avatar "site-domain" string-match "\\`https?://\\(.*[a-zA-Z]\\)/?\\'" match-string 1 #5=#:ht-temp #6=#:ht-temp] 14 (#$ . 3878)])
#@195 Render the content on each page. TEMPLATE is the template name for rendering,
if it is not set of nil, will use default post.mustache instead. PARAM-TABLE is
similar to `org2web-render-header'.
(defalias 'org2web-render-content #[(&optional template param-table org-file) "\306\307\203 \310\311\312\313#!\202 \314!\206: \315\203$ \310\311\312\313#!\202% \314\316\317\206, \320\321Q!\210\322\323\2067 \320!!\"	\206c \324\325\326\"\327\330\331\332!!\n#\210\327\333\334\331\335!\336!\205]  *\n#\210\n)\"\207" [template param-table #1=#:ht-temp org-file #2=#:--cl-org-html-fontify-code-- org-export-function mustache-render org2web-get-cache-item intern replace-regexp-in-string "\\.mustache$" "-template" :post-template org2web-update-cache-item message "Read " "post.mustache" " from file" org2web-file-to-string org2web-get-template-file make-hash-table :test equal puthash "title" org2web-get-config-option :get-title-function "content" #[(code lang) "\205 \301!\207" [code org-html-encode-plain-text] 2] :org-export-function functionp] 7 (#$ . 6376)])
#@46 A function with can export org file to html.
(defalias 'org2web-default-org-export #[nil "\300\301\302\211\303\302%\207" [org-export-as html nil t] 6 (#$ . 7452)])
#@84 Render the footer on each page. PARAM-TABLE is similar to
`org2web-render-header'.
(defalias 'org2web-render-footer #[(&optional param-table org-file) "\306\307\310!\206 \311\310\312\313!\210\314\315\316!!\"\206\240\317 \317\320!\321\322!!\321\323!\324\325\326!\2062 \327\330!!@\325\331!\211A\205M \332\333\334\335\332\336\337A\340\320#\"\"\"A\341!B\342BC\"\206` \343!AD\344D\345\"\344D\346\"@\f#E\347\350\351\"F\352\353\344D\354\"F#\210\352\355\344D\356\"\205\227 \321\357!\206\227 \321\360!F#\210\352\361@F#\210\352\362\204\257 \327\330!\202\267 \327\330\363\364!8\"F#\210\352\365AF#\210\352\366A\204\316 \367\202\324 \370\371A\372#F#\210\352\373\325\374!\206\346 G\206\346 \375F#\210\352\376EF#\210\352\377\201I E!F#\210\352\201J \321\357!F#\210\352\201K \321\357!F#\210\352\201L \201M \n\206 	\201N \372\335\f#P!F#\210\352\201O \fF#\210\352\201P \201I E!F#\210\352\201Q \321\360!F#\210\352\201R \321\360!F#\210\352\201S \321\201T !F#\210\352\201U \321\201T !F#\210\352\201V \201W  F#\210\352\201X \201Y \325\201Z !\206\227H\206\227\201[ !F#\210F.\"\207" [param-table site-domain old-site-domain org-file title default-category mustache-render org2web-get-cache-item :footer-template org2web-update-cache-item message "Read footer.mustache from file" org2web-file-to-string org2web-get-template-file "footer.mustache" org2web-get-site-domain t org2web-get-config-option :get-title-function :default-category org2web-fix-timestamp-string org2web-read-org-option "DATE" format-time-string "%Y-%m-%d" "TAGS" mapcar #[(tag-name) "\302\303\304\"\305\306\307\310\311\312\313!\"@\206 \314	\"#\210\305\315	#\210)\207" [#1=#:ht-temp tag-name make-hash-table :test equal puthash "link" org2web-generate-summary-uri rassoc (:tags) org2web-get-config-option :summary "tags" "name"] 7] delete "" org2web-trim-string split-string "[:,]+" org2web-get-category assoc org2web-get-category-setting plist-get :uri-generator :uri-template make-hash-table :test equal puthash "show-meta" :show-meta "show-comment" :show-comment :personal-disqus-shortname :personal-duoshuo-shortname "date" "mod-date" 5 file-attributes "tags" "tag-links" "N/A" mapconcat #[(tag) "\301\302\"\207" [tag mustache-render "<a href=\"{{link}}\">{{name}}</a>"] 3] " " "author" "AUTHOR" "Unknown Author" "disqus-id" "disqus-url" date tags category org2web-category-config-alist config uri #2=#:ht-temp user-full-name user-mail-address org2web-get-full-url "disqus-comment" "disqus-shortname" "duoshuo-thread-key" md5 replace-regexp-in-string "duoshuo-title" "duoshuo-url" "duoshuo-comment" "duoshuo-shortname" "google-analytics" :personal-google-analytics-id "google-analytics-id" "creator-info" org2web-get-html-creator-string "email" org2web-confound-email-address "EMAIL" "Unknown Email"] 13 (#$ . 7622)])
(provide 'org2web-template)
