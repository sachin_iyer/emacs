;;; -*- no-byte-compile: t -*-
(define-package "ppp" "20200224.1320" "Extended pretty printer for Emacs Lisp" '((emacs "25.1")) :commit "a4eaec44216b189108164b42381abf35d0031200" :keywords '("tools") :authors '(("Naoya Yamashita" . "conao3@gmail.com")) :maintainer '("Naoya Yamashita" . "conao3@gmail.com") :url "https://github.com/conao3/ppp.el")
