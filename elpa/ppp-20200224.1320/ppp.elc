;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\304\305\306\307\310\311\312\313\314\315&	\210\316\317\320\321\322DD\323\312\305\324\325&\210\316\326\320\321\327DD\330\324\331\312\305&\210\316\332\320\321\333DD\334\312\305\324\335&\210\316\336\320\321\337DD\340\312\305\324\341&\207" [require warnings seq cl-lib custom-declare-group ppp nil "Extended pretty printer for Emacs Lisp." :prefix "ppp-" :group tools :link (url-link :tag "Github" "https://github.com/conao3/ppp.el") custom-declare-variable ppp-indent-spec funcall function #[0 "\300\207" [((0 _ unwind-protect) (1 _ lambda if condition-case not null car cdr 1+ 1- goto-char goto-line) (2 _ closure defcustom) (3 _ macro))] 1] "Special indent specification.\nElement at the top of the list takes precedence.\n\nFormat:\n  FORMAT  := (SPEC*)\n  SPEC    := (LEVEL . SYMBOLS)\n  LEVEL   := <integer>\n  SYMBOLS := (<symbol>*)\n\nDuplicate LEVEL is accepted." :type sexp ppp-escape-newlines #[0 "\300\207" [t] 1] "Value of `print-escape-newlines' used by ppp-* functions." boolean ppp-debug-buffer-template #[0 "\300\207" [#1="*PPP Debug buffer - %s*"] 1 #1#] "Buffer name for `ppp-debug'." string ppp-minimum-warning-level-base #[0 "\300\207" [:warning] 1] "Minimum level for `ppp-debug'.\nIt should be either :debug, :warning, :error, or :emergency.\nEvery minimul-earning-level variable initialized by this variable.\nYou can customize each variable like ppp-minimum-warning-level--{{pkg}}." (choice (const :tag ":debug" :debug) (const :tag ":warning" :warning) (const :tag ":error" :error) (const :tag ":emergency" :emergency))] 10)
#@74 Insert FORM, execute BODY, return `buffer-string'.

(fn FORM &rest BODY)
(defalias 'with-ppp--working-buffer '(macro . #[385 "\300\301\302\303\304\305\306BBE\307\310B\311BBBBBB\207" [with-temp-buffer (lisp-mode-variables nil) (set-syntax-table emacs-lisp-mode-syntax-table) let ((print-escape-newlines ppp-escape-newlines) (print-quoted t)) prin1 ((current-buffer)) (goto-char (point-min)) save-excursion ((delete-trailing-whitespace) (while (re-search-forward "^ *)" nil t) (delete-region (line-end-position 0) (1- (point)))) (buffer-substring-no-properties (point-min) (point-max)))] 10 (#$ . 2009)]))
(byte-code "\300\301\302\303#\304\301\305\306#\207" [function-put with-ppp--working-buffer lisp-indent-function 1 put edebug-form-spec t] 5)
#@49 If non-nil, curerntly using *ppp-debug* buffer.
(defvar ppp-buffer-using nil (#$ . 2764))
(make-variable-buffer-local 'ppp-buffer-using)
#@153 Insert FORM, execute BODY, return `buffer-string'.
Unlike `with-ppp--working-buffer', use existing buffer instead of temp buffer.

(fn FORM &rest BODY)
(defalias 'with-ppp--working-buffer-debug '(macro . #[385 "\300\301\302\303\304\305\300\306\307\310\300\311\312\313BB\314BBB\315\fB\316BBBBBB\317BBFE\207" [let ((bufname "*ppp-debug*") newbuf) with-current-buffer (if ppp-buffer-using (get-buffer (setq newbuf (generate-new-buffer-name bufname))) (get-buffer-create bufname)) (erase-buffer) unwind-protect ((ppp-buffer-using t)) (lisp-mode-variables nil) (set-syntax-table emacs-lisp-mode-syntax-table) ((print-escape-newlines ppp-escape-newlines) (print-quoted t)) prin1 ((current-buffer)) ((goto-char (point-min))) progn ((delete-trailing-whitespace) (while (re-search-forward "^ *)" nil t) (delete-region (line-end-position 0) (1- (point)))) (buffer-substring-no-properties (point-min) (point-max))) ((when newbuf (kill-buffer newbuf)))] 17 (#$ . 2908)]))
(byte-code "\300\301\302\303#\304\301\305\306#\207" [function-put with-ppp--working-buffer-debug lisp-indent-function 1 put edebug-form-spec t] 5)
#@116 Output the pretty-printed representation of FORM suitable for objects.
See `ppp-sexp' to get more info.

(fn FORM)
(defalias 'ppp-sexp-to-string '(macro . #[257 "\300\301DD\207" [with-output-to-string ppp-sexp] 4 (#$ . 4025)]))
#@121 Output the pretty-printed representation of FORM suitable for macro.
See `ppp-macroexpand' to get more info.

(fn FORM)
(defalias 'ppp-macroexpand-to-string '(macro . #[257 "\300\301DD\207" [with-output-to-string ppp-macroexpand] 4 (#$ . 4261)]))
#@201 Output the pretty-printed representation of FORM suitable for macro.
Unlike `ppp-macroexpand', use `macroexpand-all' instead of `macroexpand-1'.
See `ppp-macroexpand-all' to get more info.

(fn FORM)
(defalias 'ppp-macroexpand-all-to-string '(macro . #[257 "\300\301DD\207" [with-output-to-string ppp-macroexpand-all] 4 (#$ . 4516)]))
#@113 Output the pretty-printed representation of FORM suitable for list.
See `ppp-list' to get more info.

(fn FORM)
(defalias 'ppp-list-to-string '(macro . #[257 "\300\301DD\207" [with-output-to-string ppp-list] 4 (#$ . 4859)]))
#@115 Output the pretty-printed representation of FORM suitable for plist.
See `ppp-plist' to get more info.

(fn FORM)
(defalias 'ppp-plist-to-string '(macro . #[257 "\300\301DD\207" [with-output-to-string ppp-plist] 4 (#$ . 5092)]))
#@135 Output the pretty-printed representation of FORM suitable for symbol-function.
See `ppp-symbol-funciton' to get more info.

(fn FORM)
(defalias 'ppp-symbol-function-to-string '(macro . #[257 "\300\301DD\207" [with-output-to-string ppp-symbol-function] 4 (#$ . 5329)]))
#@129 Output the pretty-printed representation of FORM suitable for symbol-value.
See `ppp-symbol-value' to get more info.

(fn FORM)
(defalias 'ppp-symbol-value-to-string '(macro . #[257 "\300\301DD\207" [with-output-to-string ppp-symbol-value] 4 (#$ . 5606)]))
#@27 Delete spaces near point.
(defalias 'ppp--delete-spaces-at-point #[0 "\300\211\301x\210`\301w\210`|\207" [" 	\n" nil] 4 (#$ . 5870)])
#@50 Delete last newline character for STR.

(fn STR)
(defalias 'ppp--delete-last-newline #[257 "\300\301\302#\207" [replace-regexp-in-string "\n$" ""] 5 (#$ . 6011)])
#@43 Return non-nil if before point is spaces.
(defalias 'ppp--space-before-p #[0 "`Sf\300>\207" [(32 9 10)] 2 (#$ . 6181)])
#@103 Prettify the current buffer with printed representation of a Lisp object.
ppp version of `pp-buffer'.
(defalias 'ppp-buffer #[0 "\212eb\210m\204\325 \303 \304\305\306\307\310\311!\312\"\313\314%\"@\206) \2119\205) \315\316!\317\"\211\250\203 \320 \210m\204y \3211u \211\322\211W\205o \211\323\324w\210\325\326\327!\330\216	\331\303 !\210)r	q\210\332 +!`\320 \210`|\210\211c\266\211T\262\202; \266\2020\202x \324\262\210\333c\210\202\320 \3341\213 \335 \210\3360\202\217 \210\202\255 \203\255 \212\337u\210\340\324x\210o\204\251 \341 \203\251 \342 \210\333c\210)\202\320 \3431\271 \344 \210\3360\202\275 \210\202\315 \203\315 \345\346!\210\342 \210\333c\210\202\320 db\210\266\202 )\336\347ed\"\210)\350 \210\351\352\324\336#\205\363 \353\322!`S|\210\202\341 \207" [ppp-indent-spec standard-output inhibit-message sexp-at-point cl-find-if make-byte-code 257 "\300A>\207" vconcat vector [] 3 "\n\n(fn ELM)" plist-get symbol-plist lisp-indent-function forward-sexp (scan-error) 0 " 	\n" nil ppp--delete-last-newline generate-new-buffer " *string-output*" #[0 "\301!\207" [standard-output kill-buffer] 2] ppp-sexp buffer-string "\n" (error) down-list t -1 "'`#^" ppp--space-before-p ppp--delete-spaces-at-point (error) up-list skip-syntax-forward ")" indent-region delete-trailing-whitespace re-search-forward "^ *)" line-end-position] 8 (#$ . 6308) nil])
#@83 Output the pretty-printed representation of FORM suitable for objects.

(fn FORM)
(defalias 'ppp-sexp #[257 "\304\305\306!r\211q\210\307\310\311\312\313!\314\"\315$\216\316\304!\210\317!\210	\320\321p\"\210*eb\210\212\322 \210)\323 \210\324\325\304\320#\203C \326\310!`S|\210\2021 \327ed\"*\262\330!\266\207" [emacs-lisp-mode-syntax-table ppp-escape-newlines print-quoted print-escape-newlines nil generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 lisp-mode-variables set-syntax-table t prin1 ppp-buffer delete-trailing-whitespace re-search-forward "^ *)" line-end-position buffer-substring-no-properties princ] 9 (#$ . 7682)])
#@81 Output the pretty-printed representation of FORM suitable for macro.

(fn FORM)
(defalias 'ppp-macroexpand '(macro . #[257 "\300\301\302DDD\207" [ppp-sexp macroexpand-1 quote] 5 (#$ . 8395)]))
#@157 Output the pretty-printed representation of FORM suitable for macro.
Unlike `ppp-macroexpand', use `macroexpand-all' instead of `macroexpand-1'.

(fn FORM)
(defalias 'ppp-macroexpand-all '(macro . #[257 "\300\301\302DDD\207" [ppp-sexp macroexpand-all quote] 5 (#$ . 8596)]))
#@80 Output the pretty-printed representation of FORM suitable for list.

(fn FORM)
(defalias 'ppp-list #[257 "\304\305\306!r\211q\210\307\310\311\312\313!\314\"\315$\216\316\304!\210\317!\210	\320\321p\"\210*eb\210\212\203H <\203H \304u\210\3221C \323 \210\324 \210\202: \210\325\326!\210)\327 \210\330\331\304\320#\203^ \332\310!`S|\210\202L \333ed\"*\262\334\335P!\266\207" [emacs-lisp-mode-syntax-table ppp-escape-newlines print-quoted print-escape-newlines nil generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 lisp-mode-variables set-syntax-table t prin1 (error) forward-sexp newline delete-char -1 delete-trailing-whitespace re-search-forward "^ *)" line-end-position buffer-substring-no-properties princ "\n"] 9 (#$ . 8878)])
#@81 Output the pretty-printed representation of FORM suitable for plist.

(fn FORM)
(defalias 'ppp-plist #[257 "\304\305\306!r\211q\210\307\310\311\312\313!\314\"\315$\216\316\304!\210\317!\210	\320\321p\"\210*eb\210\212\203I <\203I \304u\210\3221D \323\315!\210\324 \210\202: \210\325\326!\210)\327 \210\330\331\304\320#\203_ \332\310!`S|\210\202M \333ed\"*\262\334\335P!\266\207" [emacs-lisp-mode-syntax-table ppp-escape-newlines print-quoted print-escape-newlines nil generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 lisp-mode-variables set-syntax-table t prin1 (error) forward-sexp newline delete-char -1 delete-trailing-whitespace re-search-forward "^ *)" line-end-position buffer-substring-no-properties princ "\n"] 9 (#$ . 9694)])
#@47 Output `symbol-function' for FORM.

(fn FORM)
(defalias 'ppp-symbol-function '(macro . #[257 "\2119\203	 \211\202\f \300!\301\302\303DDD\207" [eval ppp-sexp symbol-function quote] 6 (#$ . 10516)]))
#@44 Output `symbol-value' for FORM.

(fn FORM)
(defalias 'ppp-symbol-value '(macro . #[257 "\2119\203	 \211\202\f \300!\301\302\303DDD\207" [eval ppp-sexp symbol-value quote] 6 (#$ . 10722)]))
#@37 Convert ALIST to plist.

(fn ALIST)
(defalias 'ppp-alist-to-plist #[257 "\300\301\"\207" [mapcan #[257 "\300@!\301\302\303\"?\205 \303P!AD\207" [prin1-to-string intern string-prefix-p ":"] 6 "\n\n(fn ELM)"]] 4 (#$ . 10919)])
#@62 Define SYM as variable if not defined for PKG.

(fn SYM PKG)
(defalias 'ppp--define-warning-level-symbol #[514 "\300!?\205 \301\302\303\304\305\"\306BBBB!\207" [boundp eval defcustom ppp-minimum-warning-level-base format "Minimum level for debugging %s.\nIt should be either :debug, :warning, :error, or :emergency." (:group 'ppp :type 'symbol)] 9 (#$ . 11156)])
#@126 Get caller function and arguments from backtrace.
Optional arguments LEVEL is pop level for backtrace.

(fn &optional LEVEL)
(defalias 'ppp--get-caller #[256 "\301\302\303\304!\305\216\306 \210)rq\210\307 +\"\310\3111P \312!A\262\310\206' \313\314\211W\203D \211\211A\262\242\211\262<\204/ \210\211T\262\202( \266\211\242D\2620\202S \315\262\207" [standard-output format "(%s)" generate-new-buffer " *string-output*" #[0 "\301!\207" [standard-output kill-buffer] 2] backtrace buffer-string nil (invalid-read-syntax) read 1 0 (:error invalid-read-syntax)] 9 (#$ . 11531)])
#@741 Output debug message to `flylint-debug-buffer'.

ARGS accepts (KEYWORD-ARGUMENTS... PKG FORMAT &rest FORMAT-ARGS).

Auto arguments:
  PKG is symbol.
  FORMAT and FORMAT-ARGS passed `format'.

Keyword arguments:
  If LEVEL is specified, output higher than
  `ppp-minimum-warning-level--{{PKG}}' initialized `ppp-minimum-warning-level'.
  LEVEL should be one of :debug, :warning, :error, or :emergency.
  If LEVEL is omitted, assume :debug.
  If BUFFER is specified, output that buffer.
  If POPUP is non-nil, `display-buffer' debug buffer.
  If BREAK is non-nil, output page break before output string.

Note:
  If use keyword arguments, must specified these before auto arguments.

(fn &key buffer level break PKG FORMAT &rest FORMAT-ARGS)
(defalias 'ppp-debug '(macro . #[128 "\303\303\211\211\303:\203e \262\211A\262\242\262@\262:\203e \304!\262\305!\203e \211\306>\203S \211\236\304!\203C \241\210\202N B\211\262	B\262	\266\202X \307\310\"\210AA\262AA\262\202 \311\236\304\211A\262\242!\203| \241\210\202\207 \311B\211\262	B\262	\266\312\236\211A\262\242\203\236 \241\210\202\251 \312B\211\262	B\262	\266\313\236\203\273 \241\210\202\306 \313B\211\262	B\262	\266\314\311\"\314\312\"\314\313\"\314\315\"\206\334 \316\314\317\"\206\350 \320\"\314\321\"\314\322\"\323\320\324	\"!\325	\"\210\326\327D\330\331\332\333\334D\334\fDE\335\336\337\340\341\320BBD\342BB\343\344\345\346\205*\347\320	\236A@\320\n\"\"\350BBBD\351BBB\352\205G\353\257EE\257\266\210\207" [ppp-debug-buffer-template warning-levels warning-type-format nil eval keywordp (:level :buffer :popup :break) error "Unknown keyword: %s" :pkg :format-raw :format-args-raw alist-get :level :debug :buffer format :popup :break intern "ppp-minimum-warning-level--%s" ppp--define-warning-level-symbol with-current-buffer get-buffer-create (special-mode) (emacs-lisp-mode) when <= warning-numeric-level prog1 t let (inhibit-read-only t) msg ((scroll (equal (point) (point-max)))) save-excursion (goto-char (point-max)) insert concat "\f\n" ("\n" msg) ((unless (and (bolp) (eolp)) (newline))) (when scroll (goto-char (point-max)) (set-window-point (get-buffer-window (current-buffer)) (point-max))) (display-buffer (current-buffer))] 30 (#$ . 12127)]))
(byte-code "\300\301\302\303#\304\305!\207" [function-put ppp-debug lisp-indent-function defun provide ppp] 4)
