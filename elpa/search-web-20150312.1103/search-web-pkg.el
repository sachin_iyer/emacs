;;; -*- no-byte-compile: t -*-
(define-package "search-web" "20150312.1103" "Post web search queries using `browse-url'." 'nil :commit "c4ae86ac1acfc572b81f3d78764bd9a54034c331" :authors '(("Tomoya Otake" . "tomoya.ton@gmail.com")) :maintainer '("Tomoya Otake" . "tomoya.ton@gmail.com"))
