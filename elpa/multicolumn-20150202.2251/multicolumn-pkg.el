;;; -*- no-byte-compile: t -*-
(define-package "multicolumn" "20150202.2251" "Creating and managing multiple side-by-side windows." 'nil :commit "c7a3afecd470859b2e60aa7c554d6e4d436df7fa" :authors '(("Anders Lindgren")) :maintainer '("Anders Lindgren") :url "https://github.com/Lindydancer/multicolumn")
