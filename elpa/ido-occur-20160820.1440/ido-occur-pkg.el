;;; -*- no-byte-compile: t -*-
(define-package "ido-occur" "20160820.1440" "Yet another `occur' with `ido'." '((dash "2.13.0")) :commit "6a0bfeaca2e334b47b4f38ab80d63f53535b189e" :keywords '("inner" "buffer" "search") :authors '(("Danil" . "danil@kutkevich.org")) :maintainer '("Danil" . "danil@kutkevich.org") :url "https://github.com/danil/ido-occur")
