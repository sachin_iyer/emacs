(define-package "apples-mode" "20110121.418" "Major mode for editing and executing AppleScript code" 'nil :commit "83a9ab0d6ba82496e2f7df386909b1a55701fccb" :keywords
  '("applescript" "languages")
  :authors
  '(("tequilasunset" . "tequilasunset.mac@gmail.com"))
  :maintainer
  '("tequilasunset" . "tequilasunset.mac@gmail.com"))
;; Local Variables:
;; no-byte-compile: t
;; End:
