;;; -*- no-byte-compile: t -*-
(define-package "helm-tail" "20181124.439" "Read recent output from various sources" '((emacs "25.1") (helm "2.7.0")) :commit "1f5a6355aa3bdb00b9b0bc93db29c17f0d6701e3" :keywords '("maint" "tools") :authors '(("Akira Komamura" . "akira.komamura@gmail.com")) :maintainer '("Akira Komamura" . "akira.komamura@gmail.com") :url "https://github.com/akirak/helm-tail")
