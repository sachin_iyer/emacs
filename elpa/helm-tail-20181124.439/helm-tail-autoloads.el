;;; helm-tail-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-tail" "helm-tail.el" (0 0 0 0))
;;; Generated autoloads from helm-tail.el

(autoload 'helm-tail "helm-tail" "\
Display recent output of common special buffers.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-tail" '("helm-tail-")))

;;;***

;;;### (autoloads nil nil ("helm-tail-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-tail-autoloads.el ends here
