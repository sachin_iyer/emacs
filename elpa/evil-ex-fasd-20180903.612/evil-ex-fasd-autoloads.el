;;; evil-ex-fasd-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "evil-ex-fasd" "evil-ex-fasd.el" (0 0 0 0))
;;; Generated autoloads from evil-ex-fasd.el

(autoload 'evil-ex-fasd "evil-ex-fasd" "\
Invoke `evil-ex' with `evil-ex-fasd-prefix'.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "evil-ex-fasd" '("evil-ex-fasd-")))

;;;***

;;;### (autoloads nil nil ("evil-ex-fasd-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; evil-ex-fasd-autoloads.el ends here
