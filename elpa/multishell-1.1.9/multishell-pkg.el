;; Generated package description from multishell.el  -*- no-byte-compile: t -*-
(define-package "multishell" "1.1.9" "Easily use multiple shell buffers, local and remote" '((cl-lib "0.5")) :keywords '("processes") :authors '(("Ken Manheimer" . "ken.manheimer@gmail.com")) :maintainer '("Ken Manheimer" . "ken.manheimer@gmail.com") :url "https://github.com/kenmanheimer/EmacsMultishell")
