(define-package "ess" "20210414.2354" "Emacs Speaks Statistics"
  '((emacs "25.1"))
  :commit "1782c6730a8fadcf4c162c7aac4329d4e28259b6" :authors
  '(("David Smith" . "dsmith@stats.adelaide.edu.au")
    ("A.J. Rossini" . "blindglobe@gmail.com")
    ("Richard M. Heiberger" . "rmh@temple.edu")
    ("Kurt Hornik" . "Kurt.Hornik@R-project.org")
    ("Martin Maechler" . "maechler@stat.math.ethz.ch")
    ("Rodney A. Sparapani" . "rsparapa@mcw.edu")
    ("Stephen Eglen" . "stephen@gnu.org")
    ("Sebastian P. Luque" . "spluque@gmail.com")
    ("Henning Redestig" . "henning.red@googlemail.com")
    ("Vitalie Spinu" . "spinuvit@gmail.com")
    ("Lionel Henry" . "lionel.hry@gmail.com")
    ("J. Alexander Branham" . "alex.branham@gmail.com"))
  :maintainer
  '("ESS Core Team" . "ESS-core@r-project.org")
  :url "https://ess.r-project.org/")
;; Local Variables:
;; no-byte-compile: t
;; End:
