;;; company-flow-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "company-flow" "company-flow.el" (0 0 0 0))
;;; Generated autoloads from company-flow.el

(autoload 'company-flow "company-flow" "\


\(fn COMMAND &optional ARG &rest ARGS)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "company-flow" '("company-flow-")))

;;;***

;;;### (autoloads nil nil ("company-flow-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; company-flow-autoloads.el ends here
