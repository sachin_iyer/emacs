;;; -*- no-byte-compile: t -*-
(define-package "company-flow" "20180225.2159" "Flow backend for company-mode" '((company "0.8.0") (dash "2.13.0")) :commit "76ef585c70d2a3206c2eadf24ba61e59124c3a16" :authors '(("Aaron Jensen" . "aaronjensen@gmail.com")) :maintainer '("Aaron Jensen" . "aaronjensen@gmail.com") :url "https://github.com/aaronjensen/company-flow")
