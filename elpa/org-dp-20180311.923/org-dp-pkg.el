(define-package "org-dp" "20180311.923" "Declarative Local Programming with Org Elements"
  '((cl-lib "0.5"))
  :authors
  '(("Thorsten Jolitz <tjolitz AT gmail DOT com>"))
  :maintainer
  '("Thorsten Jolitz <tjolitz AT gmail DOT com>")
  :url "https://github.com/tj64/org-dp")
;; Local Variables:
;; no-byte-compile: t
;; End:
