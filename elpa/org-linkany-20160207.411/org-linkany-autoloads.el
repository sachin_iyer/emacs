;;; org-linkany-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "org-linkany" "org-linkany.el" (0 0 0 0))
;;; Generated autoloads from org-linkany.el

(autoload 'org-linkany/clear-cache "org-linkany" "\
Clear cache.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-linkany" '("org-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-linkany-autoloads.el ends here
