(define-package "lsp-treemacs" "20210424.1717" "LSP treemacs"
  '((emacs "26.1")
    (dash "2.18.0")
    (f "0.20.0")
    (ht "2.0")
    (treemacs "2.5")
    (lsp-mode "6.0"))
  :commit "2a64981578aebd4ffc77b7de90b985a3d7fcfa93" :authors
  '(("Ivan Yonchovski"))
  :maintainer
  '("Ivan Yonchovski")
  :keywords
  '("languages")
  :url "https://github.com/emacs-lsp/lsp-treemacs")
;; Local Variables:
;; no-byte-compile: t
;; End:
