;;; dired-nav-enhance-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "dired-nav-enhance" "dired-nav-enhance.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from dired-nav-enhance.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dired-nav-enhance" '("dired-nav-enhance-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; dired-nav-enhance-autoloads.el ends here
