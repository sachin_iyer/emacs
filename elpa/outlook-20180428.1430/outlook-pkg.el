(define-package "outlook" "20180428.1430" "send emails in MS Outlook style"
  '((emacs "24.4"))
  :keywords
  '("mail")
  :authors
  '(("Andrew Savonichev"))
  :maintainer
  '("Andrew Savonichev")
  :url "https://github.com/asavonic/outlook.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
