;;; hmac-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "hmac" "hmac.el" (0 0 0 0))
;;; Generated autoloads from hmac.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "hmac" '("hmac")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; hmac-autoloads.el ends here
