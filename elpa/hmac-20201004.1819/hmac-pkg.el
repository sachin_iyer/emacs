;;; Generated package description from /home/siyer/.emacs.d/elpa/hmac-20201004.1819/hmac.el  -*- no-byte-compile: t -*-
(define-package "hmac" "20201004.1819" "Hash-based message authentication code" '((emacs "25.1")) :commit "f2b99a9a10becfff207cf9418c6dce78364b1a4b" :authors '(("Sean McAfee")) :maintainer '("Sean McAfee") :url "https://github.com/grimnebulin/emacs-hmac")
