;;; -*- no-byte-compile: t -*-
(define-package "helm-frame" "20180604.1005" "open helm buffers in a dedicated frame" '((emacs "24.4")) :commit "485e2a534b0de5e8dbeb144a9a60ceca00215a4a" :keywords '("lisp" "helm" "popup" "frame") :authors '(("chee" . "chee@snake.dog")) :maintainer '("chee" . "chee@snake.dog"))
