;;; -*- no-byte-compile: t -*-
(define-package "python-test" "20181018.29" "Python testing integration" '((emacs "25.1")) :commit "f899975b133539e19ba822e4b0bfd1a28572967e" :keywords '("convenience" "tools" "processes") :authors '(("Mario Rodas" . "marsam@users.noreply.github.com")) :maintainer '("Mario Rodas" . "marsam@users.noreply.github.com") :url "https://github.com/emacs-pe/python-test.el")
