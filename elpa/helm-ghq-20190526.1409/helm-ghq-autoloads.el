;;; helm-ghq-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-ghq" "helm-ghq.el" (0 0 0 0))
;;; Generated autoloads from helm-ghq.el

(autoload 'helm-ghq "helm-ghq" "\


\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-ghq" '("helm-ghq-")))

;;;***

;;;### (autoloads nil nil ("helm-ghq-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-ghq-autoloads.el ends here
