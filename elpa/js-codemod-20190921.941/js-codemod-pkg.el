(define-package "js-codemod" "20190921.941" "Run js-codemod on current sentence or selected region"
  '((emacs "24.4"))
  :keywords
  '("js" "codemod" "region")
  :authors
  '((nil . "Torgeir Thoresen <@torgeir>"))
  :maintainer
  '(nil . "Torgeir Thoresen <@torgeir>"))
;; Local Variables:
;; no-byte-compile: t
;; End:
