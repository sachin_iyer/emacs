;;; -*- no-byte-compile: t -*-
(define-package "helm-osx-app" "20190717.958" "Launch macOS apps with helm" '((emacs "25.1") (helm-core "3.0")) :commit "634ed5d721a20af265825a018e9df3ee6640daee" :authors '(("Xu Chunyang")) :maintainer '("Xu Chunyang") :url "https://github.com/xuchunyang/helm-osx-app")
