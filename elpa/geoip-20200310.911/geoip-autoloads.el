;;; geoip-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "geoip" "geoip.el" (0 0 0 0))
;;; Generated autoloads from geoip.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "geoip" '("geoip-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; geoip-autoloads.el ends here
