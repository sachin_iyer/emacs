;;; helm-idris-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-idris" "helm-idris.el" (0 0 0 0))
;;; Generated autoloads from helm-idris.el

(autoload 'helm-idris "helm-idris" "\
Search the Idris documentation with Helm.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-idris" '("helm-idris-")))

;;;***

;;;### (autoloads nil nil ("helm-idris-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-idris-autoloads.el ends here
