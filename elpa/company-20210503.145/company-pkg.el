(define-package "company" "20210503.145" "Modular text completion framework"
  '((emacs "25.1"))
  :commit "798238f37820d00b8b25b00386ce0687cb8d4187" :authors
  '(("Nikolaj Schumacher"))
  :maintainer
  '("Dmitry Gutov" . "dgutov@yandex.ru")
  :keywords
  '("abbrev" "convenience" "matching")
  :url "http://company-mode.github.io/")
;; Local Variables:
;; no-byte-compile: t
;; End:
