;;; evil-dvorak-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "evil-dvorak" "evil-dvorak.el" (0 0 0 0))
;;; Generated autoloads from evil-dvorak.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "evil-dvorak" '("global-evil-dvorak-mode" "turn-o" "evil-dvorak-mode")))

;;;***

;;;### (autoloads nil nil ("evil-dvorak-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; evil-dvorak-autoloads.el ends here
