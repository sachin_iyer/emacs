(define-package "org-notifications" "20210310.1149" "Creates notifications for org-mode entries"
  '((emacs "25.1")
    (org "9.0")
    (sound-wav "0.2")
    (alert "1.2")
    (seq "2.21"))
  :commit "41a8a6b57e11a5b676b03925d473066655364808" :authors
  '(("doppelc"))
  :maintainer
  '("doppelc")
  :keywords
  '("outlines")
  :url "https://github.com/doppelc/org-notifications")
;; Local Variables:
;; no-byte-compile: t
;; End:
