;;; org-notifications-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "org-notifications" "org-notifications.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-notifications.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-notifications" '("org-notifications-")))

;;;***

;;;### (autoloads nil nil ("org-notifications-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-notifications-autoloads.el ends here
