;;; helm-img-tiqav-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-img-tiqav" "helm-img-tiqav.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from helm-img-tiqav.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-img-tiqav" '("helm-img-tiqav")))

;;;***

;;;### (autoloads nil nil ("helm-img-tiqav-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-img-tiqav-autoloads.el ends here
