;;; -*- no-byte-compile: t -*-
(define-package "org-notebook" "20170322.452" "Ease the use of org-mode as a notebook" '((emacs "24") (org "8") (cl-lib "0.5")) :commit "86042d866bf441e2c9bb51f995e5994141b78517" :keywords '("convenience" "tools") :authors '(("Paul Elder" . "paul.elder@amanokami.net")) :maintainer '("Paul Elder" . "paul.elder@amanokami.net"))
