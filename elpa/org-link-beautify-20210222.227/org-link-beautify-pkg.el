;;; Generated package description from org-link-beautify.el  -*- no-byte-compile: t -*-
(define-package "org-link-beautify" "20210222.227" "Beautify Org Links" '((emacs "27.1") (all-the-icons "4.0.0")) :commit "4662b3a7b9244aa35aae2f469f87be4a44a6b1bb" :keywords '("hypermedia") :url "https://github.com/stardiviner/org-link-beautify")
