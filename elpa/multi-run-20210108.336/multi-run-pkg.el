(define-package "multi-run" "20210108.336" "Efficiently manage multiple remote nodes"
  '((emacs "24")
    (window-layout "1.4"))
  :commit "13d4d923535b5e8482b13ff76185203075fb26a3" :authors
  '(("Sagar Jha"))
  :maintainer
  '("Sagar Jha")
  :keywords
  '("multiple shells" "multi-run" "remote nodes")
  :url "https://www.github.com/sagarjha/multi-run")
;; Local Variables:
;; no-byte-compile: t
;; End:
