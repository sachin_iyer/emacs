;;; Generated package description from helm-firefox.el  -*- no-byte-compile: t -*-
(define-package "helm-firefox" "20210331.1900" "Firefox bookmarks" '((helm "1.5") (cl-lib "0.5") (emacs "24.1")) :commit "58a7ff023c76857ca9cd82075c8743446a50c055" :url "https://github.com/emacs-helm/helm-firefox")
