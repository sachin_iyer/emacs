;;; python-cell-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "python-cell" "python-cell.el" (0 0 0 0))
;;; Generated autoloads from python-cell.el

(autoload 'python-cell-mode "python-cell" "\
Highlight MATLAB-like cells and navigate between them.

\(fn &optional ARG)" t nil)

(autoload 'python-cell-mode-enable "python-cell" "\


\(fn)" nil nil)

(autoload 'python-cell-mode-disable "python-cell" "\


\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "python-cell" '("narrow-to-cell" "what-cell" "python-")))

;;;***

;;;### (autoloads nil nil ("python-cell-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; python-cell-autoloads.el ends here
