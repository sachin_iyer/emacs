;;; -*- no-byte-compile: t -*-
(define-package "python-cell" "20190217.1823" "Support for MATLAB-like cells in python mode" 'nil :commit "665725446b194dbaaff9645dd880524368dd710a" :keywords '("python" "matlab" "cell") :authors '(("Thomas Hisch" . "t.hisch@gmail.com")) :maintainer '("Thomas Hisch" . "t.hisch@gmail.com"))
