(define-package "org-toodledo" "20150301.1113" "Toodledo integration for Emacs Org mode"
  '((request-deferred "0.2.0")
    (emacs "24")
    (cl-lib "0.5"))
  :keywords
  '("outlines" "data")
  :authors
  '(("Christopher J. White" . "emacs@grierwhite.com"))
  :maintainer
  '("Christopher J. White" . "emacs@grierwhite.com"))
;; Local Variables:
;; no-byte-compile: t
;; End:
