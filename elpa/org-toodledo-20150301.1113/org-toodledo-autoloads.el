;;; org-toodledo-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "org-toodledo" "org-toodledo.el" (0 0 0 0))
;;; Generated autoloads from org-toodledo.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-toodledo" '("org-t" "alist-")))

;;;***

;;;### (autoloads nil "org-toodledo-sim" "org-toodledo-sim.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from org-toodledo-sim.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-toodledo-sim" '("org-toodledo-sim-")))

;;;***

;;;### (autoloads nil nil ("org-toodledo-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-toodledo-autoloads.el ends here
