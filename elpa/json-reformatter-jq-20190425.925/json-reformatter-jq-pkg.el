;;; -*- no-byte-compile: t -*-
(define-package "json-reformatter-jq" "20190425.925" "reformat json using jq" '((emacs "24") (reformatter "0.3")) :commit "86bb6f7f7e116bcb0d52f37db308085b5b6ecb16" :keywords '("languages") :authors '(("wouter bolsterlee" . "wouter@bolsterl.ee")) :maintainer '("wouter bolsterlee" . "wouter@bolsterl.ee") :url "https://github.com/wbolster/emacs-json-reformatter-jq")
