;;; json-reformatter-jq-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "json-reformatter-jq" "json-reformatter-jq.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from json-reformatter-jq.el
 (autoload 'json-reformatter-jq-buffer "json-reformatter-jq" nil t)
 (autoload 'json-reformatter-jq-region "json-reformatter-jq" nil t)
 (autoload 'json-reformatter-jq-on-save-mode "json-reformatter-jq" nil t)
 (autoload 'jsonlines-reformatter-jq-buffer "json-reformatter-jq" nil t)
 (autoload 'jsonlines-reformatter-jq-region "json-reformatter-jq" nil t)
 (autoload 'jsonlines-reformatter-jq-on-save-mode "json-reformatter-jq" nil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "json-reformatter-jq" '("json-reformatter-jq-")))

;;;***

;;;### (autoloads nil nil ("json-reformatter-jq-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; json-reformatter-jq-autoloads.el ends here
