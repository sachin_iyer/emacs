(define-package "ein" "20210429.1630" "Emacs IPython Notebook"
  '((emacs "25")
    (websocket "20190620")
    (anaphora "20180618")
    (request "20200117")
    (deferred "0.5")
    (polymode "20190714")
    (dash "2.13.0")
    (with-editor "20200522"))
  :commit "d33e04da06421813cdffed6af18e5379f7399c07" :keywords
  '("jupyter" "literate programming" "reproducible research")
  :url "https://github.com/dickmao/emacs-ipython-notebook")
;; Local Variables:
;; no-byte-compile: t
;; End:
