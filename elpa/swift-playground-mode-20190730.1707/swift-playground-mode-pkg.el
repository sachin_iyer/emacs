(define-package "swift-playground-mode" "20190730.1707" "Run Apple's playgrounds in Swift buffers"
  '((emacs "24.4")
    (seq "2.2.0"))
  :commit "111cde906508824ee11d774b908df867142a8aec" :keywords
  '("languages" "swift")
  :url "https://gitlab.com/michael.sanders/swift-playground-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
