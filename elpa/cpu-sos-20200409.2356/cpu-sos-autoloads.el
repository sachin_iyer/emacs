;;; cpu-sos-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "cpu-sos" "cpu-sos.el" (0 0 0 0))
;;; Generated autoloads from cpu-sos.el

(autoload 'cpu-sos-mode "cpu-sos" "\
Toggle CPU-SOS mode on or off.
With a prefix argument ARG, enable CPU-SOS mode if ARG is
positive, and disable it otherwise.  If called from Lisp, enable
the mode if ARG is omitted or nil, and toggle it if ARG is ‘toggle’.

For a description of this mode: \\[decribe-package] cpu-sos RET.

\(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "cpu-sos" '("cpu-sos")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; cpu-sos-autoloads.el ends here
