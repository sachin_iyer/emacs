;;; -*- no-byte-compile: t -*-
(define-package "dired-rmjunk" "20191007.1232" "A home directory cleanup utility for Dired." 'nil :commit "92af5fcc2bd0bc3826f4ce238a850e9a362533a4" :keywords '("files" "matching") :authors '(("Jakob L. Kreuze" . "zerodaysfordays@sdf.lonestar.org")) :maintainer '("Jakob L. Kreuze" . "zerodaysfordays@sdf.lonestar.org") :url "https://git.sr.ht/~jakob/dired-rmjunk")
