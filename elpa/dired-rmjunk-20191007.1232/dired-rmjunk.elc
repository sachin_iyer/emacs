;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301\302\303\304\305%\210\306\307\310\311\312DD\313\314\315%\207" [custom-declare-group dired-rmjunk nil "Remove junk files with dired." :group dired custom-declare-variable dired-rmjunk-patterns funcall function #[0 "\300\207" [(".adobe" ".macromedia" ".recently-used" ".local/share/recently-used.xbel" "Desktop" ".thumbnails" ".gconfd" ".gconf" ".local/share/gegl-0.2" ".FRD/log/app.log" ".FRD/links.txt" ".objectdb" ".gstreamer-0.10" ".pulse" ".esd_auth" ".config/enchant" ".spicec" ".dropbox-dist" ".parallel" ".dbus" "ca2" "ca2~" ".distlib" ".bazaar" ".bzr.log" ".nv" ".viminfo" ".npm" ".java" ".oracle_jre_usage" ".jssc" ".tox" ".pylint.d" ".qute_test" ".QtWebEngineProcess" ".qutebrowser" ".asy" ".cmake" ".gnome" "unison.log" ".texlive" ".w3m" ".subversion" "nvvp_workspace")] 1] "Default list of files to remove. Current as of f707d92." :type (list string)] 6)
#@83 Whether or not `dired-rmjunk' was responsible for any of the
current dired marks.
(defvar dired-rmjunk--responsible-for-last-mark nil (#$ . 1294))
#@52 Queue of directories to visit following a removal.
(defvar dired-rmjunk--visit-queue nil (#$ . 1447))
#@608 Mark all junk files in the current dired buffer.
'Junk' is defined to be any file with a name matching one of the
patterns in `dired-rmjunk-patterns'. A pattern is said to match
under the following conditions:

  1. If the pattern lacks a directory component, matching means
  that the regexp specified by the pattern matches the file-name.
  2. If the pattern lacks a directory component, matching means
  that that the regexp specified by the file-name component of
  the pattern matches the file-name, AND the regexp specified by
  the directory component of the pattern matches the current
  directory.
(defalias 'dired-rmjunk #[0 "\304=\205u \212\305\306	!\211\203] \211@\n\211\203U \211@\307!\204% \310\"\204> \307!\203N \310\307!\311 \"\203N \310\312!\"\203N T\262\313\314	!P!\210\315\316!\210A\266\202\202 \210A\266\202\202 \210\211\305U\203k \317\320!\210\202q \317\321!\210\322\211\262)\207" [major-mode dired-directory dired-rmjunk-patterns dired-rmjunk--responsible-for-last-mark dired-mode 0 directory-files dired-rmjunk--dir-name string-match dired-current-directory dired-rmjunk--file-name dired-goto-file expand-file-name dired-flag-file-deletion 1 message "No junk files found :)" "Junk files marked." t] 8 (#$ . 1556) nil])
(advice-add 'dired-do-flagged-delete :after 'dired-rmjunk--after-delete)
(defalias 'dired-rmjunk--after-delete #[0 "\2040 	\2030 \302\303\304 \305\306\307$\205 \310\"\266\205\311\312\"\211\203. \313\314!\203. \211\266\203V \203V \315@!q\203V \316\317@\"\203V \320 \321U\203V A\211\2048 \305\211\207" [dired-rmjunk--visit-queue dired-rmjunk--responsible-for-last-mark list #[257 "\300 \301Q\207" [dired-current-directory "/"] 4 "\n\n(fn SUBDIR)"] dired-rmjunk--directories-in-patterns nil apply cl-mapcar cl-coerce cl-remove-if-not file-exists-p y-or-n-p "Visit subdirectories?" dired message "Visiting %s..." dired-rmjunk 0] 10])
#@111 Return the directory portion of PATH, or `nil' if the path
does not contain a directory component.

(fn PATH)
(defalias 'dired-rmjunk--dir-name #[257 "\300\301\302\303$\211\205 \304\305T#\207" [cl-position 47 :from-end t cl-subseq 0] 6 (#$ . 3472)])
#@50 Return the file-name portion of PATH.

(fn PATH)
(defalias 'dired-rmjunk--file-name #[257 "\300\301\302\303$\211\203 \304T\"\202 \207" [cl-position 47 :from-end t cl-subseq] 6 (#$ . 3732)])
#@27 

(fn &optional PATTERNS)
(defalias 'dired-rmjunk--directories-in-patterns #[256 "\211\206 \301\302\303\304\305\306\307\310$\205 \311\"\266\205\"\312\313#\207" [dired-rmjunk-patterns cl-remove-duplicates cl-remove-if null list dired-rmjunk--dir-name nil apply cl-mapcar cl-coerce :test string=] 14 (#$ . 3934)])
(provide 'dired-rmjunk)
