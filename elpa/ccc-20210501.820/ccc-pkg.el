;;; Generated package description from ccc.el  -*- no-byte-compile: t -*-
(define-package "ccc" "20210501.820" "buffer local cursor color control library" 'nil :commit "7a7e1ecaf7f4f68058f1b8831d0b7b839d228614" :authors '(("Masatake YAMATO" . "masata-y@is.aist-nara.ac.jp")) :maintainer '("SKK Development Team") :keywords '("cursor") :url "https://github.com/skk-dev/ddskk")
