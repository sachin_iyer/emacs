;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


#@1088 Find duplicates of files and put them in a dired buffer.
FILES is a list of files which will be compared. DIR is the directory
which will be checked for duplicates of any of the files in the list.
Any matching files will be placed in a new dired buffer with name
*duplicated files*.
When called interactively from a dired buffer, the marked files in that dired buffer will
be treated as the orginals whose duplicates are to be found, and the user will be prompted
for a directory to search for duplicates.
If the function is called with 1 prefix arg then the original files that have duplicates
will be marked for deletion.
With 2 prefix args the files in the *duplicate files* buffer will be marked for deletion.
With 3 prefix args the original files will be placed in the *duplicated files* buffer,
interleaved with the duplicates (original file first, followed by duplicates), and 
the original files will be marked for deletion.
With 4 prefix args the behaviour is the same as with 3 prefix args except that the 
duplicate files will be marked for deletion instead of the originals.
(defalias 'dired-find-duplicates #[(files dir) "\306 <\203\f @\206 \307\211\310\311!\210\312\313\307#\210\314 \315\316\3171\"!\210\314 2\2113\3074\2115\203K 5@4\320\3212\"\2105A\2115\2047 *\310\322!\210*	\204^ \323\324!\210\325\326\307\"\210\323\327!\210\203r \330U\204r \331U\203\300 \332\311\333	!B!\210\334\f!\210\203\242 \330U\203\242 \203\242 \335\336 6\3376\n\340\341$\205\236 \342\343!)\307\"\210\344 \210\345 \210\346\311!\210\205\362 \331U\205\362 	\205\362 \347\350!\202\362 \3077\351\352\n	#\210\332\311\353\354\3337!\"B!\210\334\311!\210\344 \210\355U\203\350 \347\356!\202\361 \357U\205\361 \347\360!),\207" [current-prefix-arg duplicated-matched-files orignal-matched-files curr-arg original-buffer-name files buffer-name nil dired-dups-kill-buffer "*duplicated files*" dired-do-shell-command "md5sum" dired-dups-md5-file-pair shell-command format "find %s -type f -exec md5sum {} \\;" mapc #[(arg) "@	@\230\205 A	A\230?\205 	A\nBAB\211\207" [arg pair orignal-matched-files duplicated-matched-files] 2] "*Shell Command Output*" message "No duplicated files found!" throw --cl-block-dired-find-duplicates-- "Find duplicated files done" 4 16 dired reverse switch-to-buffer dired-map-over-marks dired-get-filename member* :test string-equal dired-flag-file-deletion 1 delete-other-windows split-window-vertically switch-to-buffer-other-window dired-map-dired-file-lines #[(arg) "\300\301!\207" [dired-flag-file-deletion 1] 2] mapcar* #[(arg1 arg2) "\305	\306\307\310\311&\211\203 \n\211\312\n@\fC\"\240)\202# \fD	B\211)\207" [arg1 original-duplicate-list find-it #1=#:v arg2 member* :test string-equal :key car append] 8] reduce append 64 #[(file) "\302	\303\304\305\306&\205 \307\310!\207" [file original-duplicate-list member* :test string-equal :key #[(arg) "\301@!\207" [arg file-truename] 2] dired-flag-file-deletion 1] 7] 256 #[(file) "\302	\303\304\305\306&\205 \307\310!\207" [file duplicated-matched-files member* :test string-equal :key #[(arg) "\301!\207" [arg file-truename] 2] dired-flag-file-deletion 1] 7] dir tobe-checked-pair marked-pair pair --dolist-tail-- file-name original-duplicate-list] 7 (#$ . 410) (list (dired-get-marked-files) (read-directory-name "Directory to be checked: "))])
#@64 Get an alist of (md5 . file) in buffer *Shell Command Output*.
(defalias 'dired-dups-md5-file-pair #[nil "r\305q\210eb\210\306m\2041 `\307\310\311 \312\313$	\n{\314\306w\210`\311 {\fBB\306y\210,\202	 \237*\207" [lst beg end md5 file "*Shell Command Output*" nil re-search-forward " " line-end-position t 1 "[ 	]"] 5 (#$ . 3778)])
#@47 When a buffer with name NAME exists, kill it.
(defalias 'dired-dups-kill-buffer #[(name) "\301!\205	 \302!\207" [name get-buffer kill-buffer] 2 (#$ . 4123)])
(provide 'dired-dups)
