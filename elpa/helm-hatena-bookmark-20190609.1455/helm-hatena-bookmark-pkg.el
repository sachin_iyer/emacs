;;; -*- no-byte-compile: t -*-
(define-package "helm-hatena-bookmark" "20190609.1455" "Hatena::Bookmark with helm interface" '((emacs "24") (helm "2.8.2")) :commit "10b8bfbd7fc4c3f503b2bc01f0c062dac128059e" :authors '(("Takashi Masuda" . "masutaka.net@gmail.com")) :maintainer '("Takashi Masuda" . "masutaka.net@gmail.com") :url "https://github.com/masutaka/emacs-helm-hatena-bookmark")
