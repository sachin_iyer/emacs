;;; helm-hatena-bookmark-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-hatena-bookmark" "helm-hatena-bookmark.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from helm-hatena-bookmark.el

(autoload 'helm-hatena-bookmark "helm-hatena-bookmark" "\
Search Hatena::Bookmark using `helm'.

\(fn)" t nil)

(autoload 'helm-hatena-bookmark-initialize "helm-hatena-bookmark" "\
Initialize `helm-hatena-bookmark'.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-hatena-bookmark" '("helm-hatena-bookmark-")))

;;;***

;;;### (autoloads nil nil ("helm-hatena-bookmark-pkg.el") (0 0 0
;;;;;;  0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-hatena-bookmark-autoloads.el ends here
