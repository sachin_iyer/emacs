(define-package "erlang" "20210315.1640" "Erlang major mode"
  '((emacs "24.1"))
  :commit "3d0668bd2064efeb4edfa8597745d3fa7282259e" :authors
  '(("Anders Lindgren"))
  :maintainer
  '("Anders Lindgren")
  :keywords
  '("erlang" "languages" "processes"))
;; Local Variables:
;; no-byte-compile: t
;; End:
