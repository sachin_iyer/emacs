;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\207" [require cl-lib ibuffer ibuf-ext rcirc] 2)
#@44 Return the list of current server buffers.
(defalias 'ibuffer-rcirc--server-buffers #[nil "\301\302\303 \"\304\305\306\"\307\310#)\207" [rcirc-buffers cl-remove-if-not #[(buffer) "rq\210	\205 \n\303=)\207" [buffer rcirc-server-buffer major-mode rcirc-mode] 2] buffer-list cl-remove-duplicates mapcar #[(buffer) "rq\210	)\207" [buffer rcirc-server-buffer] 1] :test equal] 4 (#$ . 517)])
#@132 Create a set of ibuffer filter groups based on the current irc servers.
Use this to programatically create your own filter groups.
(defalias 'ibuffer-rcirc-generate-filter-groups-by-server #[nil "\300\301\302 \"\207" [mapcar #[(server-buffer) "\301!\302\303\304FD\207" [server-buffer buffer-name predicate equal rcirc-server-buffer] 5] ibuffer-rcirc--server-buffers] 3 (#$ . 915)])
#@36 Set filter group by rcirc servers.
(defalias 'ibuffer-rcirc-set-filter-groups-by-server #[nil "\301 \302\303\304\"\207" [ibuffer-filter-groups ibuffer-rcirc-generate-filter-groups-by-server ibuffer-update nil t] 3 (#$ . 1305) nil])
#@159 Return rcirc activiy status string for BUFFER.
Returns a cons cell where the car is a string for the mini
status and the cdr is a string for the full status.
(defalias 'ibuffer-rcirc--activity-status-strings #[(buffer) "rq\210	\2030 \303\n>\203 \304\305B\2023 \306\n>\203  \307\310B\2023 \n\203* \311\312B\2023 \313\314B\2023 \313\211B)\207" [buffer rcirc-server-buffer rcirc-activity-types nick "m" "Nickname mentioned." keyword "k" "Keyword mentioned." "*" "Unread messages." " " "No activity."] 2 (#$ . 1545)])
#@311 Non-nil if Ibuffer-Rcirc-Track minor mode is enabled.
See the `ibuffer-rcirc-track-minor-mode' command
for a description of this minor mode.
Setting this variable directly does not take effect;
either customize it (see the info node `Easy Customization')
or call the function `ibuffer-rcirc-track-minor-mode'.
(custom-declare-variable 'ibuffer-rcirc-track-minor-mode nil '(#$ . 2068) :set 'custom-set-minor-mode :initialize 'custom-initialize-default :group 'ibuffer-rcirc :type 'boolean)
#@324 Global minor mode for tracking activity status in rcirc buffers.

If called interactively, enable Ibuffer-Rcirc-Track minor mode if
ARG is positive, and disable it if ARG is zero or negative.  If
called from Lisp, also enable the mode if ARG is omitted or nil,
and toggle it if ARG is `toggle'; disable the mode otherwise.
(defalias 'ibuffer-rcirc-track-minor-mode #[(&optional arg) "\305 \306\303	\307=\203 \310\303!?\202 \311	!\312V\"\210\n\204 \313\203+ \314\315\316\"\210\2020 \317\315\316\"\210\320\321\310\303!\203< \322\202= \323\"\210\324\325!\203h \326\303!\210\305 \203U \305 \232\203h \327\330\331\310\303!\203c \332\202d \333\f#\210))\334 \210\310\303!\207" [#1=#:last-message arg global-mode-string ibuffer-rcirc-track-minor-mode local current-message set-default toggle default-value prefix-numeric-value 0 (#2="") add-hook window-configuration-change-hook rcirc-window-configuration-change remove-hook run-hooks ibuffer-rcirc-track-minor-mode-hook ibuffer-rcirc-track-minor-mode-on-hook ibuffer-rcirc-track-minor-mode-off-hook called-interactively-p any customize-mark-as-set #2# message "Ibuffer-Rcirc-Track minor mode %sabled%s" "en" "dis" force-mode-line-update] 4 (#$ . 2564) (list (or current-prefix-arg 'toggle))])
(defvar ibuffer-rcirc-track-minor-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\300!\205 \311\211%\207" [ibuffer-rcirc-track-minor-mode-map ibuffer-rcirc-track-minor-mode-hook variable-documentation put "Hook run after entering or leaving `ibuffer-rcirc-track-minor-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode ibuffer-rcirc-track-minor-mode "" boundp nil] 6)
(defalias 'ibuffer-make-column-rcirc-activity-status-one-char #[(buffer mark) "\210r	q\210\302\303!\210\304p!@)\207" [mark buffer ibuffer-rcirc-track-minor-mode 1 ibuffer-rcirc--activity-status-strings] 2])
(put 'ibuffer-make-column-rcirc-activity-status-one-char 'ibuffer-column-name "I")
(defalias 'ibuffer-make-column-rcirc-activity-status-mini #[(buffer mark) "\210r	q\210\302\303!\210\304p!@)\207" [mark buffer ibuffer-rcirc-track-minor-mode 1 ibuffer-rcirc--activity-status-strings] 2])
(put 'ibuffer-make-column-rcirc-activity-status-mini 'ibuffer-column-name "IRC")
(defalias 'ibuffer-make-column-rcirc-activity-status #[(buffer mark) "\210r	q\210\302\303!\210\304p!A)\207" [mark buffer ibuffer-rcirc-track-minor-mode 1 ibuffer-rcirc--activity-status-strings] 2])
(put 'ibuffer-make-column-rcirc-activity-status 'ibuffer-column-name "IRC activity")
#@64 Toggle current view to buffers matching QUALIFIER server name.
(defalias 'ibuffer-filter-by-rcirc-server #[(qualifier) "\303B\304\n!\204 \305\306	\"\202 \305\307	\"\210\310\311\312\"*\207" [qualifier #1=#:ibuffer-qualifier-str #2=#:ibuffer-filter rcirc-server ibuffer-push-filter message "Filter by vc root dir already applied:  %s" "Filter by vc root dir added:  %s" ibuffer-update nil t] 3 (#$ . 5176) (list (completing-read "Filter by server: " (mapcar #'buffer-name (ibuffer-rcirc--server-buffers)) nil t))])
(byte-code "\301\302\303EB\301\207" [ibuffer-filtering-alist rcirc-server "vc root dir" #[(buf qualifier) "\3031 rq\210	\205 \n\304	!\232)0\207\210\305 \210\306\207" [buf rcirc-server-buffer qualifier (error) buffer-name ibuffer-pop-filter nil] 3]] 3)
#@41 Sort the buffers by their rcirc status.
(defalias 'ibuffer-do-sort-by-rcirc-activity-status #[nil "\303\211	=\203 \n?\304\305!\210\303\211\207" [ibuffer-sorting-mode ibuffer-last-sorting-mode ibuffer-sorting-reversep rcirc-activity-status ibuffer-redisplay t] 3 (#$ . 5958) nil])
(byte-code "\301\302\303EB\304\305!\207" [ibuffer-sorting-functions-alist rcirc-activity-status "rcirc status" #[(a b) "\304@!@\304	@!@\211\n\230\203$ \305\230\203$ \306@!\306	@!\231\202' \n\231*\207" [a b status-b status-a ibuffer-rcirc--activity-status-strings " " buffer-name] 4] provide ibuffer-rcirc] 3)
