;;; -*- no-byte-compile: t -*-
(define-package "ibuffer-rcirc" "20150215.2118" "Ibuffer integration for rcirc" '((cl-lib "0.2")) :commit "8a4409b1c679d65c819dee4085faf929840e79f8" :keywords '("buffer" "convenience" "comm") :authors '(("Fabián Ezequiel Gallina" . "fgallina@gnu.org")) :maintainer '("Fabián Ezequiel Gallina" . "fgallina@gnu.org") :url "https://github.com/fgallina/ibuffer-rcirc")
