;;; sudo-utils-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "sudo-utils" "sudo-utils.el" (0 0 0 0))
;;; Generated autoloads from sudo-utils.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "sudo-utils" '("sudo-utils-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; sudo-utils-autoloads.el ends here
