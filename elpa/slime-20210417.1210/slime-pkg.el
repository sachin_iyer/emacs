(define-package "slime" "20210417.1210" "Superior Lisp Interaction Mode for Emacs"
  '((cl-lib "0.5")
    (macrostep "0.9"))
  :commit "0d66b02b7dd5ebd8f3e85decd00eb6aeec05d785" :keywords
  '("languages" "lisp" "slime")
  :url "https://github.com/slime/slime")
;; Local Variables:
;; no-byte-compile: t
;; End:
