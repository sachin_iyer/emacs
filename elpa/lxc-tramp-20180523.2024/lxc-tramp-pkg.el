;;; -*- no-byte-compile: t -*-
(define-package "lxc-tramp" "20180523.2024" "TRAMP integration for LXC containers" '((emacs "24") (cl-lib "0.6")) :commit "1aab85fef50df2067902bff13e1bac5e6366908b" :keywords '("lxc" "convenience") :authors '(("montag451")) :maintainer '("montag451") :url "https://github.com/montag451/lxc-tramp")
