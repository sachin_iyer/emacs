;;; dashboard-hackernews-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "dashboard-hackernews" "dashboard-hackernews.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from dashboard-hackernews.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dashboard-hackernews" '("dashboard-hackernews-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; dashboard-hackernews-autoloads.el ends here
