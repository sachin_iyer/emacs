(define-package "ein" "20210416.2315" "Emacs IPython Notebook"
  '((emacs "25")
    (websocket "20190620")
    (anaphora "20180618")
    (request "20200117")
    (deferred "0.5")
    (polymode "20190714")
    (dash "2.13.0")
    (with-editor "20200522"))
  :commit "c6654d5e69f29d5cb671fe9fed19aa203c2ed70b" :keywords
  '("jupyter" "literate programming" "reproducible research")
  :url "https://github.com/dickmao/emacs-ipython-notebook")
;; Local Variables:
;; no-byte-compile: t
;; End:
