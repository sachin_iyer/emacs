(define-package "dashboard" "20210425.737" "A startup screen extracted from Spacemacs"
  '((emacs "25.3")
    (page-break-lines "0.11"))
  :commit "d308a6df8cb8be2b70667501570ac5971eacce68" :authors
  '(("Rakan Al-Hneiti"))
  :maintainer
  '("Rakan Al-Hneiti")
  :keywords
  '("startup" "screen" "tools" "dashboard")
  :url "https://github.com/emacs-dashboard/emacs-dashboard")
;; Local Variables:
;; no-byte-compile: t
;; End:
