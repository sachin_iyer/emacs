(define-package "sly" "20210205.1315" "Sylvester the Cat's Common Lisp IDE"
  '((emacs "24.3"))
  :commit "0f46f91a9542599d62c0c332b39636b2941ea372" :keywords
  '("languages" "lisp" "sly")
  :url "https://github.com/joaotavora/sly")
;; Local Variables:
;; no-byte-compile: t
;; End:
