(define-package "axiom-environment" "20210312.2248" "An environment for using Axiom/OpenAxiom/FriCAS"
  '((emacs "24.2"))
  :commit "ac8228a702290732ba12c5d13b38576a57afb0d6" :authors
  '(("Paul Onions" . "paul.onions@acm.org"))
  :maintainer
  '("Paul Onions" . "paul.onions@acm.org")
  :keywords
  '("axiom" "openaxiom" "fricas"))
;; Local Variables:
;; no-byte-compile: t
;; End:
