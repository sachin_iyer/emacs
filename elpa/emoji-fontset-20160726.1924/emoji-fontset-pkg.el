;;; Generated package description from emoji-fontset.el  -*- no-byte-compile: t -*-
(define-package "emoji-fontset" "20160726.1924" "Set font face for Emoji." 'nil :commit "8f159e8073b9b57a07a54b549df687424eeb98ee" :authors '(("USAMI Kenta" . "tadsan@zonu.me")) :maintainer '("USAMI Kenta" . "tadsan@zonu.me") :keywords '("emoji" "font" "config"))
