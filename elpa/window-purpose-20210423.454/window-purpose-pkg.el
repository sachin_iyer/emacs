(define-package "window-purpose" "20210423.454" "Purpose-based window management for Emacs"
  '((emacs "24.4")
    (let-alist "1.0.3")
    (imenu-list "0.1"))
  :commit "1a556294131a78b557f88bd28d42b43d5c6bd79a" :authors
  '(("Bar Magal"))
  :maintainer
  '("Bar Magal")
  :keywords
  '("frames")
  :url "https://github.com/bmag/emacs-purpose")
;; Local Variables:
;; no-byte-compile: t
;; End:
