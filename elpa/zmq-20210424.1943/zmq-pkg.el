(define-package "zmq" "20210424.1943" "ZMQ bindings in elisp"
  '((cl-lib "0.5")
    (emacs "26"))
  :commit "790033363cf0e78c616cfe117a2f681381e96f29" :authors
  '(("Nathaniel Nicandro" . "nathanielnicandro@gmail.com"))
  :maintainer
  '("Nathaniel Nicandro" . "nathanielnicandro@gmail.com")
  :keywords
  '("comm")
  :url "https://github.com/nnicandro/emacs-zmq")
;; Local Variables:
;; no-byte-compile: t
;; End:
