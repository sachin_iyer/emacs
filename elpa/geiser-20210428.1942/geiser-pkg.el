(define-package "geiser" "20210428.1942" "GNU Emacs and Scheme talk to each other"
  '((emacs "24.4"))
  :commit "70c3d6d5d247836b2d9d988f204ce804ae5db67d" :authors
  '(("Jose Antonio Ortega Ruiz" . "jao@gnu.org"))
  :maintainer
  '("Jose Antonio Ortega Ruiz" . "jao@gnu.org")
  :keywords
  '("languages" "scheme" "geiser")
  :url "https://gitlab.com/emacs-geiser/")
;; Local Variables:
;; no-byte-compile: t
;; End:
