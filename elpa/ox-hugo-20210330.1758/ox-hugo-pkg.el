(define-package "ox-hugo" "20210330.1758" "Hugo Markdown Back-End for Org Export Engine"
  '((emacs "24.4")
    (org "9.0"))
  :commit "be7fbd9f164d8937b2628719e21e8e6b4827e638" :keywords
  '("org" "markdown" "docs")
  :url "https://ox-hugo.scripter.co")
;; Local Variables:
;; no-byte-compile: t
;; End:
