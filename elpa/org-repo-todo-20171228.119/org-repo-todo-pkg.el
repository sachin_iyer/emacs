;;; -*- no-byte-compile: t -*-
(define-package "org-repo-todo" "20171228.119" "Simple repository todo management with org-mode" 'nil :commit "f73ebd91399c5760ad52c6ad9033de1066042003" :keywords '("convenience") :authors '(("justin talbott" . "justin@waymondo.com")) :maintainer '("justin talbott" . "justin@waymondo.com") :url "https://github.com/waymondo/org-repo-todo")
