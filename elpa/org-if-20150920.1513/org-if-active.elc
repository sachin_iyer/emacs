;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\300\306!\207" [require ob-core org org-if-misc org-if-link org-if-interpreter outline] 2)
#@42 Hide the CODE section of an org-if file.
(defalias 'org-if-hide-code #[0 "\212\300\301!b\210e`})\207" [org-find-exact-headline-in-buffer "Code"] 2 (#$ . 576)])
#@37 Add CHOICES section to org-if file.
(defalias 'org-if-add-choices-heading #[0 "\212\300\301!b\210\302\303!\210\304c)\207" [org-find-exact-headline-in-buffer "Code" open-line 1 "* Choices\n"] 2 (#$ . 742)])
#@180 Replacement for `org-confirm-babel-evaluate' when mode is on.
This keeps babel from pestering the user with confirmation checks every time 
they visit a new file.

(fn LANG BODY)
(defalias 'org-if-confirm-babel-evaluate #[514 "\300\230?\207" ["org-if"] 4 (#$ . 955)])
#@58 This is the `org-mode-hook' run by `org-if-active-mode'.
(defalias 'org-if-org-mode-hook #[0 "\203 \304!\203 \305\304!!\210\306	!\307\n!\310 \210\311 \210\312 \210\313\314!\210\315 \207" [*org-if-current-file* buffer-file-name *org-if-current-env* *org-if-old-env* get-file-buffer kill-buffer file-truename copy-hash-table outline-show-all org-if-add-choices-heading org-babel-execute-buffer set-buffer-modified-p nil org-if-hide-code] 3 (#$ . 1230)])
(byte-code "\300\301\302\303\304DD\305\306\307\310\311\312\313\314\315&\207" [custom-declare-variable org-if-active-mode funcall function #[0 "\300\207" [nil] 1] "Non-nil if Org-If-Active mode is enabled.\nSee the `org-if-active-mode' command\nfor a description of this minor mode.\nSetting this variable directly does not take effect;\neither customize it (see the info node `Easy Customization')\nor call the function `org-if-active-mode'." :set custom-set-minor-mode :initialize custom-initialize-default :group org-if-active :type boolean] 12)
#@322 This mode toggles whether the org-if system is active.

If called interactively, enable Org-If-Active mode if ARG is
positive, and disable it if ARG is zero or negative.  If called
from Lisp, also enable the mode if ARG is omitted or nil, and
toggle it if ARG is `toggle'; disable the mode otherwise.

(fn &optional ARG)
(defalias 'org-if-active-mode #[256 "\302 \303\300\304=\203 \305\300!?\202 \306!\307V\"\210\2031 \310\311\312\"\210\313\314\315\"\210\316\317\320\"\210\321 \210\202G \322\311!\210\322\314!\210\323\317\320\"\210\324	!\210\325\321 \210\326\327\305\300!\203S \330\202T \331\"\210\332\333!\203~ \334\300!\210\302 \203l \211\302 \232\203~ \335\336\337\305\300!\203y \340\202z \341#\266\210\342 \210\305\300!\207" [org-if-active-mode *org-if-current-file* current-message set-default toggle default-value prefix-numeric-value 0 customize-set-variable org-confirm-babel-evaluate org-if-confirm-babel-evaluate org-babel-do-load-languages org-babel-load-languages ((org-if . t)) add-hook org-mode-hook org-if-org-mode-hook org-if-reset-env custom-reevaluate-setting remove-hook kill-buffer nil run-hooks org-if-active-mode-hook org-if-active-mode-on-hook org-if-active-mode-off-hook called-interactively-p any customize-mark-as-set "" message "Org-If-Active mode %sabled%s" "en" "dis" force-mode-line-update] 7 (#$ . 2245) (byte-code "\206 \301C\207" [current-prefix-arg toggle] 1)])
(defvar org-if-active-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\300!\205 \311\211%\207" [org-if-active-mode-map org-if-active-mode-hook variable-documentation put "Hook run after entering or leaving `org-if-active-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode org-if-active-mode " Org-IF Active" boundp nil] 6)
#@151 Activate org-if-active minor-mode.
When NO-NAVIGATE-P is specified, do not go to file "index.org" in current directory.

(fn &optional NO-NAVIGATE-P)
(defalias 'activate-org-if #[256 "\302!\211\303P\304!\210\304!\210	\305=\203 \306p!\210\307\310!\210?\205$ \311!\207" [buffer-file-truename major-mode file-name-directory "index.org" message org-mode kill-buffer org-if-active-mode 1 find-file] 5 (#$ . 4114) nil])
#@38 Deactivate org-if-active minor-mode.
(defalias 'deactivate-org-if #[0 "\300\301!\207" [org-if-active-mode 0] 2 (#$ . 4540) nil])
#@30 Toggle `org-if-active-mode'.
(defalias 'toggle-org-if-active-mode #[0 "\203 \301 \207\302 \207" [org-if-active-mode deactivate-org-if activate-org-if] 1 (#$ . 4675) nil])
#@81 Save state of current org-if session in a file in `org-if-save-dir'.
Then quit.
(defalias 'org-if-save-and-quit #[0 "\304\211\305\262\306\262!\307	!P\nD\310	!\204 \311	!\210\312!\"\210\313 \266\203\207" [buffer-file-name org-if-save-dir *org-if-old-env* *org-if-current-file* nil #[257 "\300\301\302!!!\207" [file-name-nondirectory directory-file-name file-name-directory] 5 "\n\n(fn F)"] #[514 "\300\301!r\211q\210\302\303\304\305\306!\307\"\310$\216c\210\311ed\312$*\207" [generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 write-region nil] 9 "\n\n(fn STR FNAME)"] file-name-as-directory file-directory-p make-directory prin1-to-string deactivate-org-if] 8 (#$ . 4854) nil])
#@112 Restore state of `*org-if-current-env*' and `*org-if-current-file*' from save.
Also restore last visited file.
(defalias 'org-if-restore #[0 "\303\211\304\262\305\262\211!\306	!P!\211@A@\307!\203\" \310!\210\311\312!\210\313!\266\205\207" [buffer-file-name org-if-save-dir *org-if-current-env* nil #[257 "\300\301!r\211q\210\302\303\304\305\306!\307\"\310$\216\311!\210\312\313 !@*\207" [generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 insert-file-contents read-from-string buffer-string] 8 "\n\n(fn PATH)"] #[257 "\300\301\302!!!\207" [file-name-nondirectory directory-file-name file-name-directory] 5 "\n\n(fn F)"] file-name-as-directory get-file-buffer kill-buffer activate-org-if t find-file] 9 (#$ . 5623) nil])
(provide 'org-if-active)
