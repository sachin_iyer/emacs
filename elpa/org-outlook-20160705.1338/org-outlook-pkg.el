(define-package "org-outlook" "20160705.1338" "Outlook org" 'nil :keywords
  '("org-outlook")
  :authors
  '(("Matthew L. Fidler"))
  :maintainer
  '("Matthew L. Fidler")
  :url "https://github.com/mlf176f2/org-outlook.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
