;ELC   
;;; Compiled
;;; in Emacs version 27.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\305\306\307\310\311\312%\210\313\314\315\316\311\306\317\320&\210\313\321\322\323\311\306\317\320&\207" [require cl-lib dash transient magit custom-declare-group magit-svn nil "Git-Svn support for Magit." :group magit-extensions custom-declare-variable magit-svn-externals-dir ".git_externals" "Directory from repository root that stores cloned SVN externals." :type string magit-svn-mode-lighter " Msvn" "Mode-line lighter for Magit-Svn mode."] 8)
(defalias 'magit-svn-get-url #[nil "\300\301\302\303#\207" [magit-git-string "svn" "info" "--url"] 4])
(defalias 'magit-svn-get-rev #[nil "\306\307\310\311\"\312\306\211\211\n\210	\210\203< \211A\242\211\f\211\203< \313\314\n\"\2032 \306\n\203< \f\fT\202 -)\211\205H \315\316\n\")\207" [#1=#:needle it-index it #2=#:elt #3=#:i #4=#:list nil magit-git-lines "svn" "info" 0 string-match "^Revision: \\(.+\\)" match-string 1] 6])
(defalias 'magit-svn-expand-braces-in-branches #[(branch) "\305\306\"\204\n C\207\307\310\"\307\311\"\307\312\"\313\307\314\"\315\"\316\317	\",\207" [branch pieces rhs suffix prefix string-match "\\(.+\\){\\(.+,.+\\)}\\(.*\\):\\(.*\\)\\*" match-string 1 3 4 split-string 2 "," mapcar #[(p) "	\n\304	\260\207" [prefix p suffix rhs ":"] 6]] 7])
(defalias 'magit-svn-get-local-ref #[(url) "\306\307\310\311#\312\307\310\313#B\314\315\316\317\"\"\306\307\310\320#\321\203\216 \322\211A\242\323\"\324\325\326@#\324\325\327A@#\324\330\331	#\324\332\333	#\334\f\335Q\"\f\336\232\203W \334	\335Q\202^ \334	\337\f\335\260#\340\"$\"\203v \341\321\211$$\321\202\211 \340#$\"\203\211 \341\321\211$$\321.\202 \n,\207" [branches base-url result pats src dst magit-get "svn-remote" "svn" "fetch" magit-get-all "branches" apply nconc mapcar magit-svn-expand-braces-in-branches "url" nil split-string ":" replace-regexp-in-string "\\*" "\\\\(.*\\\\)" "\\\\1" "\\+" "\\\\+" "//.+@" "//" "^" "$" "" "/" string-match replace-match pat1 pat2 url] 5])
#@171 A cache for svn-ref-info.
As `magit-get-svn-ref-info' might be considered a quite
expensive operation a cache is taken so that `magit-status'
doesn't repeatedly call it.
(defvar magit-svn-get-ref-info-cache nil (#$ . 2447))
#@234 Gather details about the current git-svn repository.
Return nil if there isn't one.  Keys of the alist are ref-path,
trunk-ref-name and local-ref-name.
If USE-CACHE is non-nil then return the value of
`magit-get-svn-ref-info-cache'.
(defalias 'magit-svn-get-ref-info #[(&optional use-cache) "\203\n 	\203\n 	\207\306\307\310\311#\312\211\n\205w \313\n\314\"A@\315!\316!\317\301!\210\320B\321B\322\323\324!$r$q\210\325\216\326\327\330\331\332\333%\210eb\210\334\335\312\336#\203c \337\340!\337\341!\342!\202j \306\307\310\343#\312+B\304\fB\303B\257\211++\207" [use-cache magit-svn-get-ref-info-cache fetch url revision ref magit-get "svn-remote" "svn" "fetch" nil split-string ":" file-name-directory file-name-nondirectory make-local-variable ref-path trunk-ref-name local-ref generate-new-buffer " *temp*" #[nil "\301!\205	 \302!\207" [#1=#:temp-buffer buffer-name kill-buffer] 2] magit-git-insert "log" "-1" "--first-parent" "--grep" "git-svn" re-search-forward "git-svn-id: \\(.+/.+?\\)@\\([0-9]+\\)" t match-string 1 2 magit-svn-get-local-ref "url" #1#] 10 (#$ . 2678)])
#@122 Get the best guess remote ref for the current git-svn based branch.
If USE-CACHE is non-nil, use the cached information.
(defalias 'magit-svn-get-ref #[(&optional use-cache) "\301\302\303!\"A\207" [use-cache assoc local-ref magit-svn-get-ref-info] 4 (#$ . 3786)])
(defalias 'magit-svn #[nil "\300\301!\207" [transient-setup magit-svn] 2 nil nil])
(byte-code "\300\301\302\303#\210\300\301\304\305#\210\300\301\306\307\310\301\311\312$#\210\300\301\313\314#\207" [put magit-svn interactive-only t function-documentation "Invoke `git-svn' commands." transient--prefix transient-prefix :command :man-page "git-svn" transient--layout ([1 transient-column (:description "Arguments") ((1 transient-switch (:key "-n" :description "Dry run" :argument "--dry-run" :command transient:magit-svn:--dry-run)))] [1 transient-column (:description "Actions") ((1 transient-suffix (:key "c" :description "DCommit" :command magit-svn-dcommit)) (1 transient-suffix (:key "r" :description "Rebase" :command magit-svn-rebase)) (1 transient-suffix (:key "f" :description "Fetch" :command magit-svn-fetch)) (1 transient-suffix (:key "x" :description "Fetch Externals" :command magit-svn-fetch-externals)) (1 transient-suffix (:key "s" :description "Show commit" :command magit-svn-show-commit)) (1 transient-suffix (:key "b" :description "Create branch" :command magit-svn-create-branch)) (1 transient-suffix (:key "t" :description "Create tag" :command magit-svn-create-tag)))])] 8)
#@116 Show the Git commit for a Svn revision read from the user.
With a prefix argument also read a branch to search in.
(defalias 'magit-svn-show-commit #[(rev &optional branch) "\303\304\305\306\307\"	$\211\203 \310\n!\202 \311\312\")\207" [rev branch it magit-git-string "svn" "find-rev" format "r%i" magit-show-commit user-error "Revision r%s could not be mapped to a commit"] 7 (#$ . 5255) (list (read-number "SVN revision: ") (and current-prefix-arg (magit-read-local-branch "In branch")))])
#@60 Create svn branch NAME.

(git svn branch [--dry-run] NAME)
(defalias 'magit-svn-create-branch #[(name &optional args) "\302\303\304	$\207" [args name magit-run-git "svn" "branch"] 5 (#$ . 5758) (list (read-string "Branch name: ") (transient-args 'magit-svn))])
#@54 Create svn tag NAME.

(git svn tag [--dry-run] NAME)
(defalias 'magit-svn-create-tag #[(name &optional args) "\302\303\304	$\207" [args name magit-run-git "svn" "tag"] 5 (#$ . 6026) (list (read-string "Tag name: ") (transient-args 'magit-svn))])
#@92 Fetch revisions from Svn and rebase the current Git commits.

(git svn rebase [--dry-run])
(defalias 'magit-svn-rebase #[(&optional args) "\301\302\303#\207" [args magit-run-git-async "svn" "rebase"] 4 (#$ . 6279) (list (transient-args 'magit-svn))])
#@53 Run git-svn dcommit.

(git svn dcommit [--dry-run])
(defalias 'magit-svn-dcommit #[(&optional args) "\301\302\303#\207" [args magit-run-git-async "svn" "dcommit"] 4 (#$ . 6537) (list (transient-args 'magit-svn))])
#@75 Fetch revisions from Svn updating the tracking branches.

(git svn fetch)
(defalias 'magit-svn-fetch #[nil "\300\301\302\"\207" [magit-run-git-async "svn" "fetch"] 3 (#$ . 6758) nil])
#@170 Fetch and rebase all external repositories.
Loops through all external repositories found
in `magit-svn-external-directories' and runs
`git svn rebase' on each of them.
(defalias 'magit-svn-fetch-externals #[nil "\305\306!\210\307\310!\311\312#\211\2030 	\313\211\203, @\314\n!\315\316\317\"\210)A\211\204 *\2024 \320\321!\210)\322 \207" [magit-svn-externals-dir it external --dolist-tail-- default-directory require find-lisp find-lisp-find-files-internal expand-file-name #[(file dir) "\301\230\207" [file ".git"] 2] find-lisp-default-directory-predicate nil file-name-directory magit-run-git "svn" "rebase" user-error "No SVN Externals found. Check magit-svn-externals-dir" magit-refresh] 5 (#$ . 6949) nil])
(defvar magit-svn-mode-map (byte-code "\301 \302\303\304#\210)\207" [map make-sparse-keymap define-key "N" magit-svn] 4))
#@97 Non-nil if Magit-Svn mode is enabled.
Use the command `magit-svn-mode' to change this variable.
(defvar magit-svn-mode nil (#$ . 7803))
(make-variable-buffer-local 'magit-svn-mode)
#@270 Git-Svn support for Magit.

If called interactively, enable Magit-Svn mode if ARG is
positive, and disable it if ARG is zero or negative.  If called
from Lisp, also enable the mode if ARG is omitted or nil, and
toggle it if ARG is `toggle'; disable the mode otherwise.
(defalias 'magit-svn-mode #[(&optional arg) "\304 	\305=\203 \n?\202 \306	!\307V\310\311!\204 \312\313!\210\n\203= \314\315\316\317\320\211%\210\314\315\321\322\320\211%\210\314\323\324\325\320\211%\210\202O \326\315\316\320#\210\326\315\321\320#\210\326\323\324\320#\210\327\330!\203X \331 \210\332\333\n\203b \334\202c \335\"\210\327\330!\203\210 \304 \203w \304 \232\203\210 \336\337\340\n\203\203 \341\202\204 \342#\210))\343 \210\n\207" [#1=#:last-message arg magit-svn-mode local current-message toggle prefix-numeric-value 0 derived-mode-p magit-mode user-error "This mode only makes sense with Magit" magit-add-section-hook magit-status-sections-hook magit-insert-svn-unpulled magit-insert-unpulled-commits t magit-insert-svn-unpushed magit-insert-unpushed-commits magit-status-headers-hook magit-insert-svn-remote nil remove-hook called-interactively-p any magit-refresh run-hooks magit-svn-mode-hook magit-svn-mode-on-hook magit-svn-mode-off-hook " in current buffer" message "Magit-Svn mode %sabled%s" "en" "dis" force-mode-line-update] 6 (#$ . 7991) (list (or current-prefix-arg 'toggle))])
(defvar magit-svn-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\211%\210\311\312\306\"\207" [magit-svn-mode-map magit-svn-mode-hook variable-documentation put "Hook run after entering or leaving `magit-svn-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode magit-svn-mode magit-svn-mode-lighter nil custom-add-option magit-mode-hook] 6)
#@21 Magit-Svn mode menu
(defvar magit-svn-mode-menu nil (#$ . 9849))
(byte-code "\301\300\302\303\304$\210\305\306\307#\207" [magit-svn-mode-menu easy-menu-do-define nil "Magit-Svn mode menu" ("Git-Svn" :visible magit-svn-mode :active (lambda nil (magit-get "svn-remote" "svn" "fetch")) ["Dcommit" magit-svn-dcommit] ["Rebase" magit-svn-rebase] ["Fetch" magit-svn-fetch] ["Fetch Externals" magit-svn-fetch-externals] ["Show commit" magit-svn-show-commit] ["Create branch" magit-svn-create-branch] ["Create tag" magit-svn-create-tag]) easy-menu-add-item magit-mode-menu ("Extensions")] 5)
(defalias 'magit-insert-svn-unpulled #[nil "\306 \211\205`\307\310	!\203 	\202 	\n\236A\206 \311\312\310	!\203* \313	\n\"@\206+ 	\314\315\316\317 \320&\321\f\322\323\324\f\"\211\203G \325=\202z 5\205S \326\327\f!5\"\2116\203a \3306\322\"\202y \331\f7\"\211\205x \332!\203u \f!\325=)))#\210\f\2115\206\216 ?\205\216 8\f859\3332]\334\335!\210\336\337\340\"!\210\341\342!\210\343\f!\210\344\330\f\345\"\346\"\210\321\f\347\317 #'\350\f\351\":\352\337\353\330\f\354\"\"!;\352\337\355\330\f\354\"\"!<:\203\341 :J\206\370 \356;!\203\356 ;J\206\370 \356<!\205\370 <J=\212\330\f\345\"b\210`'W\2038\357`\311\"\206'>\360`\311\"\2040\361`>\311\f$\210=\2030\361`>\351=$\210>b\210)\202.\f8=\203K\315?\362\f!)\202\\\321\330\f\363\"\364\330\211\f\363\"\364\"\fC\244#0\210\f-)\207" [it #1=#:type506 magit--section-type-alist magit-insert-section--parent #2=#:section507 value magit-svn-get-ref svn-unpulled class-p magit-section :type rassq :value nil :start point-marker :parent eieio-oset hidden run-hook-with-args-until-success magit-section-set-visibility-hook hide magit-get-section magit-section-ident eieio-oref magit-section-match-assoc functionp cancel-section magit-insert-heading "Unpulled svn commits:" magit-insert-log format "HEAD..%s" run-hooks magit-insert-section-hook magit-insert-child-count set-marker-insertion-type start t end eieio-oref-default keymap intern "magit-%s-section-map" type "forge-%s-section-map" boundp next-single-property-change get-text-property put-text-property magit-section-show parent children magit-insert-section--oldroot incarnation magit-section-initial-visibility-alist magit-root-section magit-insert-section--current class-map magit-map forge-map map next magit-section-cache-visibility] 10])
(defalias 'magit-insert-svn-unpushed #[nil "\306 \211\205`\307\310	!\203 	\202 	\n\236A\206 \311\312\310	!\203* \313	\n\"@\206+ 	\314\315\316\317 \320&\321\f\322\323\324\f\"\211\203G \325=\202z 5\205S \326\327\f!5\"\2116\203a \3306\322\"\202y \331\f7\"\211\205x \332!\203u \f!\325=)))#\210\f\2115\206\216 ?\205\216 8\f859\3332]\334\335!\210\336\337\340\"!\210\341\342!\210\343\f!\210\344\330\f\345\"\346\"\210\321\f\347\317 #'\350\f\351\":\352\337\353\330\f\354\"\"!;\352\337\355\330\f\354\"\"!<:\203\341 :J\206\370 \356;!\203\356 ;J\206\370 \356<!\205\370 <J=\212\330\f\345\"b\210`'W\2038\357`\311\"\206'>\360`\311\"\2040\361`>\311\f$\210=\2030\361`>\351=$\210>b\210)\202.\f8=\203K\315?\362\f!)\202\\\321\330\f\363\"\364\330\211\f\363\"\364\"\fC\244#0\210\f-)\207" [it #1=#:type508 magit--section-type-alist magit-insert-section--parent #2=#:section509 value magit-svn-get-ref svn-unpushed class-p magit-section :type rassq :value nil :start point-marker :parent eieio-oset hidden run-hook-with-args-until-success magit-section-set-visibility-hook hide magit-get-section magit-section-ident eieio-oref magit-section-match-assoc functionp cancel-section magit-insert-heading "Unpushed git commits:" magit-insert-log format "%s..HEAD" run-hooks magit-insert-section-hook magit-insert-child-count set-marker-insertion-type start t end eieio-oref-default keymap intern "magit-%s-section-map" type "forge-%s-section-map" boundp next-single-property-change get-text-property put-text-property magit-section-show parent children magit-insert-section--oldroot incarnation magit-section-initial-visibility-alist magit-root-section magit-insert-section--current class-map magit-map forge-map map next magit-section-cache-visibility] 10])
#@84 Jump to the section "Unpulled svn commits".
With a prefix argument also expand it.
(defalias 'magit-jump-to-svn-unpulled #[(&optional expand) "\305\306\307B\310!B!\211\2033 \311	\312\"b\210\n\2056 \3131& \307\314	!)0\202, \210\315\316\317!\210\320\321!\2026 \322\323!)\207" [magit-root-section it expand inhibit-quit quit-flag magit-get-section svn-unpulled nil magit-section-ident eieio-oref start (quit) magit-section-show t eval (ignore nil) recenter 0 message "Section \"Unpulled svn commits\" wasn't found"] 5 (#$ . 14020) "P"])
#@84 Jump to the section "Unpushed git commits".
With a prefix argument also expand it.
(defalias 'magit-jump-to-svn-unpushed #[(&optional expand) "\305\306\307B\310!B!\211\2033 \311	\312\"b\210\n\2056 \3131& \307\314	!)0\202, \210\315\316\317!\210\320\321!\2026 \322\323!)\207" [magit-root-section it expand inhibit-quit quit-flag magit-get-section svn-unpushed nil magit-section-ident eieio-oref start (quit) magit-section-show t eval (ignore nil) recenter 0 message "Section \"Unpushed git commits\" wasn't found"] 5 (#$ . 14564) "P"])
(defalias 'magit-insert-svn-remote #[nil "\306 \211\205d\307\310	!\203 	\202 	\n\236A\206 \311\312\310	!\203* \313	\n\"@\206+ 	\314\315\316\317 \320&\321\f\322\323\324\f\"\211\203G \325=\202z 8\205S \326\327\f!8\"\2119\203a \3309\322\"\202y \331\f:\"\211\205x \332!\203u \f!\325=)))#\210\f\2118\206\216 ?\205\216 ;\f;8<\3332a\334\335\336\337\340P\341\342#\343 $c\210\344\345!\210\346\f!\210\347\330\f\350\"\351\"\210\321\f\352\317 #*\353\f\354\"=\355\334\356\330\f\357\"\"!>\355\334\360\330\f\357\"\"!?=\203\345 =J\206\374 \361>!\203\362 >J\206\374 \361?!\205\374 ?J@\212\330\f\350\"b\210`*W\203<\362`\311\"\206*A\363`\311\"\2044\364`A\311\f$\210@\2034\364`A\354@$\210Ab\210)\202.\f;=\203O\315B\365\f!)\202`\321\330\f\366\"\367\330\211\f\366\"\367\"\fC\244#0\210\f-)\207" [it #1=#:type510 magit--section-type-alist magit-insert-section--parent #2=#:section511 value magit-svn-get-rev line class-p magit-section :type rassq :value nil :start point-marker :parent eieio-oset hidden run-hook-with-args-until-success magit-section-set-visibility-hook hide magit-get-section magit-section-ident eieio-oref magit-section-match-assoc functionp cancel-section format "%-10s%s from %s\n" "Remote:" propertize "r" face magit-hash magit-svn-get-url run-hooks magit-insert-section-hook magit-insert-child-count set-marker-insertion-type start t end eieio-oref-default keymap intern "magit-%s-section-map" type "forge-%s-section-map" boundp next-single-property-change get-text-property put-text-property magit-section-show parent children magit-insert-section--oldroot incarnation magit-section-initial-visibility-alist magit-root-section magit-insert-section--current class-map magit-map forge-map map next magit-section-cache-visibility] 10])
(provide 'magit-svn)
