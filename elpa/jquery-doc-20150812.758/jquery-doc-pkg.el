(define-package "jquery-doc" "20150812.758" "jQuery api documentation interface for emacs" 'nil :keywords
  '("docs" "jquery")
  :authors
  '(("Anantha kumaran" . "ananthakumaran@gmail.com"))
  :maintainer
  '("Anantha kumaran" . "ananthakumaran@gmail.com"))
;; Local Variables:
;; no-byte-compile: t
;; End:
