(define-package "lispy" "20201226.1746" "vi-like Paredit"
  '((emacs "24.3")
    (ace-window "0.9.0")
    (iedit "0.9.9")
    (counsel "0.11.0")
    (hydra "0.14.0")
    (zoutline "0.1.0"))
  :commit "1ad128be0afc04b58967c1158439d99931becef4" :authors
  '(("Oleh Krehel" . "ohwoeowho@gmail.com"))
  :maintainer
  '("Oleh Krehel" . "ohwoeowho@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/abo-abo/lispy")
;; Local Variables:
;; no-byte-compile: t
;; End:
