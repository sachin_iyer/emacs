;;; Generated package description from dired-launch.el  -*- no-byte-compile: t -*-
(define-package "dired-launch" "20210416.1954" "Use dired as a launcher" 'nil :commit "2a946c72473b3d2e4a7c3827298f54c2b9f3edc2" :authors '(("David Thompson")) :maintainer '("David Thompson") :keywords '("dired" "launch") :url "https://github.com/thomp/dired-launch")
