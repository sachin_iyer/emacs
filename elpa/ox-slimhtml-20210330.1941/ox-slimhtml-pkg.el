;;; Generated package description from ox-slimhtml.el  -*- no-byte-compile: t -*-
(define-package "ox-slimhtml" "20210330.1941" "a minimal HTML org export backend" '((emacs "24") (cl-lib "0.6")) :commit "72cffc4292c82d5f3a24717ed386a953862485d8" :authors '(("Elo Laszlo <hello at bald dot cat>")) :maintainer '("Elo Laszlo <hello at bald dot cat>") :keywords '("files"))
