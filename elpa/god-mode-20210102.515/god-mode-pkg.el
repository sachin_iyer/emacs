(define-package "god-mode" "20210102.515" "Minor mode for God-like command entering"
  '((emacs "25.1"))
  :commit "a72feb2fe8b1a8993c472995d83d9c4718f7a7c1" :authors
  '(("Chris Done" . "chrisdone@gmail.com"))
  :maintainer
  '("Chris Done" . "chrisdone@gmail.com")
  :url "https://github.com/emacsorphanage/god-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
