;;; Generated package description from clojure-mode.el  -*- no-byte-compile: t -*-
(define-package "clojure-mode" "20210502.824" "Major mode for Clojure code" '((emacs "25.1")) :commit "078f591741d329534595b97c66eb612a8bca0316" :maintainer '("Bozhidar Batsov" . "bozhidar@batsov.com") :keywords '("languages" "clojure" "clojurescript" "lisp") :url "http://github.com/clojure-emacs/clojure-mode")
