;;; multi-run-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "multi-run" "multi-run.el" (0 0 0 0))
;;; Generated autoloads from multi-run.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "multi-run" '("multi-run")))

;;;***

;;;### (autoloads nil "multi-run-helpers" "multi-run-helpers.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from multi-run-helpers.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "multi-run-helpers" '("multi-run-" "calculate-window-batch")))

;;;***

;;;### (autoloads nil "multi-run-vars" "multi-run-vars.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from multi-run-vars.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "multi-run-vars" '("multi-run-")))

;;;***

;;;### (autoloads nil nil ("multi-run-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; multi-run-autoloads.el ends here
