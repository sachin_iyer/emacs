(define-package "multi-run" "20190507.2349" "Efficiently manage multiple remote nodes"
  '((emacs "24")
    (window-layout "1.4"))
  :keywords
  '("multiple shells" "multi-run" "remote nodes")
  :authors
  '(("Sagar Jha"))
  :maintainer
  '("Sagar Jha")
  :url "https://www.github.com/sagarjha/multi-run")
;; Local Variables:
;; no-byte-compile: t
;; End:
