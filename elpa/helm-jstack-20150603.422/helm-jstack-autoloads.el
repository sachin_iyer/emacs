;;; helm-jstack-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-jstack" "helm-jstack.el" (0 0 0 0))
;;; Generated autoloads from helm-jstack.el

(autoload 'helm-jstack-suggest "helm-jstack" "\
Preconfigured `helm' for jstack lookup with jstack suggest.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "helm-jstack" '("helm-j")))

;;;***

;;;### (autoloads nil nil ("helm-jstack-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-jstack-autoloads.el ends here
