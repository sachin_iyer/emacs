;;; -*- no-byte-compile: t -*-
(define-package "outline-toc" "20170730.1130" "Sidebar showing a \"table of contents\"." 'nil :commit "31f04bea19cfcfb01a94d1fd2b72391cb02b7463" :keywords '("convenience" "outlines") :authors '(("Austin Bingham" . "austin.bingham@gmail.com")) :maintainer '("Austin Bingham" . "austin.bingham@gmail.com") :url "https://github.com/abingham/outline-toc.el")
